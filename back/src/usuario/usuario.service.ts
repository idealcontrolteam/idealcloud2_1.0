import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuarios } from './entity/usuarios.entity';
import { GLOBAL_BD_PREFIX } from '../global';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuarios)
    private usuarioRepository: Repository<Usuarios>,
  ) {}

  async validateUser(empresa: string, usuario: any) {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    //const prefix=`${GLOBAL_BD_PREFIX}${empresa}`;
    // console.log(prefix)
    const bd_user = await this.usuarioRepository
    .query(
      `SELECT
      id_usuarios, 
      login_usuarios,
      id_centros,
      perfiles.nombre_perfiles as perfil,
      usuarios.id_perfiles
      FROM ${prefix}.usuarios 
      INNER JOIN ${prefix}.perfiles on perfiles.id_perfiles=usuarios.id_perfiles
        WHERE 
        login_usuarios = ? AND 
        clave_usuarios = ?`,
      [usuario.login_usuarios, usuario.clave_usuarios],
    );
    const result = bd_user.length != 0;
    // console.log(JSON.stringify(bd_user))
    return bd_user;
  }

  // async validateExists(empresa: string, usuario: any) {
  //   const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
  //   //const prefix=`${GLOBAL_BD_PREFIX}${empresa}`;
  //   // console.log(prefix)
  //   const bd_user = await this.usuarioRepository
  //   .query(
  //     `SELECT
  //     id_usuarios, 
  //     id_centros
  //     FROM ${prefix}.cent_usua 
  //       WHERE 
  //       id_usuarios = ${usuario.id_usuario} AND 
  //       id_centros = ${usuario.id_centro}`,
  //   );
  //   const result = bd_user.length != 0;
  //   // console.log(JSON.stringify(bd_user))
  //   return bd_user;
  // }

  async getUsuarios(empresa:string): Promise<Usuarios[]>{
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.usuarioRepository.query(
      `SELECT 
      id_usuarios as id,
      nombres_usuarios as name,
      activo_usuarios as activo
            FROM ${prefix}.usuarios`,
    );
  }
  async getUsuarios_Centros(empresa:string): Promise<Usuarios[]>{
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.usuarioRepository.query(
      `SELECT 
      cent_usua.id_cent_usua as id,
      cent_usua.id_centros,
      cent_usua.id_usuarios,
      usuarios.nombres_usuarios,
      centros.nombre_centros 
      FROM ${prefix}.cent_usua 
      INNER JOIN ${prefix}.usuarios on usuarios.id_usuarios=cent_usua.id_usuarios 
      INNER JOIN ${prefix}.centros on centros.id_centros=cent_usua.id_centros`,
    );
  }
  async createUsuarios_Centros(empresa:string,usuario: any): Promise<Usuarios[]>{
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.usuarioRepository.query(
      `INSERT INTO ${prefix}.cent_usua (id_centros,id_usuarios) SELECT * FROM (SELECT ${usuario.id_centro}, ${usuario.id_usuario}) AS tmp
      WHERE NOT EXISTS (
          SELECT id_centros, id_usuarios FROM ${prefix}.cent_usua WHERE id_centros = ${usuario.id_centro} and id_usuarios=${usuario.id_usuario}
      ) LIMIT 1`,
    );
  }
  async deleteUsuarios_Centros(empresa:string,id: string): Promise<Usuarios[]>{
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.usuarioRepository.query(
      `DELETE FROM ${prefix}.cent_usua WHERE id_cent_usua=${id}`,
    );
  }
}
