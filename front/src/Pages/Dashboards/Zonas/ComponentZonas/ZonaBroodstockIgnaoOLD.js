import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
// import superagent from 'superagent';
import { API_ROOT } from '../../../../api-config';
import Loader from 'react-loaders';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody
} from 'reactstrap';

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis
} from 'recharts';
import Slider from "react-slick";



const APIchart2 = `${API_ROOT}/tag/location/5ccb142f459dc45190294ae8/2019-04-19/2019-10-22`;



export default class SalesDashboard1 extends Component {   
    constructor() {
        super();
        this.state = {   
            myDataChart2: [],        
            isLoading: false,
            error: null
           
        }
        this.loadDataChar = this.loadDataChar.bind(this);

    }
  

    componentDidMount = () => {
        const intervaloRefresco = 10000;
        this.setState({ isLoading: true });
        this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        this.loadDataChar();       
      }
    componentWillUnmount = () => {
       clearInterval(this.intervalIdChart);
    }
   

 
    loadDataChar = () => {      
        //const token = localStorage.getItem("token");        
        const token = "tokenfalso";
        axios
        .get(APIchart2, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart2 = response.data.data;  

           this.setState({ myDataChart2,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });



        
    }
    getMinY = (data)=> {
        return data.reduce((min, p) => p.lastValue < min ? p.lastValue  : min, data[0].lastValue );
      }

    render() {
    
          const { myDataChart2,isLoading, error} = this.state;
          const settings = {
            autoplaySpeed:6000,
            autoplay: true,
            className: "",
            centerMode: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };
       
        const miscolores= ["DODGERBLUE","DARKBLUE","FORESTGREEN","#CD5C5C","var(--danger)","var(--warning)","var(--info)","var(--secondary)","var(--success)","var(--primary)"];

            if (error) {
                return <p>{error.message}</p>;
            }
      
            if (isLoading) {
                return <Loader type="ball-pulse"/>;
            }
      
        return (
            <Fragment>                   
               
                      <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                       Sala Broodstock
                                    </div>
                                </div>
                                <div className="page-title-actions">
                                    Alarmado
                                    <span  className="badge badge-dot badge-dot-lg badge-danger  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>  
                    
                        
                                    Normal
                                    <span  className="badge badge-dot badge-dot-lg badge-success  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span> 
                                </div>                    
                            </div>
                        </div>
                        <Row>

                            <Col sm="6" lg="4" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                        <div
                                            className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                            <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                            Estanque A
                                        </div>                                 
                                    </CardHeader>
                                    <CardBody className="p-0">

                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider1 => (this.slider1 = slider1)} {...settings}>

                                        {
                                                             
                                                             myDataChart2
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-5 text-muted text-uppercase">
                                                                                        {data.name}
                                                                                    </div>

                                                                                </div>

                                                                        
                                                                                <div className="widget-numbers p-1 m-0">
                                                                                    <div className="widget-chart-flex">
                                                                                        <div>
                                                                                            
                                                                                            {data.lastValue}
                                                                                            <small className="opacity-5 pl-1">  {data.unity}</small>
                                                                                        </div>
                                                                                     
                                                                                    </div>
                                                                          
                                                                             
                                                                                </div> 
                                                                             <div className="widget-description opacity-8 text-focus pt-2">
                                                                                   Max:
                                                                                   <div className="d-inline text-danger pr-1">  
                                                                                                                                                                
                                                                                       <span className="pl-1">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   Prom:
                                                                                   <div className="d-inline text-danger pr-1">                                                                                  
                                                                                       <span className="pl-1">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   Min:
                                                                                   <div className="d-inline text-danger pr-1">                                                                                  
                                                                                       <span className="pl-1">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>
                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={90} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:0,
                                                                                                left: -30,
                                                                                                bottom: 0
                                                                                            }}>
                                                                                            <Tooltip   />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '10px'}}
                                                                                            />
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {true}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm DD-MMM')}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                        <h6 className="text-muted text-uppercase font-size-md opacity-5 pl-3 pr-3 pb-1 font-weight-normal">
                                            Sondas 
                                        </h6>
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-4col">
                                                <Row className="no-gutters">

                                                {
                                                             
                                                             myDataChart2
                                                                 .map((data,i)=>
                                                                    <Col sm="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider1.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color="dark">  
                                                                            <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>  
                                                                            <div className="size-boton mt-0 size_boton">                                                    
                                                                            {data.lastValue} 
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>

                                                                 </Col>
                                                                 
                                                                 )
                                                                 
                                                         }  
                                           
                                                </Row>
                                            </div>
                                        </Card>                                                    
                                    </CardBody>
                                </Card>
                            </Col>  
                                  
               
                            <Col sm="6" lg="4" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                        <div
                                            className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                            <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                            Estanque A
                                        </div>                                 
                                    </CardHeader>
                                    <CardBody className="p-0">

                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider  ref={slider2 => (this.slider2 = slider2)} {...settings}>

                                        {
                                                             
                                                             myDataChart2
                                                                 .map((data,i)=>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg p-2">
                                                                                <div className="widget-chart-flex">
                                                                                    <div
                                                                                        className="widget-title opacity-5 text-muted text-uppercase">
                                                                                        {data.name}
                                                                                    </div>
                                                                                </div>
                                                                                <div className="widget-numbers p-1 m-0">
                                                                                    <div className="widget-chart-flex">
                                                                                        <div>
                                                                                            {data.lastValue}
                                                                                            <small className="opacity-5 pl-1">  {data.unity}</small>
                                                                                        </div>
                                                                                     
                                                                                    </div>
                                                                                </div>
                                                                                <div className="widget-description opacity-8 text-focus pt-2">
                                                                                   Max:
                                                                                   <div className="d-inline text-danger pr-1">  
                                                                                                                                                                
                                                                                       <span className="pl-1">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   Prom:
                                                                                   <div className="d-inline text-danger pr-1">                                                                                  
                                                                                       <span className="pl-1">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   Min:
                                                                                   <div className="d-inline text-danger pr-1">                                                                                  
                                                                                       <span className="pl-1">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                              </div> 
                                                                            </div>
                                                                            
                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={90} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right: 0,
                                                                                                left: -30,
                                                                                                bottom: 0
                                                                                            }}>
                                                                                            <Tooltip   />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '10px'}}
                                                                                            />
                                                                                            <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {true}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm DD-MMM')}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                        <h6 className="text-muted text-uppercase font-size-md opacity-5 pl-3 pr-3 pb-1 font-weight-normal">
                                            Sondas 
                                        </h6>
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-4col">
                                                <Row className="no-gutters">

                                                {
                                                             
                                                             myDataChart2
                                                                 .map((data, i)=>
                                                                    <Col sm="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button key={data._id}
                                                                          onClick={ e => this.slider2.slickGoTo(i)}
                                                                           className="btn-icon-vertical btn-square btn-transition p-3" outline color="dark">  
                                                                            <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>  
                                                                            <div className="size-boton mt-0 size_boton">                                                    
                                                                            {data.lastValue} 
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>

                                                                 </Col>
                                                                 
                                                                 )
                                                                 
                                                         }  
                                           
                                                </Row>
                                            </div>
                                        </Card>                                                    
                                    </CardBody>
                                </Card>
                            </Col>  
                                  
               
                            <Col sm="6" lg="4" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                        <div
                                            className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                            <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                            Estanque A
                                        </div>                                 
                                    </CardHeader>
                                    <CardBody className="p-0">

                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider3 => (this.slider3 = slider3)} {...settings}>

                                        {
                                                             
                                                             myDataChart2
                                                                 .map((data,i)=>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex">
                                                                                    <div
                                                                                        className="widget-title opacity-5 text-muted text-uppercase">
                                                                                        {data.name}
                                                                                    </div>
                                  
                                                                                </div>
                                                                                <div className="widget-numbers p-1 m-0">
                                                                                    <div className="widget-chart-flex">
                                                                                        <div>
                                                                                            {data.lastValue}
                                                                                            <small className="opacity-5 pl-1">  {data.unity}</small>
                                                                                        </div>
                                                                                     
                                                                                    </div>
                                                                                </div>
                                                                                <div className="widget-description opacity-8 text-focus pt-2">
                                                                                   Max:
                                                                                   <div className="d-inline text-danger pr-1">  
                                                                                                                                                                
                                                                                       <span className="pl-1">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   Prom:
                                                                                   <div className="d-inline text-danger pr-1">                                                                                  
                                                                                       <span className="pl-1">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   Min:
                                                                                   <div className="d-inline text-danger pr-1">                                                                                  
                                                                                       <span className="pl-1">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                              </div> 
                                                                            </div>
                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={90} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right: 0,
                                                                                                left: -30,
                                                                                                bottom: 0
                                                                                            }}>
                                                                                            <Tooltip   />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '10px'}}
                                                                                            />
                                                                                            <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {true}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm DD-MMM')}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                        <h6 className="text-muted text-uppercase font-size-md opacity-5 pl-3 pr-3 pb-1 font-weight-normal">
                                            Sondas 
                                        </h6>
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-4col">
                                                <Row className="no-gutters">

                                                {
                                                             
                                                             myDataChart2
                                                                 .map((data,i)=>
                                                                    <Col sm="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button key={data._id} 
                                                                          onClick={ e => this.slider3.slickGoTo(i)}
                                                                          className="btn-icon-vertical btn-square btn-transition p-3" outline color="dark">  
                                                                            <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>  
                                                                            <div className="size-boton mt-0 size_boton">                                                    
                                                                            {data.lastValue} 
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>

                                                                 </Col>
                                                                 
                                                                 )
                                                                 
                                                         }  
                                           
                                                </Row>
                                            </div>
                                        </Card>                                                    
                                    </CardBody>
                                </Card>
                            </Col>  
                                  
               
                     
                     
                        </Row>

           
            </Fragment>
        )
    }
}
