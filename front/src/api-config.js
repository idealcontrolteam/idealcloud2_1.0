

let backendHost;
const hostname = window && window.location && window.location.hostname;
 

if(hostname === 'ihc.idealcontrol.cl') {
  //backendHost = 'https://ihc.idealcontrol.cl/aquagen/apiv1/';
  backendHost = 'https://ihc.idealcontrol.cl/ideal_cloud2/back/';
} else {
    backendHost =  `http://${hostname}:3013`;
}


export const API_ROOT = `${backendHost}`;

