import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';
import {Col, Row, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import backgroundImage from '../../../assets/utils/images/sidebar/abstract2.jpg';  
import city3 from '../../../assets/utils/images/dropdown-header/headerbg1.jpg';
import {connect} from 'react-redux';
import {
    setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../../reducers/Session';
// import CryptoJS from 'crypto-js';
//import sha256 from 'crypto-js/sha256';
// Layout
import { API_ROOT } from '../../../api-config';
import axios from "axios";
import {Empresas} from '../../Comun/data'


const APILogin= `${API_ROOT}/user/login`;
const app="idealcloud2.0";

console.log(Empresas);



class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
                email: "",
                password: "",
                rol:"",
                empresa:"",
                empresas:"",
                status: "",
                message: ""
        };
         this.ingresarNameUsuario = this.ingresarNameUsuario.bind(this);
        
        
    }
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.handleLogin();
        }
    }
    handleChange = event => {
            const name = event.target.name;
            const value = event.target.value;
            this.setState({ [name]: value });
    };

    ingresarNameUsuario = (usuario) => {
        let {setNameUsuario} = this.props;
        setNameUsuario(usuario);
    }
    ingresarEmailUsuario = (email) => {
        let {setEmailUsuario} = this.props;
        setEmailUsuario(email);
    }
    ingresarRolUsuario = (rol) =>{
        let {setRolUsuario} = this.props;
        setRolUsuario(rol);
    }
    ingresarCentroUsuario = (centro) =>{
        let {setCentroUsuario} = this.props;
        setCentroUsuario(centro);
    }
    
    handleLogin = () => {
        this.ingresarCentroUsuario("Login")
        // let empresas=this.state.empresas;
        console.log(this.state.empresa)
        let email=this.state.email;
        let split_user=email.split('@');
        let user=split_user[0];
        email=String(split_user[1]).split('.')
        let empresa=Empresas.filter((data)=>email.includes(String(data.title))).map((data)=>{return data.name});
        console.log("empresa "+empresa)
        //alert(email[0])
        this.setCompany(String(empresa,user));
        const data ={
            login_usuarios: user,
            clave_usuarios: this.state.password
          };
        axios
        .post(API_ROOT+"/"+empresa+"/login",data)
                .then(response => {   
                if (response.data.statusCode === 200) { 
                    //console.log(response.data.data.rol)
                    sessionStorage.setItem("rol", response.data.data.rol);
                   // console.log(response.data.data.user);
                //     const encrypted = this.state.email;
                //  //   console.log(encrypted);  
                //     const ciphertext = CryptoJS.AES.encrypt (encrypted,  'secret' );            
                //     //console.log(usuario);
                //     this.ingresarNameUsuario(ciphertext.toString());
                //     this.setState({ isloggedIn: true });

                   // console.log(this.state.email)
                    
                       //const user = response.data.data;     
                       console.log(response.data)
                       //localStorage.setItem("token"+app,response.data.data.token);
                       //console.log(response.data.data)
                       sessionStorage.setItem("Empresa",this.state.empresa);
                       this.ingresarEmailUsuario(this.state.email);
                       //alert(this.state.empresas);
                    //    this.ingresarCentroUsuario(this.state.empresas);
                       this.ingresarRolUsuario(response.data.data.perfil);
                       //this.ingresarNameUsuario(user);
                       this.setState({ isloggedIn: true });
                  
                } else {
                    this.setState({ isloggedIn: false });   
                }
                })
                .catch(error => {
                      
                        this.setState({ isloggedIn: false });
                         this.setState({ status: "error", message: "Credenciales no válidas" });
                });
     };

     setCompany=(empresa)=>{
        this.setState({empresa:empresa})
        let email=this.state.email;
        let split_user=email.split('@');
        let user=split_user[0];
        axios
        //.post(API_ROOT+"/"+empresa+"/centros_activos",{
        .post(API_ROOT+"/"+empresa+"/centros_usuario",{
            "login_usuarios": user,
            "clave_usuarios": this.state.password
        })
        .then(response => {   
            console.log(response.data.data);
            // let data={
            //     empresa:empresa,
            //     centros:response.data.data
            // }
            //this.ingresarCentroUsuario(data)
            sessionStorage.setItem("Centros",JSON.stringify(response.data.data));
        })
        .catch(error => {
            console.log(error)
        });
     }

  
    render()
     {
        
        let message = "";


        const errorStyle = {
          backgroundColor: "#f5d3d3",
          color: "black",
          textAlign: "center",
          borderRadius: "3px",
          border: "1px solid darkred",
          marginBottom: "15px",
          padding: "5px",
          fontSize: "15px",
          width: "100%"
        };
    
        // if (this.state.isloggedIn) {
        //     return <Redirect to="/dashboards/panelgeneral"></Redirect>
        // }
        if (this.state.isloggedIn) {
            return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }

        if (this.state.status === "error") {
            message = <div style={errorStyle}>{this.state.message}</div>;
        }
        return (

    <Fragment>

        <div className="h-100 bg-heavy-rain bg-animation opacity-11" >  
            <div className="menu-header-image h-100 opacity-9"
                style={{
                    backgroundImage: 'url(' + city3 + ')'
                }}
            >                          
                            
            <div className="d-flex h-100 justify-content-center align-items-center">
                <Col md="8" className="mx-auto app-login-box">
                    <div className="modal-dialog w-100 mx-auto">
                        <div className="modal-content">
                            <div className="modal-body">
                               <div className="app-logo mx-auto mb-3 mt-4"/>
                                <div className="h5 modal-title text-center">
                                    <h4 className="mt-2">
                                        
                                        <div>Bienvenido</div>
                                        <span>Ingrese credenciales de acceso</span>
                                    </h4>
                                </div>
                                <Form>
                                    <Row form>
                                       {message}
                                        <Col md={12}>
                                        <FormGroup className="mt-2">
                                                    <Label for="exampleEmail" className="m-0">Usuario</Label>
                                                    <Input
                                                    type="email"
                                                    name="email"
                                                    id="exampleEmail"
                                                    placeholder="Usuario"
                                                    onChange={this.handleChange}
                                                    onKeyDown={this.handleKeyDown}
                                                 
                                                />
                                                </FormGroup>
                                        </Col>
                                        <Col md={12}>
                                        <FormGroup className="mt-2">
                                                    <Label for="examplePassword" className="m-0">Contraseña</Label>
                                                    <Input
                                                        type="password"
                                                        name="password"
                                                        id="examplePassword"
                                                        placeholder="Contraseña "
                                                        onChange={this.handleChange}
                                                        onKeyDown={this.handleKeyDown}
                                                    />
                                        </FormGroup>
                                        </Col>
                                        {/* <Col md={12}>
                                        <FormGroup className="mt-2">
                                                <Label for="exampleEmpresa" className="m-0">Empresa</Label>
                                                <Input  
                                                 type="select" 
                                                 name="empresas"
                                                //  disabled={this.state.email!="" && this.state.password!=""?false:true}
                                                 onChange={this.handleChange}
                                                 onChange={((e)=>{
                                                    // alert(e.target.value);
                                                    this.setState({empresa:e.target.value})

                                                })} id="exampleSelect">
                                                    <option disabled selected>Seleccione una Empresa</option>
                                                    {
                                                        Empresas.map((data,i)=>{
                                                            return <option value={data.name}>{data.title}</option>
                                                        })
                                                    }
                                                </Input>
                                        </FormGroup>
                                        </Col> */}
                                    </Row>
                                   
                                </Form>
                              
                               
                            </div>
                            <div className="modal-footer clearfix">                              
                                <div className="float-right">
                                                <Button
                                                        onClick={this.handleLogin}
                                                        onKeyDown={this.handleKeyDown}
                                                        color="primary"
                                                        size="lg"
                                                        >
                                                Acceder</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="text-center text-black opacity-10 mt-3">
                        Copyright &copy; IdealControl 2019
                    </div>
                </Col>
            </div>
              <div className=""
               
                        style={{
                            backgroundImage: 'url(' + backgroundImage + ')'
                        }}>
                    </div>
        </div>

        </div>
    </Fragment>
    );
    
    }
}



const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
    centroUsuario: state.Session.centroUsuario,
});

const mapDispatchToProps = dispatch => ({
    setNameUsuario: enable => dispatch(setNameUsuario(enable)),
    setEmailUsuario: enable => dispatch(setEmailUsuario(enable)),
    setRolUsuario: enable => dispatch(setRolUsuario(enable)),
    setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
