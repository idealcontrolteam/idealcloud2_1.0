import { Module } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuarios } from './entity/usuarios.entity';
import { UsuarioController } from './usuario.controller';


@Module({
  imports: [TypeOrmModule.forFeature([Usuarios])],
  providers: [UsuarioService],
  exports: [TypeOrmModule],
  controllers: [UsuarioController]
})
export class UsuarioModule {}
