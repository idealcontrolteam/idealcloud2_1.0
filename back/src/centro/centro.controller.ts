import {
  Controller,
  Post,
  Param,
  Req,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { CentroService } from './centro.service';
import { GLOBAL_URL } from '../global';
import { UsuarioService } from '../usuario/usuario.service';

@Controller(`${GLOBAL_URL}`)
export class CentroController {
  constructor(
    private service: CentroService,
    private usuarioService: UsuarioService,
  ) {}

  @Post('centros')
  async list(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getCentros(params.empresa);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Centros fetched!',
        data: result,
        count: result.length,
      });
    }
  }
  @Post('centros_usuario')
  async list_usuario(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getCentros_Usuario(params.empresa,req.body);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Centros fetched!',
        data: result,
        count: result.length,
      });
    }
  }
  @Post('centros_activos')
  async centros_activos(@Param() params, @Req() req, @Res() res) {
    //console.log(req.body)

    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body
    );

    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getCentrosActivos(params.empresa,userResult[0]);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Centros fetched!',
        data: result,
        count: result.length,
      });
    }
  }
  @Post('centros_inactivos')
  async centros_inactivos(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getCentrosInactivos(params.empresa);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Centros fetched!',
        data: result,
        count: result.length,
      });
    }
  }
  @Post('centros/:id')
  async get(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getCentro(params.empresa, params.id);
      return res.status(HttpStatus.OK).json({
        statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        message: result.length > 0 ? 'Centro fetched!' : 'Not found',
        data: result[0] || null,
      });
    }
  }
}
