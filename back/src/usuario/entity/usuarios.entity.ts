import { Column, PrimaryGeneratedColumn, Entity } from 'typeorm';

@Entity('usuarios')
export class Usuarios {
  @PrimaryGeneratedColumn()
  id_usuarios: number;

  @Column()
  id_empresa: number;

  @Column()
  id_centros: number;

  @Column()
  id_perfiles: number;

  @Column()
  id_zztemas: number;

  @Column()
  nombres_usuarios: string;

  @Column()
  apellido_paterno_usuarios: string;

  @Column()
  apellido_materno_usuarios: string;

  @Column()
  login_usuarios: string;

  @Column()
  clave_usuarios: string;

  @Column()
  telefonos_usuarios: string;

  @Column()
  email_usuarios: string;

  @Column()
  direccion_usuarios: string;

  @Column()
  cargo_usuarios: string;

  @Column()
  activo_usuarios: number;
}
