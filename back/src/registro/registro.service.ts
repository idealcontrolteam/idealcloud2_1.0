import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Registros } from './entity/registros.entity';
import { GLOBAL_BD_PREFIX } from '../global';

@Injectable()
export class RegistroService {
  constructor(
    @InjectRepository(Registros)
    private registroRepository: Repository<Registros>,
  ) {}

  async getRegistrosCentro(empresa:string, dispositivo): Promise<Registros[]> {
    let prefix='';
    let values='';
    console.log(empresa);
    if(empresa=='sasa'){
      prefix='idealcloud_ox_bd';
      values=`value_v1_registros as oxd, 
      value_v2_registros as temp, 
      value_v3_registros as oxs, 
      value_v4_registros as sal`;
    }else{
      prefix=`${GLOBAL_BD_PREFIX}${empresa}`;
      values=`value_v1_registros as oxd, 
      value_v2_registros as oxs, 
      value_v3_registros as temp, 
      value_v4_registros as sal`;
    }
    return await this.registroRepository.query(
      `SELECT
          fecha_registros as dateTime, 
          ${values}
          from ${prefix}.registros WHERE 
          id_dispositivos = ?
          ORDER BY id_registros DESC
          LIMIT 1`,
          [dispositivo]
    );
  }
  async getRegistrosCentroFecha(
    empresa:string,
    dispositivo:number,
    from:string,
    to:string,
  ): Promise<Registros[]> {
    let prefix='';
    let values='';
    if(empresa=='sasa'){
      prefix='idealcloud_ox_bd';
      values=`value_v1_registros as oxd, 
      value_v2_registros as temp, 
      value_v3_registros as oxs, 
      value_v4_registros as sal`;
    }else{
      prefix=`${GLOBAL_BD_PREFIX}${empresa}`;
      values=`value_v1_registros as oxd, 
      value_v2_registros as oxs, 
      value_v3_registros as temp, 
      value_v4_registros as sal`;
    }
    const registros = await this.registroRepository.query(
      `SELECT
        fecha_registros as dateTime,
        ${values}
        from ${prefix}.registros WHERE 
        id_dispositivos = ? AND 
        fecha_registros BETWEEN ? AND ?`,
      [dispositivo, `${from}.000Z`, `${to}.000Z`],
    );
    return registros.filter((item)=>item.oxd>0 && item.oxd<1000 
                          && item.oxs>0 && item.oxd<1000
                          && item.temp>0 && item.oxd<1000)
                    .map((item)=>{
                      var fecha=new Date(item.dateTime);
                      var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
                      zona_horaria=zona_horaria/60;
                      fecha.setHours(fecha.getHours()-zona_horaria);
                      item.dateTime=fecha;
                      return item;
                    });
  }
  async getRegistrosCentroFechaxy(
    empresa:string,
    dispositivo:number,
    from:string,
    to:string,
  ): Promise<Registros[]> {
    let prefix='';
    let values='';
    if(empresa=='sasa'){
      prefix='idealcloud_ox_bd';
      values=`value_v1_registros as oxd, 
      value_v2_registros as temp, 
      value_v3_registros as oxs, 
      value_v4_registros as sal`;
    }else{
      prefix=`${GLOBAL_BD_PREFIX}${empresa}`;
      values=`value_v1_registros as oxd, 
      value_v2_registros as oxs, 
      value_v3_registros as temp, 
      value_v4_registros as sal`;
    }
    const registros = await this.registroRepository.query(
      `SELECT
        fecha_registros as dateTime,
        ${values}
        from ${prefix}.registros WHERE 
        id_dispositivos = ? AND 
        fecha_registros BETWEEN ? AND ? ORDER BY fecha_registros ASC`,
      [dispositivo, `${from}.000Z`, `${to}.000Z`],
    );

    let xy=[];
    registros.filter((item)=>item.oxd>0 && item.oxd<1000 
                          && item.oxs>0 && item.oxd<1000
                          && item.temp>0 && item.oxd<1000).map( item => {
        var fecha=new Date(item.dateTime);
        //var zona_horaria=new Date(item.dateTime).getTimezoneOffset();
        // zona_horaria=zona_horaria/60;
        fecha.setHours(fecha.getHours());

      xy.push({ x: fecha.getTime() , oxd : item.oxd, oxs : item.oxs, temp : item.temp, sal : item.sal });
    });

    return xy;
  }
}
