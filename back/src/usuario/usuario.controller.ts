import {
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Res,
    Req,
    HttpStatus,
    Body,
    Param,
    NotFoundException,
    BadRequestException,
    UseGuards,
  } from '@nestjs/common';
  import { UsuarioService } from './usuario.service';
  import { GLOBAL_URL } from '../global';
//   import * as bcrypt from 'bcryptjs';
//   import * as jwt from 'jsonwebtoken';

@Controller(`${GLOBAL_URL}`)
export class UsuarioController {
    constructor(private usuarioService: UsuarioService) {}

  @Post('login')
  async login(@Param() params, @Req() req, @Res() res) {
    // console.log(params)

    let userResult = await this.usuarioService.validateUser(
        params.empresa,
        req.body,
      );

    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    }else {
        return res.status(HttpStatus.OK).json({
          statusCode: userResult.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
          message: userResult.length > 0 ? 'Registro fetched!' : 'Not found',
          data: userResult[0] || null
        });
        // let result = await this.service.getRegistrosCentro(
        //   params.empresa,
        //   params.code,
        // );
        // return res.status(HttpStatus.OK).json({
        //   statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        //   message: result.length > 0 ? 'Registro fetched!' : 'Not found',
        //   data: result[0] || null
        // });
      }
    
  }
  
  @Post('usuarios')
  async list(@Param() params, @Req() req, @Res() res) {

      let result = await this.usuarioService.getUsuarios(params.empresa);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Usuarios fetched!',
        data: result,
        count: result.length,
      });
    }

  @Post('usuarios_centros')
  async usuarios_centros(@Param() params, @Req() req, @Res() res) {

      let result = await this.usuarioService.getUsuarios_Centros(params.empresa);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Usuarios fetched!',
        data: result,
        count: result.length,
      });
    }
  
    @Post('crear_usuarios_centros')
    async crear_usuarios_centros(@Param() params, @Req() req, @Res() res) {
      let result = await this.usuarioService.createUsuarios_Centros(
        params.empresa,
        req.body,
        );
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Usuarios fetched!',
        data: result,
        count: result.length,
      });
    }

    @Delete('usuarios_centros/:id')
    async delete_usuarios_centros(@Param() params, @Req() req, @Res() res) {
        //console.log(params.id)
        let result = await this.usuarioService.deleteUsuarios_Centros(
          params.empresa,
          params.id,
          );
        return res.status(HttpStatus.OK).json({
          statusCode: HttpStatus.OK,
          message: 'Usuarios fetched!',
          data: result,
          count: result.length,
        });
    }

}
