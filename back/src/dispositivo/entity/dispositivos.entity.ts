import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { Registros } from "../../registro/entity/registros.entity";

@Entity("dispositivos")
export class Dispositivos {
  @PrimaryGeneratedColumn()
  id_dispositivos: number;

  @Column()
  nombre_dispostivos: string;

  @Column()
  descripcion_dispostivos: string;

  @Column()
  activo_dispositivos: number;

  @OneToMany(
    () => Registros,
    registros => registros.id_registros
  )
  registros: Registros[];
}
