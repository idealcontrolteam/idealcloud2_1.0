import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../../api-config'
import Loader from 'react-loaders';
import Media from 'react-media';
import {
    toast

} from 'react-toastify';
import CanvasJSReact from '../../../../../assets/js/canvasjs.react';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText,
    Badge,Breadcrumb, BreadcrumbItem, ButtonGroup
} from 'reactstrap';
//CustomInput,
import '../../../Zonas/style.css'
import {connect} from 'react-redux';

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';

//import ventilador from '../../../../../src/assets/utils/images/misimagenes/fan.png';
//import valvula from '../../../../../src/assets/utils/images/misimagenes/pipe.png';
import { SizeMe } from 'react-sizeme';
import { number } from 'prop-types';
import {TagServices} from '../../../../../services/comun';
import * as _ from "lodash";


//console.log("props: "+this.props.Mensaje)

//console.log("props: "+JSON.stringify(this.props));
const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let TK801 = "5d94ab6b1c064a2120e19059";
//solo esta definido el TK801 los demas solo estan de relleno pertenecen a TK802-712
let TK802 = "5d7a83a5746ff9396c51f14e";
let TK803 = "5d7a83ad746ff9396c51f14f";
let TK804 = "5d7a83b4746ff9396c51f150";
let TK805 = "5d7a83bc746ff9396c51f151";
let TK806 = "5d7a83c4746ff9396c51f152";
let TK807 = "5d7a83d0746ff9396c51f153";
let TK808 = "5d7a83d6746ff9396c51f154";
let TK809 = "5d7a83dc746ff9396c51f155";
let TK810 = "5d7a83e1746ff9396c51f156";
let TK811 = "5d7a83e7746ff9396c51f157";
let TK812 = "5d7a83eb746ff9396c51f158";
// const S3 = "5ced38e9197f5732a44eb517";
let Z2 = "5d94aa611c064a2120e19058";
// const S3 = "5ced38e9197f5732a44eb517";

let sw=false;
let visible="block";

let check=true;
let check2=false;

let contador=0;
let dataChaAxisy= []
let dataCha = []
let Id_Workplace=0;

let empresa=sessionStorage.getItem("Empresa");

class IndexZonaParcelaLarvas extends Component {   
    constructor(props) {
        super(props);
        
        this.state = {   
            myDataChart1: [],   
            NameWorkplace:"",
            ActiveWorkplace:"",
            misTag: [],     
            isLoading: false,
            error: null,
            modal1: false,
            setpointOx:1,
            setpointOxs:[],         
            minOx:3,
            minOxs:[],
            maxOx:0,
            maxOxs:[],
            timeOn:5,
            timeOns:[],
            timeOff:0,
            timeOffs:[],
            alarmTemp:7,
            histeresisOx:0,
            histeresisOxs:[],
            estadoTK:[],
            habilitadoTK:[],
            OxdTK:[],
            Modulos:[],
            dataCharts:[],
            dataAxisy: [],

            activoTemp: true,
            activoHum: true,
            blocking1: false,  
            intentosGuardado: 0,
            check:true,
            check2:false,
            
            bitValTK801:0,
            bitVenTK801:0,
            // bitValTK802:0,
            // bitVenTK802:0,
            // bitValS3:0,
            // bitVenS3:0,
            mySalaModal:0,
            MyTKN:[],
            nCard:[],
            Mis_Sondas:[],
            Mis_Registros:[],
            Sonda_Registro:[],
        }
        this.tagservices = new TagServices();
        this.getSondas = this.getSondas.bind(this);
        // this.loadDataChar = this.loadDataChar.bind(this);
        // this.loadDataTag = this.loadDataTag.bind(this);
        // this.toggleModal1 = this.toggleModal1.bind(this);
        // this.almacenarConfig = this.almacenarConfig.bind(this);
        // this.confirmacionTermino = this.confirmacionTermino.bind(this);
    }
    
  
    componentDidMount = () => {
        // alert(workplace.id)
        console.log(this.props)
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        this.setState({Mis_Registros:[]});
        const intervaloRefresco =240000;
        //const intervaloRefresco =24000;
        if(workplace!=null){
            Id_Workplace=workplace.id;
            //alert(workplace.active)
            this.setState({NameWorkplace:workplace.name});
            this.getSondas(workplace.id);
            this.intervalIdTag1 = setInterval(() => this.getSondas(),intervaloRefresco);
        }
        console.log(this.state.Mis_Sondas);
        let { emailUsuario,centroUsuario } = this.props;
        //alert(JSON.stringify(nameCentro))
        console.log(centroUsuario)
        check=false;
        check2=false;
        // this.getTags();
        this.setState({ isLoading: true });
        // this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        // // this.loadDataChar(); 
        // // this.loadDataTag();
        // this.intervalIdtag = setInterval(() => this.loadDataTag(),intervaloRefresco);
    }

    // componentWillUnmount = () => {
    //    clearInterval(this.componentWillReceiveProps);
    // }  

    // componentDidUpdate(prevProps, prevState){
    //     // alert(JSON.stringify(prevProps)); extrae los states globales de readux
    //     alert(prevProps.centroUsuario);
    // }

    componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:centro[1]});
        if(this.state.NameWorkplace!=centro[1]){
            this.setState({Mis_Registros:[],Mis_Sondas:[]});
            Id_Workplace=centro[0].toString();
            this.getSondas(centro[0].toString());
        }
      }

    getSondas=(id)=>{
        //console.log("wena :"+Id_Workplace);
        const EndPointTag = `${API_ROOT}/${empresa}/sondas/centro/${Id_Workplace}`;
        axios
        .post(EndPointTag)
        .then(response => {
            if(response.data.statusCode==200){
                let Mis_Sondas = response.data.data;
                //alert("sondas :"+JSON.stringify(Mis_Sondas))
                let Modulos=[];
                // let Mis_Registros=[];
                Mis_Sondas.map((data)=>{
                    Modulos.push(data.grupo);
                    this.tagservices.getRegistros(data.code).then(data =>{
                        console.log("code :"+data[0].name)
                        //console.log(this.state.Mis_Registros.filter((d)=>d.code!=data[0].code))
                        this.setState({Mis_Registros:this.state.Mis_Registros.filter((d)=>d.code!=data[0].code&&d.name!=data[0].name).concat(data)})
                    });
                    //Mis_Registros.push(this.getRegistros(data.code));
                })
                Modulos=Modulos.filter(this.onlyUnique);

                // let ordenados=this.state.Mis_Registros;
                // console.log("ordenados2 "+ordenados)
                // ordenados = _.orderBy(ordenados, ['name'],['asc']);
                // console.log("ordenados "+ordenados);
    
               this.setState({ Mis_Sondas,isLoading: false,Modulos});
            }
        })
        .catch(error => {
          console.log(error);
        });
    }

    // getRegistros=(id)=>{
    //     var date = moment().subtract(1, 'hours');
    //     var minute = date.minutes();
    //     //console.log(minute);
    //     //alert(date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm'))
    //     let now = new Date(); 
    //     const f1 = date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    //     const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";    
      
    //     let f1_split=f1.split("T");
    //     let f2_split=f2.split("T");
    //     //const API1 = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
    //     //alert(API1);
    //     //const EndPointTag = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
    //     const EndPointTag=`http://localhost:3009/aqu/registros/sonda/${id}/2019-06-07T00:00:00/2019-06-07T23:59:59`;

    //     axios
    //     .post(EndPointTag,  {
    //         "login_usuarios": "purrutia",
    //         "clave_usuarios": "purrutia"
    //       })
    //     .then(response => {

    //          let Mis_Registros = response.data.data;
    //          //console.log(Mis_Registros.filter((data)=>data.code==sonda.code));
    //         //  let sonda_registro=[];
    //         //  sonda_registro.push({
    //         //     code:sonda.code,
    //         //     grupo:sonda.grupo,
    //         //     name:sonda.name,
    //         //     active:sonda.active,
    //         //     largo:response.data.data.length-1,
    //         //     registros:Mis_Registros
    //         //  });
    //         //  let array=[];
    //         // array.push({
    //         //     id:id_disp,
    //         //     oxd:Mis_Registros[Mis_Registros.length-1].oxd,
    //         //     oxs:Mis_Registros[Mis_Registros.length-1].oxs,
    //         //     temp:Mis_Registros[Mis_Registros.length-1].temp,
    //         //     sal:Mis_Registros[Mis_Registros.length-1].sal,
    //         //     fecha:Mis_Registros[Mis_Registros.length-1].dateTime,
    //         // });

    //         // this.setState({ Mis_Registros });
    //         //console.log(response.data.data)
    //         //  this.getRegistrosLocales(id_disp,Mis_Registros);
    //          return Mis_Registros;
    //          //this.setState({ Mis_Registros });   
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    // }

    // getMinY = (data)=> {
    //     return data.reduce((min, p) => p.lastValue < min ? p.lastValue  : min, data[0].lastValue );
    // }
    toggleModal1(sala) {
        console.log(sala);

        if (this.state.modal1 === false){ 
        
            
                let sa = "";
                let num="";

                switch (sala) {  
                    case "Cabecera":
                        sa = "801";
                        num="01";
                    break;
                    case "802":
                        sa = "802";
                        num="02";
                    break;
                    case "803":
                        sa = "803";
                        num="03";                  
                    break;
                    case "804":
                        sa = "804";
                        num="04";                   
                    break;
                    case "805":
                        sa = "805";
                        num="05";                  
                    break;
                    case "806":
                        sa = "806";
                        num="06";                   
                    break;
                    case "807":
                        sa = "807";
                        num="07";                 
                    break;
                    case "808":
                        sa = "808";
                        num="08";                    
                    break;
                    case "809":
                        sa = "809";
                        num="09";              
                    break;
                    case "810":
                        sa = "810";
                        num="10";                    
                    break;
                    case "811":
                        sa = "811";
                        num="11";                  
                    break;
                    case "812":
                        sa = "812";
                        num="12";                    
                    break;

                    default:
                        sa = "No definido";
                    break;

                }  
                this.setState({
                    mySalaModal:"Cabecera"
                });

                const EndPointTag = `${API_ROOT}/tag`;
                let mysala = "TK" + sa;              
                let tags = [];
                const token = "tokenfalso";
                axios
                .get(EndPointTag, {
                headers: {  
                    'Authorization': 'Bearer ' + token
                }
                })
                .then(response => {
                    tags = response.data.data;  
                    //console.log(tags.filter((tag) => tag.nameAddress === "maxTempS1V"))
                    //console.log(tags.filter((tag) => tag.nameAddress === `M6001TK801N8`)[0].lastValue);
                    try{
                        if(tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N8`)[0].lastValue==1){
                            sw=true;
                            console.log("si "+sw);
                        }else{
                            sw=false;
                            console.log("no "+sw);
                        }
                        this.setState({
                            histeresisOxs : tags.filter((tag) => tag.nameAddress === "D20105TK801N8"),
                            histeresisOx : tags.filter((tag) => tag.nameAddress === "D20105TK801N8")[0].lastValue,
                            setpointOxs: tags.filter((tag) => tag.nameAddress === "D20104TK801N8"),
                            setpointOx: tags.filter((tag) => tag.nameAddress === "D20104TK801N8")[0].lastValue,
                            maxOxs : tags.filter((tag) => tag.nameAddress === "D20106TK801N8"),
                            maxOx : tags.filter((tag) => tag.nameAddress === "D20106TK801N8")[0].lastValue,
                            minOxs: tags.filter((tag) => tag.nameAddress === "D20107TK801N8"),
                            minOx: tags.filter((tag) => tag.nameAddress === "D20107TK801N8")[0].lastValue,
                            timeOffs : tags.filter((tag) => tag.nameAddress === "D20109TK801N8"),
                            timeOff : tags.filter((tag) => tag.nameAddress === "D20109TK801N8")[0].lastValue,
                            timeOns: tags.filter((tag) => tag.nameAddress === "D20108TK801N8"),
                            timeOn: tags.filter((tag) => tag.nameAddress === "D20108TK801N8")[0].lastValue,
                            estadoTK: tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N8`)[0].lastValue,
                            habilitadoTK: tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N8`),     
                            
                            OxdTK: tags.filter((tag) => tag.nameAddress === `D100TK801N8`),
                            
                            modal1: !this.state.modal1,
                            misTag: tags
                        });
                    }catch(e){
                        console.log(e);
                    }
                    
                })
                .catch(error => {
                console.log(error);
                });
            }else{
                this.setState({
                    modal1: !this.state.modal1
                });

            }
    }


    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });   
        
        
        //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            console.log(response);
            console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );      
            if (TagWrites.length === 0){
                clearInterval(this.intervalConfirmacion);
                this.setState({blocking1: false});
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
            }else if (this.state.intentosGuardado >= 10) 
            {
              this.loadDataChar();
              toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
              this.setState({blocking1: false}); 
              clearInterval(this.intervalConfirmacion);
            }
           
        })
        .catch(error => {
          console.log(error);
        });

    }

   


    almacenarConfig = () => {
        console.log(sw);
        this.setState({blocking1: true,intentosGuardado:0}); 

       this.intervalConfirmacion = setInterval(() => this.confirmacionTermino(),3000);
       
       const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };

       let EndPointTag = `${API_ROOT}/tag/${this.state.histeresisOxs[0]._id}`; 
        const  content1 = {
            "lastValue": this.state.histeresisOx,
            "lastValueWrite":this.state.histeresisOx,
            "write":true
        }   
        axios.put(EndPointTag, content1, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.setpointOxs[0]._id}`; 
        const  content2 = {
            "lastValue": this.state.setpointOx,
            "lastValueWrite":this.state.setpointOx,
            "write":true
        }   
        axios.put(EndPointTag, content2, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.maxOxs[0]._id}`; 
        const  content3 = {
            "lastValue": this.state.maxOx,
            "lastValueWrite":this.state.maxOx,
            "write":true
        }   
        axios.put(EndPointTag, content3, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.minOxs[0]._id}`; 
        const  content4 = {
            "lastValue": this.state.minOx,
            "lastValueWrite":this.state.minOx,
            "write":true
        }   
        axios.put(EndPointTag, content4, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.OxdTK[0]._id}`; 
        const  content4_5 = {
            "alertMax": this.state.maxOx,
            "alertMin":this.state.minOx,
            "write":true
        }  
        axios.put(EndPointTag, content4_5, config)
        .then(response => {
            
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.timeOffs[0]._id}`; 
        const  content5 = {
            "lastValue": this.state.timeOff,
            "lastValueWrite":this.state.timeOff,
            "write":true
        }   
        axios.put(EndPointTag, content5, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.timeOns[0]._id}`; 
        const  content6 = {
            "lastValue": this.state.timeOn,
            "lastValueWrite":this.state.timeOn,
            "write":true
        }   
        axios.put(EndPointTag, content6, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

         EndPointTag = `${API_ROOT}/tag/${this.state.habilitadoTK[0]._id}`; 
        let content7="";
        if(sw==true){
            content7 = {
                "lastValue": 1,
                "lastValueWrite":1,
                "write":true            
            }
        }else{
            content7 = {
                "lastValue": 0,
                "lastValueWrite":0,
                "write":true            
            }
        }

        axios.put(EndPointTag, content7, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });    

    const get_address=this.state.habilitadoTK[0].nameAddress.split('TK');
    let address=get_address[1];
    console.log(address);
    let content8="";
    //console.log("sw: "+sw);
    if(!sw){
        //console.log("wenaaa");
         content8={
            "state":-1
        }
    }else{
         content8={
            "state":1
        }
    }
    
    console.log(address);
    let tags_capture=""

    switch (address) {
        case "801N8":
            tags_capture=this.state.myDataChart1.filter((data)=>data.nameAddress.includes(address))
            .map((data)=>{
                EndPointTag = `${API_ROOT}/tag/${data._id}`;
                axios.put(EndPointTag, content8, config)
                .then(response => {
                });
                return data
            })
            break;
    }
    

}
 
    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    checkChangeHandler = (event) => { 
        // console.log(event.target.defaultChecked);  
        this.setState( { 
            ...this.state,
            [event.target.id]: !event.target.defaultChecked
        } );
    }

    getColor(unidad){
        // #FC3939
        //1,,3,2
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if(unidad=="oxd"){
            return miscolores[0]
        }else if(unidad=="temp"){
            return miscolores[1]
        }else if(unidad=="oxs"){
            return miscolores[2]
        }else if(unidad=="sal"){
            return miscolores[3]
        }
        else if(unidad=="oxd_n"){
            return 0
        }else if(unidad=="temp_n"){
            return 1
        }else if(unidad=="oxs_n"){
            return 2
        }else if(unidad=="sal_n"){
            return 3
        }else{
            return ""
        }
    }

    getEndRegister(type,data){
        if(type=="oxd"){
            return data.registros[data.registros.length-1].oxd
        }else if(type=="temp"){
            return data.registros[data.registros.length-1].temp
        }else if(type=="oxs"){
            return data.registros[data.registros.length-1].oxs
        }else if(type=="sal"){
            return data.registros[data.registros.length-1].sal
        }else{
            return ""
        }      
    }

    //genera cada boton para el Oxs, Temp y Oxd
    getButton = (data,i,type,unity)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        try{
            return<div className="widget-chart widget-chart-hover  p-0 p-0 ">
            <Button 
               onClick={ e => {
                   //this.slider1.slickGoTo(this.getColor(type+"_n"))
                   if(this.state.check)
                       if(i==0)
                           this.slider1.slickGoTo(this.getColor(type+"_n"))
                       else if(i==1)
                           this.slider2.slickGoTo(this.getColor(type+"_n"))
                       else if(i==2)
                           this.slider3.slickGoTo(this.getColor(type+"_n"))
                       else if(i==3)
                           this.slider4.slickGoTo(this.getColor(type+"_n"))
                       else if(i==4)
                           this.slider5.slickGoTo(this.getColor(type+"_n"))
                       else if(i==5)
                           this.slider6.slickGoTo(this.getColor(type+"_n"))
                       else if(i==6)
                           this.slider7.slickGoTo(this.getColor(type+"_n"))
                       else if(i==7)
                           this.slider8.slickGoTo(this.getColor(type+"_n"))
                       else if(i==8)
                           this.slider9.slickGoTo(this.getColor(type+"_n"))
                       else if(i==9)
                           this.slider10.slickGoTo(this.getColor(type+"_n"))
                       else if(i==10)
                           this.slider11.slickGoTo(this.getColor(type+"_n"))
                       else if(i==11)
                           this.slider12.slickGoTo(this.getColor(type+"_n"))
               }}
               key={i} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[0]}>  
               {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
               <div className="size-boton mt-0" style={{color:`${this.getColor(type)}`}} > 
               {/* {console.log("registros: "+registros)} */}

               {this.getEndRegister(type,data)}       
               
                   {/* <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span> */}
                   <span className="opacity-6  pl-0 size_unidad"> 
                   {unity}
                   
                    </span>
               </div>
               {/* <div className="widget-subheading">
               {type}
               
               </div> */}
           </Button>                                              
       </div>
        }catch(e){
            console.log(e)
        }
            
    }

    createOption=(dataCharts,dataAxisy)=>{
        //const {dataCharts,dataAxisy} = this.state; 

        let optionsChart1 = {

            
            data: dataCharts,
           
            height:200,
            zoomEnabled: true,
            //exportEnabled: true,
            animationEnabled: false, 
          
            toolTip: {
                shared: true,
                contentFormatter: function (e) {
                    var content = " ";
                    for (var i = 0; i < e.entries.length; i++){
                        content = moment(e.entries[i].dataPoint.x).format("DDMMM HH:mm");       
                     } 
                     content +=   "<br/> " ;
                    for (let i = 0; i < e.entries.length; i++) {
                        // eslint-disable-next-line no-useless-concat
                        content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                        content += "<br/>";
                    }
                    return content;
                }
            },
        //     legend: {
        //       horizontalAlign: "center", 
        //       cursor: "pointer",
        //       fontSize: 11,
        //       itemclick: (e) => {
        //           if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        //               e.dataSeries.visible = false;
        //           } else {
        //               e.dataSeries.visible = true;
        //           }
        //           this.setState({renderChart:!this.state.renderChart});   
                
        //       }
        //   }, 
           
            axisX:{
                 valueFormatString:  "HH:mm",
                 labelFontSize: 10,
        
            },
            axisY :
                dataAxisy.filter((data,i)=>!String(data.id).includes("PSU")&&!String(data.id).includes("%")),
            axisY2 :{
                title:"% - PSU"
            },      
            }
        return optionsChart1
    }
    getMydatachart=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "spline",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             //showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return mydatachart
      }
      getMyaxis=(color,unity)=>{
         let axisy= {
            id:unity,
            title: "Mg/L - °C",
             labelFontSize: 11,                   
            //  lineColor: color,
            //  tickColor: color,
            //  labelFontColor: color,
            //  titleFontColor: color,
            //suffix: " "+unity
            //includeZero: false
           }
           dataChaAxisy.push(axisy);
         return axisy;
      }
      getMydatachart2=(data,name,i,color)=>{
         let mydatachart = {
              axisYIndex: i,
              type: "spline",
              axisYType: "secondary",
              legendText: name,
              name: name,
              color:color,
              dataPoints : data,
              xValueType: "dateTime",
              indexLabelFontSize:"30",
              //showInLegend: true,
              markerSize: 0,  
              lineThickness: 3
               }
               i++;
             dataCha.push(mydatachart);
           return mydatachart
       }
       getMyaxis2=(color,unity)=>{
         let axisy= {    
             id:unity,
             labelFontSize: 11, 
             title: "% - PSU",                
            //  lineColor: color,
            //  tickColor: color,
            //  labelFontColor: color,
            //  titleFontColor: color,
            // suffix: " "+unity
            //includeZero: false
           }
           dataChaAxisy.push(axisy);
         return axisy;
      }
 
      loadDataChart = (data,chartcolor,i) => {   
            dataChaAxisy= []
            dataCha = []

             console.log("punto 2")
             let sonda=data;
             let mediciones=sonda.registros;
             let dataCharts = mediciones.map( item => { 
                 return { x: item.x , y : item.oxd }; 
             });
             let dataCharts3 = mediciones.map( item => { 
                return { x: item.x , y : item.temp }; 
            });
             let dataCharts2 = mediciones.map( item => { 
                 return { x: item.x , y : item.oxs }; 
             });
             let dataCharts4 = mediciones.map( item => { 
                 return { x: item.x , y : item.sal }; 
             });
             console.log(mediciones)
             let mydatachart ={};
             let mydatachart2 ={};
             let mydatachart3 ={};
             let mydatachart4 ={};
             let  axisy= {};
             
             mydatachart=this.getMydatachart(dataCharts,"Oxd",3,chartcolor[0]);
             axisy=this.getMyaxis(chartcolor[0]," ");
             i++;
             mydatachart3=this.getMydatachart(dataCharts3,"Temp",2,chartcolor[1]);
             //axisy=this.getMyaxis(chartcolor[2]," °C");
             i++;
             mydatachart2=this.getMydatachart2(dataCharts2,"Oxs",1,chartcolor[2]);
             axisy=this.getMyaxis2(chartcolor[3]," %");
             i++;
             mydatachart4=this.getMydatachart2(dataCharts4,"Sal",0,chartcolor[3]);
             axisy=this.getMyaxis2(chartcolor[4]," PSU");
             i++;
           
             let option=this.createOption(dataCha,dataChaAxisy);
             return <CanvasJSChart id={data.name} key={data.code} options = {option} className="altografico  "  />
            //   this.setState({
            //       dataAxisy:dataChaAxisy,
            //       dataCharts:dataCha
            //  });   
            //   if (sw === 1)
            //      this.setState({blocking: false}); 
      }
     
    
    //genera el grafico de Oxs, Temp y Oxd
    getChart = (data,i,unity,type)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if(this.state.check2)
            return <Fragment>
            <div>
                    <ResponsiveContainer width="100%" height="100%">
                                {this.loadDataChart(data,miscolores,i)}
                    </ResponsiveContainer>
            </div>
            </Fragment>
        if(this.state.check)
            return <Fragment><div className="widget-chart widget-chart2 text-left p-0" style={{display:`${this.state.check?"block":"none"}`}}>
            <div className="widget-chat-wrapper-outer">
                {console.log(data)}
                <div className="widget-chart-content widget-chart-content-lg  p-2">
                    <div className="widget-chart-flex ">
                        <div
                            className="widget-title opacity-9 text-muted text-uppercase">
                            {/* {data.name} */}
                        </div>
                    </div>

                    <div className="widget-numbers p-1 m-0">
                        <div className="widget-chart-flex">
                            <div>
                            {type!="oxd"?"":data.registros[data.registros.length-1].oxd}  
                            {type!="temp"?"":data.registros[data.registros.length-1].temp}
                            {type!="oxs"?"":data.registros[data.registros.length-1].oxs}
                            {type!="sal"?"":data.registros[data.registros.length-1].sal}  
                                <small className="opacity-5 pl-1 size_unidad3"> {unity}</small>
                            </div>
                        </div>
                    </div> 
                    
                <div className=" opacity-8 text-focus pt-0">
                    <div className=" opacity-5 d-inline">
                    Max
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>  
                                                                                                    
                        <span className="pl-1 size_prom">
                                {type!="oxd"?"":data.registros.reduce((max, b) => Math.max(max, b.oxd), data.registros[0].oxd) } 
                                {type!="temp"?"":data.registros.reduce((max, b) => Math.max(max, b.temp), data.registros[0].temp) }
                                {type!="oxs"?"":data.registros.reduce((max, b) => Math.max(max, b.oxs), data.registros[0].oxs) } 
                                {type!="sal"?"":data.registros.reduce((max, b) => Math.max(max, b.sal), data.registros[0].sal) }                                             
                                {/* data.reduce((max, b) => Math.max(max, b.oxd), data[0].oxd) */}
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline  ml-2">
                    Prom
                    </div>
                    
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>                                                                                  
                        <span className="pl-1 size_prom">
                            {type!="oxd"?"":((data.registros.reduce((a, b) => +a + +b.oxd, 0)/data.registros.length)).toFixed(1) }  
                            {type!="temp"?"":((data.registros.reduce((a, b) => +a + +b.temp, 0)/data.registros.length)).toFixed(1) }
                            {type!="oxs"?"":((data.registros.reduce((a, b) => +a + +b.oxs, 0)/data.registros.length)).toFixed(1) }
                            {type!="sal"?"":((data.registros.reduce((a, b) => +a + +b.sal, 0)/data.registros.length)).toFixed(1) }
                        </span>
                    </div>
                    <div className=" opacity-5 d-inline ml-2">
                    Min
                    </div>
                    <div className="d-inline  pr-1" style={{color:`${this.getColor(type)}`}}>                                                                                  
                        <span className="pl-1 size_prom">
                                {type!="oxd"?"":data.registros.reduce((min, b) => Math.min(min, b.oxd), data.registros[0].oxd) }  
                                {type!="temp"?"":data.registros.reduce((min, b) => Math.min(min, b.temp), data.registros[0].temp) }
                                {type!="oxs"?"":data.registros.reduce((min, b) => Math.min(min, b.oxs), data.registros[0].oxs) }
                                {type!="sal"?"":data.registros.reduce((min, b) => Math.min(min, b.sal), data.registros[0].sal) }
                                {/* { data.reduce((min, b) => Math.min(min, b.oxd), data[0].oxd)} */}
                        </span>
                    </div>
                    
                </div> 
            

                </div>

                <div className="d-inline text-secondary pr-1">                                                                                                                                                                 
                            <span className="pl-1">
                                    { moment(data.registros[data.registros.length-1].dateTime).format('HH:mm DD-MMM')}

                            </span>
                </div>


                <div
                    className="widget-chart-wrapper he-auto opacity-10 m-0">
                    <ResponsiveContainer height={150} width='100%'>

                        <AreaChart data={data.registros}
                                // animationDuration={1}
                                isAnimationActive = {false}
                                showInLegend = {true}
                                margin={{
                                    top: 0,
                                    right:10,
                                    left: -30,
                                    bottom: 0
                                }}>
                            
                                <Tooltip                                                                                                        
                                            labelFormatter={function(value) {
                                                return `${ moment(value).format('HH:mm DD-MMM')}`;
                                                }}
                                            formatter={function(value, name) {
                                            return `${value}`;
                                            }}
                                        />
                                <defs>
                                    <linearGradient id={"colorPv" + this.getColor(type+"_n")} x1="0" y1="0" x2="0" y2="1">
                                        <stop offset="10%" stopColor={`${this.getColor(type)}`} stopOpacity={0.7}/>
                                        <stop offset="90%" stopColor={`${this.getColor(type)}`}stopOpacity={0}/>
                                    </linearGradient>
                                </defs>
                                <YAxis                                                                                    
                                tick={{fontSize: '10px'}}
                                // domain={['dataMin - 4','dataMax + 4']}
                                />
                                {/* <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />  */}
                                <XAxis
                                        dataKey={'x'}                                                                              
                                        hide = {false}
                                        tickFormatter={x => moment(x).format('HH:mm')}
                                        tick={{fontSize: '10px'}}
                                        />
                                <Area type='monotoneX' dataKey={type}
                            
                                    stroke={`${this.getColor(type)}`}
                                    strokeWidth='3'
                                    fillOpacity={1}
                                    fill={"url(#colorPv" + this.getColor(type+"_n") + ")"}/>
                            </AreaChart>

                    </ResponsiveContainer>
                </div>
            </div>
        </div></Fragment>
    }

    //realiza los filtro y la llamada a generar el button para los 3 tags de la card
    //className={cx(data.active ? '' : 'opacity-3')} #FC3939
    getViewButton=(array,i)=>{
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        return array.map((data,i)=>
           <Col sm="6" > 
                <Button 
                onClick={ e => {
                    this.slider1.slickGoTo(i)
                }}
                    className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                    <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                                                                                            
                    {/* {data.oxd}  */}
                        {/* <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span> */}
                        <span className="opacity-6  pl-0 size_unidad">  unidad </span>
                    </div>
                    <div className="widget-subheading">
                    {/* {data.dateTime} */}
                    </div>
                </Button>  
               {/* {this.getButton(data,i)} */}
           </Col>
       )     
    }

    //realiza un switch por separado para optimizar la busqueda del Slider
    switchViewChart=(sonda,ind,settings)=>{
        let slider={};
        // let name_slider=eval("slider"+(ind+1));
        //let slider_interior=eval("slider"+(ind+1)+"="+(eval("this.slider"+(ind+1)+"="+"slider"+(ind+1))));
        
        if(ind==0)
            slider=slider1 => (this.slider1 = slider1)
        else if(ind==1)
            slider=slider2 => (this.slider2 = slider2)
        else if(ind==2)
            slider=slider3 => (this.slider3 = slider3)
        else if(ind==3)
            slider=slider4 => (this.slider4 = slider4)
        else if(ind==4)
            slider=slider5 => (this.slider5 = slider5)
        else if(ind==5)
            slider=slider6 => (this.slider6 = slider6)
        else if(ind==6)
            slider=slider7 => (this.slider7 = slider7)
        else if(ind==7)
            slider=slider8 => (this.slider8 = slider8)
        else if(ind==8)
            slider=slider9 => (this.slider9 = slider9)
        else if(ind==9)
            slider=slider10 => (this.slider10 = slider10)
        else if(ind==10)
            slider=slider11 => (this.slider11 = slider11)
        else if(ind==11)
            slider=slider12 => (this.slider12 = slider12)
        try{
            if(this.state.check){
                return<Slider key={sonda.code} ref={slider} {...settings}>
                    {this.getChart(sonda,ind,"mg/L","oxd")}
                    {this.getChart(sonda,ind,"°C","temp")}
                    {this.getChart(sonda,ind,"%","oxs")}
                    {this.getChart(sonda,ind,"PSU","sal")}
                </Slider>
            }if(this.state.check2){
                return<Fragment key={sonda.code} ref={slider} {...settings}>
                    {this.getChart(sonda,ind,"mg/L","oxd")}
                </Fragment>
            }
        }
        catch(e){
            console.log(e)
        }
            
    }


    getCard=(i,settings,sonda)=>{
      return  <Card className="mb-3 mr-20">
                            <CardHeader className="card-header-tab">
                                <div className="card-header-title font-size-ls text-capitalize font-weight-normal">
                                    <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                    {sonda.name} 
                                    {/* id:{sonda.code} */}
                                    {/* {console.log("son_reg:"+JSON.stringify(this.state.Sonda_Registro))} */}
                                </div>
                                {/* <div className="btn-actions-pane-right text-capitalize">
                                    <span className="d-inline-block">
                                            <Button color="primary" disabled={sonda.active==0?1:0} onClick={() => this.toggleModal1(`Cabecera`)}
                                                outline>
                                                <i className="pe-7s-tools btn-icon-wrapper" ></i>
                                            </Button>
                                        
                                    </span>
                                </div> */}

                                </CardHeader>
                                {/* {console.log(this.getRegistros(myDataChart1.code))} */}
                                <CardBody className="p-0"  >
                                    {this.getCardBody(i,settings,sonda)}
                                </CardBody>
                                    {/* {
                                        this.state.nCard.map((data)=>{
                                            if(data==1)
                                                switch (i) {
                                                    case i=0:
                                                        // this.state.myDataCard1=1?"":"0.3"
                                                        //style={{opacity:`${this.state.myDataCard1==1?"":"0.3"}`}}
                                                        return 
                                                
                                                    default:
                                                        return "";
                                                }
                                        })
                                    }      */}
                            </Card>
    }

    getCardBody=(ind,settings,sonda)=>{
        return <Fragment>
                                {/* <h6 className="text-muted text-uppercase font-size-md opacity-8 pl-3 pt-3 pr-3 pb-1 font-weight-normal">
                                    Valor actual de Sondas
                                    </h6> */}
                                    <Card className="main-card mb-0">
                                        <div className="grid-menu grid-menu-4col">
                                            <Row className="no-gutters">
                                                {/* {console.log("Registros: "+JSON.stringify(this.state.Mis_Registros))} */}
                                                
                                                {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"oxd"," mg/L")}
                                                    </Col>
                                                }
                                                {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"temp"," °C")}
                                                    </Col>
                                                }
                                                {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"oxs"," %")}
                                                    </Col>
                                                }
                                                {
                                                    <Col sm="3"> 
                                                        {this.getButton(sonda,ind,"sal"," PSU")}
                                                    </Col>
                                                }

                                            </Row>
                                        </div>
                                    </Card>
                                    {/* style={{display:`${check?"block":"none"}`}} */}
                                    <div className="p-1 slick-slider-sm mx-auto" >
                                    

                                    {this.switchViewChart(sonda,ind,settings)}

                                    </div>
                                </Fragment>
    }
    
    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }
    
    cardSonda=(settings,modulo)=>{
        let sondas=this.state.Mis_Registros;
        console.log("sondas "+JSON.stringify(sondas))
        sondas=_.orderBy(sondas, ['code'],['asc']);
        sondas=sondas.filter((data)=>!String(data.name).includes("Salinidad"));
        return sondas.map((data,i)=>{
            if(modulo==data.grupo){
                if(empresa=="cam"){
                    return<Col key={i} sm={3} className="animated fadeIn fast">
                        {this.getCard(i,settings,data)}
                    </Col> 
                }else{
                    return<Col key={i} sm={4} className="animated fadeIn fast">
                        {this.getCard(i,settings,data)}
                    </Col> 
                }
            }
        })
    }

    getValidate(id){
        let empresas=JSON.parse(sessionStorage.getItem("Centros"));
        try{
            let active=empresas.filter(data=>data.code==id).map((data)=>{
                return data.active
            })
            //alert(active)
            if(active==1){
                return <Fragment><div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-success">Success</div>(Activo)</Fragment>
            }else{
                return <Fragment><div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>(Inactivo)</Fragment>
            }
        }
        catch(e){
            console.log(e)
        }
        
    }

    render() {
        // var this_url=window.location.href;
        // var n_nave=this_url.split('/');
        
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { isLoading,error} = this.state;
         
          const settings = {
            autoplaySpeed:6000,
            autoplay: false,        
            centerMode: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };
       
        const miscolores= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if (error) {
            return <p>{error.message}</p>;
        }
    
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        

        return (
            <Fragment>     
                {/* {console.log(this.state.Mis_Registros)} */}
                {/* {console.log(this.state.Mis_Sondas)}
                {console.log(this.state.Modulos)} */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Funciones Generales</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Tendecia Online</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace}
                      {/* {alert(this.state.ActiveWorkplace)} */}
                      {this.getValidate(Id_Workplace)}
                      
                      {/* <div style={{marginLeft:5}} className="mb-2 mr-2 badge badge-dot badge-dot-lg badge-secondary">Secondary</div>(Offline) */}
                      </BreadcrumbItem>
                      <Media queries={{ small: { maxWidth: 1158 } }}>
                        {matches =>
                            matches.small ? (
                                <ButtonGroup style={{marginLeft:`${70}%`,marginTop:-20}}>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                                    this.setState({check:true,check2:false})
                                                }}>
                                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                                    this.setState({check:false,check2:false})
                                                }}>-</Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    this.setState({check2:true,check:false})
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                                </Button>
                                </ButtonGroup>
                            ) : (
                                <ButtonGroup style={{marginLeft:`${90}%`,marginTop:-20}}>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={() => {
                                                    this.setState({check:true,check2:false})
                                                }}>
                                                    <i style={{fontSize:20}} className="pe-7s-graph2 btn-icon-wrapper" ></i>
                                                </Button>
                                                <Button color="primary" outline onChange={e => this.inputChangeHandler(e)} onClick={()=>{
                                                    this.setState({check:false,check2:false})
                                                }}>-</Button>
                                                <Button color="primary" outline  onClick={() => {
                                                    this.setState({check2:true,check:false})
                                                }} >
                                                    <i style={{fontSize:20}} className="pe-7s-graph1 btn-icon-wrapper" ></i>
                                                </Button>
                                </ButtonGroup>
                            )
                        }
                        </Media>

                      </Breadcrumb>
                      {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        MODULOS
                                    </div>
                                    
                                </div>
                                <div className="page-title-actions"> 
                                </div>                    
                            </div>
                        </div> */}
                        <Row>
                        <Modal isOpen={this.state.modal1} toggle={() => this.toggleModal1("-1")} className={this.props.className}>
                                                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                    <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion {this.state.mySalaModal}</ModalHeader>
                                                    <ModalBody>

                                                    <Row>     
                                                        <Col xs="3" md="3" lg="3"></Col>
                                                        <Col  xs="6" md="6" lg="6">
                                                            <Card className="main-card mb-3">
                                                                <CardBody>
                                                                    <CardTitle>Habilitado</CardTitle>
                                                                    <Form>
                                                                        <FormGroup className="mt-2">
                                                                            <Label for="setpointOx" className="m-0">Set Point</Label>
                                                                            <InputGroup>                                                                                      
                                                                                <Input   id="setpointOx" defaultValue={this.state.setpointOx}  onChange={e => this.inputChangeHandler(e)}/>
                                                                                <InputGroupAddon addonType="append">                                                                                  
                                                                                <InputGroupText>mg/L</InputGroupText>
                                                                                </InputGroupAddon>
                                                                            </InputGroup>
                                                                        </FormGroup>                                                                           
                                                                        <FormGroup className="mt-2">
                                                                            <Label for="histeresisOx" className="m-0">Histeresis</Label>
                                                                            <InputGroup>                                                                                      
                                                                                <Input   id="histeresisOx" defaultValue={this.state.histeresisOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                <InputGroupAddon addonType="append">                                                                                  
                                                                                <InputGroupText>mg/L</InputGroupText>
                                                                                </InputGroupAddon>
                                                                            </InputGroup>                                                                                 
                                                                        </FormGroup>
                                                                        <FormGroup className="mt-2" style={{display:visible}}>
                                                                            <Label for="maxOx" className="m-0">Alarma Alta de Oxígeno</Label>
                                                                            <InputGroup>                                                                                      
                                                                                <Input   id="maxOx" defaultValue={this.state.maxOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                <InputGroupAddon addonType="append">                                                                                  
                                                                                <InputGroupText>mg/L</InputGroupText>
                                                                                </InputGroupAddon>
                                                                            </InputGroup>                                                                                 
                                                                        </FormGroup>
                                                                        <FormGroup className="mt-2" style={{display:visible}}>
                                                                            <Label for="minOx" className="m-0">Alarma Baja de Oxígeno</Label>
                                                                            <InputGroup>                                                                                      
                                                                                <Input   id="minOx" defaultValue={this.state.minOx}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                <InputGroupAddon addonType="append">                                                                                  
                                                                                <InputGroupText>mg/L</InputGroupText>
                                                                                </InputGroupAddon>
                                                                            </InputGroup>                                                                                 
                                                                        </FormGroup>
                                                                        <FormGroup className="mt-2">
                                                                            <Label for="timeOn" className="m-0">Time On</Label>
                                                                            <InputGroup>                                                                                      
                                                                                <Input  id="timeOn" defaultValue={this.state.timeOn}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                <InputGroupAddon addonType="append">                                                                                  
                                                                                <InputGroupText>Seg</InputGroupText>
                                                                                </InputGroupAddon>
                                                                            </InputGroup>                                                                                 
                                                                        </FormGroup>
                                                                        <FormGroup   className="mt-2">
                                                                            <Label for="timeOff" className="m-0">Time Off</Label>
                                                                            <InputGroup >                                                                                      
                                                                                <Input  id="timeOff" defaultValue={this.state.timeOff}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                <InputGroupAddon addonType="append">                                                                                  
                                                                                <InputGroupText>Seg</InputGroupText>
                                                                                </InputGroupAddon>
                                                                            </InputGroup>                                                                                 
                                                                        </FormGroup>
                                                                        <FormGroup   className="mt-2">
                                                                            <Label for="timeOff" className="m-0">Estado TK</Label>
                                                                            <InputGroup>
                                                                            {
                                                                                this.state.nCard.map((c,i)=>{
                                                                                    if(i==0)
                                                                                        if(sw){
                                                                                            return <label key={i} className="switch">
                                                                                                        <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                            // this.setState({SW:false})
                                                                                                            sw=false;
                                                                                                        })} onChange={e => this.inputChangeHandler(e)} />
                                                                                                        <span className="slider round"></span>
                                                                                                    </label>
                                                                                        }else{
                                                                                            return <label key={i} className="switch">
                                                                                                        <input type="checkbox" id="estadoTK" checked={sw} onClick={(()=>{
                                                                                                            // this.setState({SW:true})
                                                                                                            sw=true;
                                                                                                        })} onChange={e => this.inputChangeHandler(e)}/>
                                                                                                        <span className="slider round"></span>
                                                                                                    </label>
                                                                                        }
                                                                                })
                                                                            }                                                                                    
                                                                            </InputGroup>                                                                                 
                                                                        </FormGroup>
                                                                        {/* <FormGroup  className="mt-3"> 
                                                                            <CustomInput type="checkbox" id="activoTemp" label="Activo" defaultChecked={this.state.activoTemp} onChange={e => this.checkChangeHandler(e)}/>                                                                      
                                                                        </FormGroup> */}
                                                                    </Form>
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        
                                                    </Row>
                                                    
                                                    </ModalBody>
                                                    <ModalFooter>
                                                    
                                                            <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button> 
                                                            <Button color="primary"  onClick={() => this.almacenarConfig()}>Guardar</Button>{' '}
                                                    
                                                    </ModalFooter>
                                                    </BlockUi>
                                                </Modal>
                         
                        </Row>
                        {this.state.Modulos.map((data)=>{
                                return <Card size={12}>
                                            <CardBody>
                                            <CardTitle>{data}</CardTitle>
                                                <Row size={12}>
                                                    {this.cardSonda(settings,data)}
                                                </Row>
                                            </CardBody>
                                        </Card>
                            })}

           
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
  });

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(IndexZonaParcelaLarvas);