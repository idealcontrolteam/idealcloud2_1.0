import {
  Entity, Column,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
// import { Centros } from '../../centro/entity/centros.entity';
// import { Dispositivos } from '../../dispositivo/entity/dispositivos.entity';

@Entity('registros')
export class Registros {
  @PrimaryGeneratedColumn()
  id_registros: number;

  @Column()
  fecha_registros: string;

  // RELACIONES POR IMPLEMENTAR A FUTURO
  // // CENTROS
  // @ManyToOne(type => Centros, centros => centros.id_centros)
  // id_centros: Centros;

  // // DISPOSITIVOS
  // @ManyToOne(type => Dispositivos, dispositivos => dispositivos.id_dispositivos)
  // id_dispositivos: Dispositivos;

  @Column()
  id_centros: number; //ESTA COLUMNA DEBIERA IMPLEMENTARSE COMO RELACION

  @Column()
  id_dispositivos: number; //ESTA COLUMNA DEBIERA IMPLEMENTARSE COMO RELACION

  @Column()
  value_v1_registros: number;

  @Column()
  value_v2_registros: number;

  @Column()
  value_v3_registros: number;

  @Column()
  value_v4_registros: number;
}
