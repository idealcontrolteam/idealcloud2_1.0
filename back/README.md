[![Idealcontrol](idealcontrol.cl/webwp/wp-content/uploads/2018/04/logo-solo-180largo.png)](idealcontrol.cl/webwp/wp-content/uploads/2018/04/logo-solo-180largo.png)

# API Aquachile V1

## Objetivo del documento
Informar al cliente sobre las URL que dispondrá en la presente API, adjuntando además los parámetros y resultados de cada una de ellas

## Endpoints y Validación

**Todos los endpoints requieren enviar por POST un body en formato JSON para validar el usuario**
```
{
  "login_usuarios": string",
  "clave_usuarios": string"
}
```
* _login_usuarios_ : usuario asociado al ingreso del sistema
* _clave_usuarios_ : contraseña asociada a la cuenta de ingreso

**De lo contrario las solicitudes serán rechazadas**
```
{
    "statusCode": 401,
    "message": "Not authorized"
}
```

**Se acepta un máximo de 10 consultas por minuto. Posterior a esto, serán rechazadas**
```
{
    "statusCode": 429,
    "message": "Too many accounts created from this IP, please try again after an minute"
}
```

### Centros

#### Todos los centros
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros
```

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Centros fetched!",
    "data": [
        {
            "code": 1,
            "name": "ESTERO RETROCESO",
            "active": 1
        },
        {
            "code": 2,
            "name": "AUSTRALIS2",
            "active": 0
        },
        ...
    ],
    "count": 6
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de centros
* _count_ : cantidad de centros encontrados

#### Todos los centros activos
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros_activos
```

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros_activos
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Centros fetched!",
    "data": [
        {
            "code": 1,
            "name": "ESTERO RETROCESO",
            "active": 1
        },
        {
            "code": 5,
            "name": "PTO. BROWNE",
            "active": 1
        },
        {
            "code": 6,
            "name": "MUÑOZ GAMERO",
            "active": 1
        }
    ],
    "count": 3
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de centros
* _count_ : cantidad de centros encontrados


#### Todos los centros inactivos
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros_inactivos
```

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros_inactivos
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Centros fetched!",
    "data": [
        {
            "code": 2,
            "name": "AUSTRALIS2",
            "active": 0
        },
        {
            "code": 3,
            "name": "BAHIA BUCKLE",
            "active": 0
        },
        {
            "code": 4,
            "name": "MORGAN",
            "active": 0
        }
    ],
    "count": 3
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de centros
* _count_ : cantidad de centros encontrados


#### Un centro específico
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros/:id
```
* _id_ : id del centro consultado

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/centros/1
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Centro fetched!",
    "data": {
        "code": 1,
        "name": "CAPERA",
        "active": 0
    }
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de centros

### Sondas

#### Todas las sondas activas
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/sondas_activas
```

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/sondas_activas
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Sondas fetched!",
    "data": [
        {
            "code": 1,
            "name": "Sensor Salinidad1 5m",
            "active": 1
        },
        {
            "code": 2,
            "name": "Sensor OD 5m_Mod100",
            "active": 1
        },
        ...
    ],
    "count": 19
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de sondas
* _count_ : cantidad de sondas encontradas

#### Una sonda específica
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/sondas/:id
```
* _id_ : id de la sonda consultada

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/sondas/2
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Sonda fetched!",
    "data": {
        "code": 2,
        "name": "Sensor OD 1m_Mod100",
        "active": 1
    }
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de sondas

#### Todas las sondas correspondientes a un centro
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/sondas/centro/:id
```
* _id_ : id del centro asociado

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/sondas/centro/1
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Sondas fetched!",
    "data": [
        {
            "code": 1,
            "name": "Sensor Salinidad1 5m",
            "active": 0
        },
        {
            "code": 2,
            "name": "Sensor OD 1m_Mod100",
            "active": 1
        },
        ...
    ],
    "count": 8
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de sondas
* _count_ : cantidad de sondas encontradas

### Registros

#### Último registro de una sonda
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/registros/sonda/:id
```
* _id_ : id de la sonda consultada

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/registros/sonda/3
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Registro fetched!",
    "data": {
        "dateTime": "2019-06-07T15:00:00.000Z",
        "oxd": 10.2,
        "oxs": 105,
        "temp": 16.4,
        "sal": 0
    }
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de sondas

#### Todos los registros de una sonda en un rango de fechas
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/registros/sonda/:id/:from/:to
```
* _id_ : id de la sonda consultada
* _from_ : rango inicial en formato YYYY-MM-DD
* _to_ : rango final en formato YYYY-MM-DD. Debe ser posterior al rango inicial y no exceder los 31 días.

##### Ejemplo
```
POST - https://ihc.idealcontrol.cl/mod/apiv1/aqu/registros/sonda/3/2019-06-04/2019-06-07
```

##### Body
```
{
  "login_usuarios": "userapiaqu",
  "clave_usuarios": "351ce56ad8787dc1046e37be223e45c8"
}
```

##### Resultado
```
{
    "statusCode": 200,
    "message": "Registros fetched!",
    "data": [
        {
            "dateTime": "2019-06-07T14:40:01.000Z",
            "oxd": 10.3,
            "oxs": 105,
            "temp": 16,
            "sal": 0
        },
        {
            "dateTime": "2019-06-07T14:45:00.000Z",
            "oxd": 10.3,
            "oxs": 105,
            "temp": 16,
            "sal": 0
        },
        ...
    ],
    "count": 5
}
```
* _satusCode_ : estado de la consulta
* _message_ : mensaje descriptor de la consulta realizada
* _data_ : conjunto de sondas
* _count_ : cantidad de sondas encontradas