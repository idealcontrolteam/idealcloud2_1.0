import {
  Controller,
  Param,
  BadRequestException,
  Post,
  Req,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { RegistroService } from './registro.service';
import { GLOBAL_URL } from '../global';
import * as moment from 'moment';
import { UsuarioService } from '../usuario/usuario.service';
import { DispositivoService } from '../dispositivo/dispositivo.service';
import { Registros } from './entity/registros.entity';

@Controller(`${GLOBAL_URL}/registros/sonda`)
export class RegistroController {
  constructor(
    private service: RegistroService,
    private usuarioService: UsuarioService,
    private dispositivoService: DispositivoService,
  ) {}

  @Post('/:code')
  async ultimo_registro(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getRegistrosCentro(
        params.empresa,
        params.code,
      );
      return res.status(HttpStatus.OK).json({
        statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        message: result.length > 0 ? 'Registro fetched!' : 'Not found',
        data: result[0] || null
      });
    }
  }
  @Post('xy/:code/:from/:to')
  async registrosxy(@Param() params, @Req() req, @Res() res) {

    let dispositivoResult = await this.dispositivoService.getDispositivo(
      params.empresa,
      params.code,
    );
    let dispositivo=JSON.parse(JSON.stringify(dispositivoResult));
    //console.log(dispositivo[0].code);

      if (moment(params.to).diff(moment(params.from), 'days') > 31) {
        throw new BadRequestException(
          'Time range exeeds max allowed, please try less than 31 days',
        );
      }
      let result = await this.service.getRegistrosCentroFechaxy(
        params.empresa,
        params.code,
        params.from,
        params.to,
      );

      let array={
        code:dispositivo[0].code,
        grupo:dispositivo[0].grupo,
        name:dispositivo[0].name,
        active:dispositivo[0].active,
        registros:result,
      }
      //console.log(array)
      return res.status(HttpStatus.OK).json({
        statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        message: result.length > 0 ? 'Registros fetched!' : 'Not found',
        data: [array] || [],
        count: result.length,
      });
  }
  @Post('/:code/:from/:to')
  async registros(@Param() params, @Req() req, @Res() res) {
   
    let dispositivoResult = await this.dispositivoService.getDispositivo(
      params.empresa,
      params.code,
    );
    let dispositivo=JSON.parse(JSON.stringify(dispositivoResult));
    //console.log(dispositivo[0].code);
    
      if (moment(params.to).diff(moment(params.from), 'days') > 31) {
        throw new BadRequestException(
          'Time range exeeds max allowed, please try less than 31 days',
        );
      }
      let result = await this.service.getRegistrosCentroFecha(
        params.empresa,
        params.code,
        params.from,
        params.to,
      );
      let array={
        code:dispositivo[0].code,
        grupo:dispositivo[0].grupo,
        name:dispositivo[0].name,
        active:dispositivo[0].active,
        registros:result,
      }
      //console.log(array)
      return res.status(HttpStatus.OK).json({
        statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        message: result.length > 0 ? 'Registros fetched!' : 'Not found',
        data: [array] || [],
        count: result.length,
      });
  }
}
