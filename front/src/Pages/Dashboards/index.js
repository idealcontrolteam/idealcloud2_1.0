import React, {Fragment} from 'react';
import {Route} from 'react-router-dom';

// DASHBOARDS
import PanelGeneral from './PanelGeneral/IndexPanelGeneral';
// import IndexZona1 from './Zonas/IndexZona1';
//import IndexZona2 from './Zonas/IndexZona2';
import IndexNave from './Comparador/IndexComparador';
import Tendencia from './Tendencia/IndexTendencia';
import Tendencia_Online from './Tendencia/IndexTendencia_Online';
import UsuarioCentros from './Comparador/IndexUsuarioCentros';
// import HistAlarmas from './HistAlarmas/IndexHisAlarmas';
// import HistFallas from './HistFallas/IndexHisFallas';

// Layout
import AppHeader from '../../Layout/AppHeader';
import AppSidebar from '../../Layout/AppSidebar';



// Theme Options
import ThemeOptions from '../../Layout/ThemeOptions';




const Dashboards = ({match}) => (
    <Fragment>
        {/* {alert(JSON.stringify(workplace))} */}
        <ThemeOptions/>
        <AppHeader/>
        <div className="app-main">
            <AppSidebar/>
            <div className="app-main__outer">
                <div className="app-main__inner">
                    <Route path={`${match.url}/panelgeneral`} component={PanelGeneral}/>
                    {/* <Route path={`${match.url}/zona1`} component={IndexZona1}/> */}
                    {/* <Route path={`${match.url}/zona2`} component={IndexZona2}/> */}
                    <Route path={`${match.url}/tendencia`} component={Tendencia}/>
                    <Route path={`${match.url}/tendencia_online`} component={Tendencia_Online}/>
                    <Route path={`${match.url}/comparador`} component={IndexNave}/>
                    <Route path={`${match.url}/usuario_centros`} component={UsuarioCentros} />
                </div>           
            </div>
        </div>
    </Fragment>
);

export default Dashboards;