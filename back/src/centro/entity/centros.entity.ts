import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { Registros } from "../../registro/entity/registros.entity";

@Entity("centros")
export class Centros {
  
  @PrimaryGeneratedColumn()
  id_centros: number;

  @Column()
  nombre_centros: string;

  @Column()
  nombre_rasp_centros: string;

  @Column()
  time_read_centros: string;
  
  @Column()
  time_write_centros: string;
  
  @Column()
  time_register_centros: string;

  @Column()
  ip_rasp_centros: string;

  @Column()
  activo_centros: number;

  @OneToMany(
    () => Registros,
    registros => registros.id_registros
  )
  registros: Registros[];
}
