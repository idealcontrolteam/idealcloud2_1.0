import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import { Redirect} from 'react-router-dom';

import CanvasJSReact from '../../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../../services/comun';

import ReactTable from "react-table";
import "react-table/react-table.css";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");

class Tendencia extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);       
        this.filtrar =this.filtrar.bind(this);
        this.filtrar2 =this.filtrar2.bind(this);
        this.loadDataChart =this.loadDataChart.bind(this);
        
        
    }

  
    componentDidMount = () => {
        // this.tagservices.getDataTag().then(data => 
        //     this.setState({Tags: data})
        // );  
        //this.this_Centro=setInterval(()=>this.getIdCentro(),2000);
        //var data = JSON.parse(sessionStorage.getItem('workplace'));
        // alert(JSON.stringify(data.id));
        // this.getIdCentro();
        this.setState({redirect:false})
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            this.getSondas(workplace.id);
        }
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
        
    } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        this.getSondas(centro[0].toString());
        this.setState({redirect:true})
      }

      getSondas=(id)=>{
        const EndPointTag = `${API_ROOT}/${empresa}/sondas/centro/${id}`;

        axios
        .post(EndPointTag)
        .then(response => {

            let data = response.data.data;
            //let Modulos=[];
            // data.map((data)=>{
            //     Modulos.push(data.grupo);
            // })
            //Modulos=Modulos.filter(this.onlyUnique);
            console.log(response.data.data)

           this.setState({isLoading: false,Tags: data});   
        })
        .catch(error => {
          console.log(error);
        });
    }
    //  getIdCentro=()=>{
    //     let url = window.location.href;
    //     //console.log(alert);
    //     let s=url.split('/');
    //     console.log(s[s.length-1])

    //     let now = new Date(); 
    //     const f1 = moment(now).subtract(360, "minutes").format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    //     const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";  
    //     const token = "tokenFalso";

    //     axios.get(`${API_ROOT}/measurement/xy/tag/${s[s.length-1]}/${f1}/${f2}`, {
    //          'Authorization': 'Bearer ' + token
    //     })
    //     .then(response => {
    //         const measurements = response.data.data;
    //         console.log(measurements);
    //         //this.setState({ dateTimeRefresh:measurements[measurements.length-1].x});
    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    //     return s[s.length-1]
    //  }

     getMydatachart=(data,name,i,color)=>{
       let mydatachart = {
            axisYIndex: i,
            type: "spline",
            legendText: name,
            name: name,
            color:color,
            dataPoints : data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,  
            lineThickness: 3
             }
             i++;
           dataCha.push(mydatachart);
         return
     }
     getMyaxis=(color,unity)=>{
        let axisy= {    
            id:unity,
            title: "Mg/L - °C",
            labelFontSize: 11,                   
            // lineColor: color,
            // tickColor: color,
            // labelFontColor: color,
            // titleFontColor: color,
            // suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }
     getMydatachart2=(data,name,i,color)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "spline",
             axisYType: "secondary",
             legendText: name,
             name: name,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return
      }
      getMyaxis2=(color,unity)=>{
        let axisy= {
            id:unity,
            labelFontSize: 11,                   
            lineColor: color,
            tickColor: color,
            labelFontColor: color,
            titleFontColor: color,
            suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

     loadDataChart = (shortName, URL,chartcolor,sw) => {   
         
       // this.setState({blocking: true});
        const token = "tokenFalso";
              
        axios
          .post(URL)
         .then(response => { 
            console.log("punto 2")
            let sonda=response.data.data;
            let mediciones=sonda[0].registros;
            let dataCharts = mediciones.map( item => { 
                return { x: item.x , y : item.oxd }; 
            });
            let dataCharts3 = mediciones.map( item => { 
                return { x: item.x , y : item.temp }; 
            });
            let dataCharts2 = mediciones.map( item => { 
                return { x: item.x , y : item.oxs }; 
            });
            let dataCharts4 = mediciones.map( item => { 
                return { x: item.x , y : item.sal }; 
            });
            console.log(mediciones)
            let mydatachart ={};
            let  axisy= {};
            
             this.getMydatachart(dataCharts,"O2 Disuelto",3,chartcolor[0]);
             axisy=this.getMyaxis(chartcolor[0]," ");
             i++;
             this.getMydatachart(dataCharts3,"Temp",2,chartcolor[1]);
             //axisy=this.getMyaxis(chartcolor[2]," °C");
             i++;
             this.getMydatachart2(dataCharts2,"O2 SAT",1,chartcolor[2]);
             axisy=this.getMyaxis2(chartcolor[3]," %");
             i++;
             this.getMydatachart2(dataCharts4,"Sal",0,chartcolor[3]);
             axisy=this.getMyaxis2(chartcolor[4]," PSU");
             i++;
            // mydatachart = {
            //     axisYIndex: i,
            //     type: "spline",
            //     legendText: "Oxd",
            //     name: "Oxd",
            //     color:chartcolor[0],
            //     dataPoints : dataCharts,
            //     xValueType: "dateTime",
            //     indexLabelFontSize:"30",
            //     showInLegend: true,
            //     markerSize: 0,  
            //     lineThickness: 3
            //      }
                
                // axisy= {    
                //     labelFontSize: 11,                   
                //     lineColor: chartcolor[0],
                //     tickColor: chartcolor[0],
                //     labelFontColor: chartcolor[0],
                //     titleFontColor: chartcolor[0],
                //     suffix: " mg/L"
                //    // includeZero: false
                //   }
               //dataChaAxisy.push(axisy);    
               //dataCha.push(mydatachart);

            //    mydatachart = {
            //     axisYIndex: i,
            //     type: "spline",
            //     legendText: "Oxs",
            //     name: "Oxs",
            //     color:chartcolor[1],
            //     dataPoints : dataCharts2,
            //     xValueType: "dateTime",
            //     indexLabelFontSize:"30",
            //     showInLegend: true,
            //     markerSize: 0,  
            //     lineThickness: 3
            //      }
            //      i++;

            //     axisy= {    
            //         labelFontSize: 11,                   
            //         lineColor: chartcolor[1],
            //         tickColor: chartcolor[1],
            //         labelFontColor: chartcolor[1],
            //         titleFontColor: chartcolor[1],
            //         suffix: " %"
            //        // includeZero: false
            //       }
                
            //    dataChaAxisy.push(axisy);    
            //    dataCha.push(mydatachart);

            //    mydatachart = {
            //     axisYIndex: i,
            //     type: "spline",
            //     legendText: "Temp",
            //     name: "Temp",
            //     color:chartcolor[3],
            //     dataPoints : dataCharts3,
            //     xValueType: "dateTime",
            //     indexLabelFontSize:"30",
            //     showInLegend: true,
            //     markerSize: 0,  
            //     lineThickness: 3
            //      }
            //      i++;

            //     axisy= {    
            //         labelFontSize: 11,                   
            //         lineColor: chartcolor[3],
            //         tickColor: chartcolor[3],
            //         labelFontColor: chartcolor[3],
            //         titleFontColor: chartcolor[3],
            //         suffix: " °C"
            //        // includeZero: false
            //       }
                
            //    dataChaAxisy.push(axisy);    
            //    dataCha.push(mydatachart);
               
          
             this.setState({
                 dataAxisy:dataChaAxisy,
                 dataCharts:dataCha
            });  
            //                 dataExport:dataChaExport,
            //   this.setState({dataExport:dataChaExport});  
            //   this.setState({dataCharts:dataCha});  
             if (sw === 1)
                this.setState({blocking: false}); 

                // console.timeEnd('loop2');
         })
         .catch(error => {
        //    console.log(error);
         });
         
 
     }
    

    filtrar =() => {

  
        const {TagsSelecionado} = this.state;

        //this.setState({dataExport:[]});
        console.log(TagsSelecionado)
        if (TagsSelecionado !== null){           
 
            const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            let f1_split=f1.split("T");
            let f2_split=f2.split("T");

            dataChaAxisy = [] ;
            dataCha = [] ;
            dataChaExport = [];
            i = 0;
            const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
           
            // console.time('loop');
            for (let i = 0; i < TagsSelecionado.length; i++) {
              this.setState({blocking: true});      
              //const APItagMediciones = `${API_ROOT}/measurement/xy/tag/${TagsSelecionado[i]._id}/${f1}/${f2}`;
              //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
              const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/${f1_split[0]}T00:00:00/${f2_split[0]}T23:59:59`;
    
              let sw =0
              if (i=== TagsSelecionado.length- 1)
                sw = 1;
                console.log("punto 1")
              this.loadDataChart(TagsSelecionado[i].name,APItagMediciones, ColorChart,sw);
            }
            // console.timeEnd('loop');
            
     
         
         //   console.log (dataChaAxisy);

        }
      
   
    }

    //carga los datos para la Exportación de la tendencia
    loadDataChart2 = (shortName, URL) => {   
         
        // this.setState({blocking: true});
         const token = "tokenFalso";
               
         axios
           .post(URL)
          .then(response => { 
              const dataChart = response.data.data;
              let mediciones=dataChart[0].registros;
              console.log(mediciones);
             //  this.setState({gridDataChart:dataChart});  
              
            // console.log(dataChart); 
            //  console.time('loop2');
              let _dataCharts = mediciones.map( item => { 
                 return { x: moment(item.dateTime.substr(0,19)) , y : item.oxd }; 
               });
 
              //console.log(_dataCharts);
               
               let _dataChartsExport = mediciones.map( item => { 
                 return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , oxd : item.oxd.toString().replace(".",",") 
                                                                                           , temp : item.temp.toString().replace(".",",")
                                                                                           , oxs : item.oxs.toString().replace(".",",")
                                                                                           , sal : item.sal.toString().replace(".",",")
                        }; 
               });
               console.log(_dataChartsExport);
 
              let mydatachartExport = {  
                     unity : "Mg/L",
                     unity2 : "°C",
                     unity3 : "%",
                     unity4 : "PSU",              
                     sonda: shortName,
                     measurements : _dataChartsExport
                      }
                //dataChaAxisy.push(axisy);  
                 dataChaExport.push(mydatachartExport);   
                //dataCha.push(mydatachart);  
           
              this.setState({
                dataExport:dataChaExport
             });
            //  console.log("dataExport: ",dataChaExport);

             //                 dataExport:dataChaExport,
             //   this.setState({dataExport:dataChaExport});  
             //   this.setState({dataCharts:dataCha});  
 
                 //console.timeEnd('loop2');
                 //this.filtrar2();
          })
          .catch(error => {
            // console.log(error);
          });
          
  
      }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =() => {
        
        this.setState({buttonExport:'none'});
   
         const TagsSelecionado = this.state.TagsSelecionado;
 
         
         if (TagsSelecionado !== null){

            //this.filtrar();
  
             const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             let f1_split=f1.split("T");
             let f2_split=f2.split("T");
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;

             const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            
            //  console.time('loop');
             for (let i = 0; i < TagsSelecionado.length; i++) {
               //this.setState({blocking: true});      
               const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/${TagsSelecionado[i].code}/${f1_split[0]}T00:00:00/${f2_split[0]}T23:59:59`;

              console.log(APItagMediciones);
     
               this.loadDataChart2(TagsSelecionado[i].name,APItagMediciones);
             }
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        isMulti
                        name="colors"
                        options={Tags}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Tag"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

    renderPickerAutoApply=(ranges, local, maxDate)=>{  
        let value1 = this.state.start.format("DD-MM-YYYY") 
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>
              
            </DateTimeRangeContainer>   
          </div>
        );
      }
 
    render() {
        if(this.state.redirect){
          return <Redirect to="/dashboards/tendencia_online"></Redirect>
        }
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
         
 
         let optionsChart1 = {

            
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMM HH:mm");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                dataAxisy.filter((data,i)=>!String(data.id).includes("PSU")&&!String(data.id).includes("%")),
             axisY2 :{
                title:"% - PSU"
            },   
            
             }
         
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Funciones por Centro</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Tendencia Historica</BreadcrumbItem>
                      <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem>
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                    <Row>
                                        <Col   md={12} lg={3}> 
                                            {this.renderPickerAutoApply(ranges, local, maxDate)}   
                                        </Col>
                                        <Col md={12}  lg={8}>
                                            {this.renderSelecTag(this.state.Tags)}
                                        </Col>
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                             this.filtrar();
                                                             }}
                                                >Filtrar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                              



                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                        <BlockUi tag="div" blocking={this.state.blocking} 
                                    loader={<Loader active type={this.state.loaderType}/>}>
                                    <Card className="main-card mb-1 p-0">
                                        <CardBody className="p-3">                                 
                                            <Row>
                                                <Col   md={12} lg={12}> 
                                                
                                                    <ResponsiveContainer height='100%' width='100%' >
                                                        <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                                    </ResponsiveContainer>   
                                                </Col> 
                                            </Row>
                                        </CardBody>
                                    </Card>
                            </BlockUi>

                        </Col>

                    </Row>
                    <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                            <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%` }}
                                onClick={() => {  
                                        this.filtrar2();
                                        // console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button>
                    </Col>
                    <Col sm="4"></Col>
                    
                    
                    {/* {console.time("loop 3")} */}
                    
                    {
                                                          
                         dataExport
                            .map(data=>(
                                  
                                  <Col md="6" key={data.sonda}  >
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        <CSVLink
                                                                        
                                                                        separator={";"}                                                                   
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: "O2 Disuelto ("+ data.unity + ")", key: "oxd" },
                                                                                { label: "Temp ("+ data.unity2 + ")", key: "temp" },
                                                                                { label: "O2 Disuelto ("+ data.unity3 + ")", key: "oxs" },
                                                                                { label: "Sal ("+ data.unity4 + ")", key: "sal" }
                                                                            ]
                                                                        
                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                                    </CSVLink>
                                                </div>
                                                                                 
                                                </CardHeader>
                                                <CardBody className="p-10 m-10">                                 
                                                
                                                        <ReactTable                                                           
                                                            data={
                                                                
                                                                data.measurements}
                                                        
                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"
                                                                  
                                                                    },
                                                                    {
                                                                    Header: "O2 Disuelto ("+ data.unity+ ")",
                                                                    accessor: "oxd",                                                              
                                                                    width: 140
                                                                    },
                                                                    {
                                                                      Header: "Temp ("+ data.unity2+ ")",
                                                                      accessor: "temp",                                                              
                                                                      width: 140
                                                                    },
                                                                    {
                                                                      Header: "O2 SAT ("+ data.unity3+ ")",
                                                                      accessor: "oxs",                                                              
                                                                      width: 140
                                                                    },
                                                                    {
                                                                      Header: "Sal ("+ data.unity4+ ")",
                                                                      accessor: "sal",                                                              
                                                                      width: 140
                                                                    }
                                                                ]                                                             
                                                            }
                                                            
                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>
                         
                         )
        
                            )
                            
                    }
                    {/* {console.timeEnd("loop 3")} */}
                                       

                    </Row>


                 
             
            
                                 

          

                    

               


                                                                 
                                                    
   

            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Tendencia);
