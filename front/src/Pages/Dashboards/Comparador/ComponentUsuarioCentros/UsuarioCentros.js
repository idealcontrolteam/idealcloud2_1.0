import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';
import {
  toast
} from 'react-toastify';

import CanvasJSReact from '../../../../assets/js/canvasjs.react';
import {Row, Form, Label, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup,Breadcrumb, BreadcrumbItem,
    Modal, ModalHeader, ModalBody, ModalFooter,CardTitle} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../services/comun';

import ReactTable from "react-table";
import "react-table/react-table.css";
import { CSVLink } from "react-csv";

import {connect} from 'react-redux';
import { clearAsyncError } from 'redux-form';

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let empresa=sessionStorage.getItem("Empresa");
let centros=JSON.parse(sessionStorage.getItem("Centros"));
//let Seleccion={fecha_inicio:"",fecha_fin:"",centro:"",centro_id:"",sensor:"",sensor_id:"",variable:""};
let centro="";
let sensor="";
let variable="";
let centro_usuario="";
let nombres_usuarios="";
let nombre_centros="";
let id_centro="0";
let id_usuario="0";
//let Seleccionadas=[];


class UsuarioCentros extends Component {

    
    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");
  
        this.state = {       
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          dataAxisy: [],
          NameWorkplace:"",
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none',
          Divices:[],
          Usuarios:[],
          Usuarios_Centros:[],
          Variables:[
            "Oxigeno Disuelto",
            "Temperatura",
            "Saturación",
            "Salinidad"
          ],
          Seleccionadas:[],
          modal1: false,
          modal2: false,
          selectValue:'0',
          selectValue_usu:'0',
          selectValue_cent:'0',
        };     
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);       
        this.filtrar =this.filtrar.bind(this);
        this.filtrar2 =this.filtrar2.bind(this);
        this.loadDataChart =this.loadDataChart.bind(this);
        
        
    }

  
    componentDidMount = () => {
        // this.tagservices.getDataTag().then(data => 
        //     this.setState({Tags: data})
        // );  
        //this.this_Centro=setInterval(()=>this.getIdCentro(),2000);
        //var data = JSON.parse(sessionStorage.getItem('workplace'));
        // alert(JSON.stringify(data.id));
        // this.getIdCentro();
        this.getUsuarios();
        this.getUsuarios_Centros();
        let workplace=JSON.parse(sessionStorage.getItem("workplace"));
        if(workplace!=null){
            //alert(workplace.id)
            this.setState({NameWorkplace:{id:workplace.id,name:workplace.name}});
            //this.getSondas(workplace.id);
        }
      }

      componentWillUnmount() {
        // fix Warning: Can't perform a React state update on an unmounted component
        this.setState = (state,callback)=>{
            return;
        };
    } 

      componentWillReceiveProps(nextProps){
        if (nextProps.initialCount && nextProps.initialCount > this.state.count){
          this.setState({
            count : nextProps.initialCount
          });
        }
        //alert(JSON.stringify(nextProps));
        let separador=nextProps.centroUsuario;
        let centro=separador.split('/');
        this.setState({NameWorkplace:{id:centro[0].toString(),name:centro[1]}});
        //this.getSondas(centro[0].toString());
      }

      // getSondas=(id)=>{
      //     const EndPointTag = `${API_ROOT}/${empresa}/sondas/centro/${id}`;

      //     axios
      //     .post(EndPointTag,  {
      //         "login_usuarios": "purrutia",
      //         "clave_usuarios": "purrutia"
      //       })
      //     .then(response => {

      //         let data = response.data.data;
      //         //console.log(response.data.data)

      //       //this.setState({isLoading: false,Tags: data});   
      //       this.setState({Divices: data});
      //     })
      //     .catch(error => {
      //       console.log(error);
      //     });
      // }
      getUsuarios=()=>{
        const EndPointTag = `${API_ROOT}/${empresa}/usuarios`;

        axios
        .post(EndPointTag)
        .then(response => {

            let data = response.data.data;
            //console.log(response.data.data)

          //this.setState({isLoading: false,Tags: data});   
          this.setState({Usuarios: data});
        })
        .catch(error => {
          console.log(error);
        });
    }

     getUsuarios_Centros=()=>{
        const EndPointTag = `${API_ROOT}/${empresa}/usuarios_centros`;

        axios
        .post(EndPointTag)
        .then(response => {

            let data = response.data.data;
            //console.log(response.data.data)

          //this.setState({isLoading: false,Tags: data});   
          this.setState({Usuarios_Centros: data});
        })
        .catch(error => {
          console.log(error);
        });
     }
    
     getMydatachart=(data,name,i,color,leyenda)=>{
       let mydatachart = {
            axisYIndex: i,
            type: "line",
            legendText: leyenda,
            name: name+" "+leyenda,
            color:color,
            dataPoints : data,
            xValueType: "dateTime",
            indexLabelFontSize:"30",
            showInLegend: true,
            markerSize: 0,  
            lineThickness: 3
             }
             i++;
           dataCha.push(mydatachart);
         return
     }
     getMyaxis=(color,unity)=>{
        let axisy= {    
            id:unity,
            //title: "Mg/L - °C",
            labelFontSize: 11,                   
            // lineColor: color,
            // tickColor: color,
            // labelFontColor: color,
            // titleFontColor: color,
            // suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }
     getMydatachart2=(data,name,i,color,leyenda)=>{
        let mydatachart = {
             axisYIndex: i,
             type: "line",
             axisYType: "secondary",
             legendText: leyenda,
             name: name+" "+leyenda,
             color:color,
             dataPoints : data,
             xValueType: "dateTime",
             indexLabelFontSize:"30",
             showInLegend: true,
             markerSize: 0,  
             lineThickness: 3
              }
              i++;
            dataCha.push(mydatachart);
          return
      }
      getMyaxis2=(color,unity)=>{
        let axisy= {
            id:unity,
            labelFontSize: 11,                   
            lineColor: color,
            tickColor: color,
            labelFontColor: color,
            titleFontColor: color,
            suffix: " "+unity
           // includeZero: false
          }
          dataChaAxisy.push(axisy);
        return axisy;
     }

     loadDataChart = (name, URL,chartcolor,sw,variable) => {   
         
       // this.setState({blocking: true});
        const token = "tokenFalso";
              
        axios
          .post(URL, {
            "login_usuarios": "purrutia",
            "clave_usuarios": "purrutia"
          })
         .then(response => { 
            //console.log("punto 2")
            let sonda=response.data.data;
            let mediciones=sonda[0].registros;
            //console.log(mediciones)
            let  axisy= {};

            if(i==0){
              axisy= {    
                title: "Mg/L - °C",
                labelFontSize: 11,                   
              }
              dataChaAxisy.push(axisy);
            }
            name=name.replace("Sensor OD", " ");
            if(variable=="Oxigeno Disuelto"){
              let dataCharts = mediciones.map( item => { 
                  return { x: item.x , y : item.oxd }; 
              });
              this.getMydatachart(dataCharts,"Oxd",3,chartcolor,name);
              //axisy=this.getMyaxis(chartcolor," ");
              i++;}

            if(variable=="Temperatura"){
              let dataCharts3 = mediciones.map( item => { 
                  return { x: item.x , y : item.temp }; 
              });
              this.getMydatachart(dataCharts3,"Temp",2,chartcolor,name);
              //axisy=this.getMyaxis(chartcolor[2]," °C");
              i++;}

            if(variable=="Saturación"){
              let dataCharts2 = mediciones.map( item => { 
                  return { x: item.x , y : item.oxs }; 
              });
              this.getMydatachart2(dataCharts2,"Oxs",1,chartcolor,name);
              //axisy=this.getMyaxis2(chartcolor," %");
              i++;}

            if(variable=="Salinidad"){
              let dataCharts4 = mediciones.map( item => { 
                  return { x: item.x , y : item.sal }; 
              });
              this.getMydatachart2(dataCharts4,"Sal",0,chartcolor,name);
              //axisy=this.getMyaxis2(chartcolor," PSU");
              i++;}

            //console.log(mediciones)
            let mydatachart ={};
          
             this.setState({
                 dataAxisy:dataChaAxisy,
                 dataCharts:dataCha
            });  
            //                 dataExport:dataChaExport,
            //   this.setState({dataExport:dataChaExport});  
            //   this.setState({dataCharts:dataCha});  
             if (sw === 1)
                this.setState({blocking: false}); 

                // console.timeEnd('loop2');
         })
         .catch(error => {
        //    console.log(error);
         });
         
 
     }
    

    filtrar =() => {

  
        const {Seleccionadas} = this.state;

        //this.setState({dataExport:[]});
        //console.log(Seleccionadas)
        if (Seleccionadas !== null){           
 
            const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            let f1_split=f1.split("T");
            let f2_split=f2.split("T");

            dataChaAxisy = [] ;
            dataCha = [] ;
            dataChaExport = [];
            i = 0;
            const ColorChart= ["DODGERBLUE","#FC3939","#F3B415","#13B955","DARKBLUE","FORESTGREEN","#CD5C5C","#EFA31D","#009CDC","#d1bcf6","#593196"];
            let centro_sel=[];
            Seleccionadas.map((data)=>{
                centro_sel.push(data.centro)
            })
            centro_sel=centro_sel.filter(this.onlyUnique).map((data,i)=>{return {centro:data,ind:i}});

            // console.time('loop');
            for (let i = 0; i < Seleccionadas.length; i++) {
              let centro_color=centro_sel.filter((data)=>data.centro==Seleccionadas[i].centro)
              .map((data)=>{return ColorChart[data.ind] });
              //console.log(centro_color)

              this.setState({blocking: true});      
              //const APItagMediciones = `${API_ROOT}/measurement/xy/tag/${TagsSelecionado[i]._id}/${f1}/${f2}`;
              const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${Seleccionadas[i].id_sensor}/${f1_split[0]}T00:00:00/${f2_split[0]}T23:59:59`;
              //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${Seleccionadas[i].id_sensor}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
    
              let sw =0
              if (i=== Seleccionadas.length- 1)
                sw = 1;
                //console.log("punto 1")
              this.loadDataChart(Seleccionadas[i].sensor,APItagMediciones, centro_color,sw,Seleccionadas[i].variable);
            }
            // console.timeEnd('loop');
            
     
         
         //   console.log (dataChaAxisy);

        }
      
   
    }
    onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    //carga los datos para la Exportación de la tendencia
    loadDataChart2 = (shortName, URL ,unity) => {   
         
        // this.setState({blocking: true});
         const token = "tokenFalso";
               
         axios
           .get(URL, {
            headers: {  
              'Authorization': 'Bearer ' + token
             }
           })
          .then(response => { 
              const dataChart = response.data.data;
            //   console.log(dataChart);
             //  this.setState({gridDataChart:dataChart});  
              
            // console.log(dataChart); 
            //  console.time('loop2');
              let _dataCharts = dataChart.map( item => { 
                 return { x: moment(item.dateTime.substr(0,19)) , y : item.value }; 
               });
 
                //console.log(dataChart);
               
               let _dataChartsExport = dataChart.map( item => { 
                 return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , y : item.value.toString().replace(".",",") }; 
               });
 
              let mydatachartExport = {  
                     unity : unity,              
                     sonda: shortName,
                     measurements : _dataChartsExport
                      }
                 
                //dataChaAxisy.push(axisy);  
                 dataChaExport.push(mydatachartExport);   
                //dataCha.push(mydatachart);  
           
              this.setState({
                dataExport:dataChaExport
             });

          })
          .catch(error => {
            // console.log(error);
          });
          
  
      }
     
      //filtro 2 necesario para la exportación de tendencia
      filtrar2 =() => {
        
        this.setState({buttonExport:'none'});
   
         const TagsSelecionado = this.state.TagsSelecionado;
 
         
         if (TagsSelecionado !== null){

            //this.filtrar();
  
             const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             //let f1_split=f1.split("T");
            // let f2_split=f2.split("T");
 
             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;
            
            //  console.time('loop');
             for (let i = 0; i < TagsSelecionado.length; i++) {
               //this.setState({blocking: true});      
               //const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/${f1_split[0]}T00:00:00/${f2_split[0]}T23:59:59`;
               const APItagMediciones = `${API_ROOT}/${empresa}/registros/sonda/xy/${TagsSelecionado[i].code}/2019-06-07T00:00:00/2019-06-07T23:59:59`;

            //    console.log(APItagMediciones);
     
               this.loadDataChart2(TagsSelecionado[i].shortName,APItagMediciones,TagsSelecionado[i].unity);
             }
            //  console.timeEnd('loop');
             
      
          
          //   console.log (dataChaAxisy);
 
         }
       
    
     }

    applyCallback = (startDate, endDate) =>{      
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {   
         
        this.setState({ TagsSelecionado });
   
    }
    
    renderSelecTag = (Tags) =>{ 
        return (
          <div>
               <FormGroup>
                    <Select                      
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        isMulti
                        name="colors"
                        options={Tags}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Tag"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }
    

      toggleModal1(sala) {
        //console.log(sala);

        if (this.state.modal1 === false){ 
        
            
                // let sa = "";
                // let num="";

                // switch (sala) {  
                //     case "Cabecera":
                //         sa = "801";
                //         num="01";
                //     break;
                //     case "802":
                //         sa = "802";
                //         num="02";
                //     break;
                //     case "803":
                //         sa = "803";
                //         num="03";                  
                //     break;
                //     case "804":
                //         sa = "804";
                //         num="04";                   
                //     break;
                //     case "805":
                //         sa = "805";
                //         num="05";                  
                //     break;
                //     case "806":
                //         sa = "806";
                //         num="06";                   
                //     break;
                //     case "807":
                //         sa = "807";
                //         num="07";                 
                //     break;
                //     case "808":
                //         sa = "808";
                //         num="08";                    
                //     break;
                //     case "809":
                //         sa = "809";
                //         num="09";              
                //     break;
                //     case "810":
                //         sa = "810";
                //         num="10";                    
                //     break;
                //     case "811":
                //         sa = "811";
                //         num="11";                  
                //     break;
                //     case "812":
                //         sa = "812";
                //         num="12";                    
                //     break;

                //     default:
                //         sa = "No definido";
                //     break;

                // }  
                // this.setState({
                //     mySalaModal:"Cabecera"
                // });

                // const EndPointTag = `${API_ROOT}/tag`;
                // let mysala = "TK" + sa;              
                // let tags = [];
                // const token = "tokenfalso";
                // axios
                // .get(EndPointTag, {
                // headers: {  
                //     'Authorization': 'Bearer ' + token
                // }
                // })
                // .then(response => {
                //     tags = response.data.data;  
                    //console.log(tags.filter((tag) => tag.nameAddress === "maxTempS1V"))
                    //console.log(tags.filter((tag) => tag.nameAddress === `M6001TK801N8`)[0].lastValue);
                    try{
                        // if(tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N8`)[0].lastValue==1){
                        //     sw=true;
                        //     console.log("si "+sw);
                        // }else{
                        //     sw=false;
                        //     console.log("no "+sw);
                        // }
                        // this.setState({
                        //     histeresisOxs : tags.filter((tag) => tag.nameAddress === "D20105TK801N8"),
                        //     histeresisOx : tags.filter((tag) => tag.nameAddress === "D20105TK801N8")[0].lastValue,
                        //     setpointOxs: tags.filter((tag) => tag.nameAddress === "D20104TK801N8"),
                        //     setpointOx: tags.filter((tag) => tag.nameAddress === "D20104TK801N8")[0].lastValue,
                        //     maxOxs : tags.filter((tag) => tag.nameAddress === "D20106TK801N8"),
                        //     maxOx : tags.filter((tag) => tag.nameAddress === "D20106TK801N8")[0].lastValue,
                        //     minOxs: tags.filter((tag) => tag.nameAddress === "D20107TK801N8"),
                        //     minOx: tags.filter((tag) => tag.nameAddress === "D20107TK801N8")[0].lastValue,
                        //     timeOffs : tags.filter((tag) => tag.nameAddress === "D20109TK801N8"),
                        //     timeOff : tags.filter((tag) => tag.nameAddress === "D20109TK801N8")[0].lastValue,
                        //     timeOns: tags.filter((tag) => tag.nameAddress === "D20108TK801N8"),
                        //     timeOn: tags.filter((tag) => tag.nameAddress === "D20108TK801N8")[0].lastValue,
                        //     estadoTK: tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N8`)[0].lastValue,
                        //     habilitadoTK: tags.filter((tag) => tag.nameAddress === `M60${num}${mysala}N8`),     
                            
                        //     OxdTK: tags.filter((tag) => tag.nameAddress === `D100TK801N8`),
                            
                        //     modal1: !this.state.modal1,
                        //     misTag: tags
                        // });
                        this.setState({
                            modal1: !this.state.modal1
                        });
                    }catch(e){
                        console.log(e);
                    }
                    
                // })
                // .catch(error => {
                // console.log(error);
                // });
            }else{
                this.setState({
                    modal1: !this.state.modal1
                });

            }
    }

    toggleModal2(sala) {
        centro_usuario=sala;
        //console.log(sala);
        if (this.state.modal2 === false){ 
                    try{
                        this.setState({
                            modal2: !this.state.modal2
                        });
                    }catch(e){
                        console.log(e);
                    }
            }else{
                this.setState({
                    modal2: !this.state.modal2
                });
            }
    }

    agregar(){
      //alert(id_usuario+" - "+id_centro);
      if(this.state.selectValue_usu!="0" && this.state.selectValue_cent!="0"){
        const EndPointTag = `${API_ROOT}/${empresa}/crear_usuarios_centros`;

          axios
          .post(EndPointTag, {
            "id_usuario": this.state.selectValue_usu,
            "id_centro": this.state.selectValue_cent
          })
          .then(response => {
              if(response.data.statusCode==200){
                //console.log(response.data.data.affectedRows);
                if(response.data.data.affectedRows!=0){
                  let data = response.data.data;
                  toast['success']('El Centro ha sido agregado correctamente', { autoClose: 4000 })
                  this.getUsuarios_Centros();
                  this.setState({selectValue_usu:'0',selectValue_cent:'0'})
                  //this.toggleModal1("-1")
                  //console.log(response.data.data)
                }else{
                  toast['warning']('El Centro ya ha sido agregado anteriormente', { autoClose: 4000 })
                }
              }
              
            //this.setState({isLoading: false,Tags: data});   
          })
          .catch(error => {
            console.log(error);
          });

      }else{
        toast['warning']('Favor de seleccionar un Centro y/o Usuario', { autoClose: 4000 })
      }
    }
    limpiar(){
        nombres_usuarios="";
        this.setState({selectValue:'0'});
    }
    eliminar(id){
      //alert(id)
      const EndPointTag = `${API_ROOT}/${empresa}/usuarios_centros/${id}`;
      axios
      .delete(EndPointTag)
      .then(response => {
          if(response.data.statusCode==200){
              //let data = response.data.data;
              let tabla=this.state.Usuarios_Centros.filter((data)=>data.id!=id).map((data)=>data);
              this.setState({Usuarios_Centros:tabla});
              this.toggleModal2("-1");

              toast['success']('El Centro ha sido elimiado correctamente', { autoClose: 4000 })
              centro_usuario="";
              this.getUsuarios_Centros();
              //console.log(response.data.data)
          }else{
              toast['warning']('El Centro no pudo ser eliminado', { autoClose: 4000 })
          }   
      })
      .catch(error => {
        console.log(error);
      });
      
       
    }

    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }
 
    render() {
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state; 

     
        
 
        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));       
           let end = moment(start).add(1, "days").subtract(1, "seconds");         
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */
         
         ///***********************************+++ */
       
         
 
         let optionsChart1 = {

            
             data: dataCharts,
            
             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false, 
           
             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMM HH:mm");       
                      } 
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center", 
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});   
                 
               }
           }, 
            
             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10
         
             },
             axisY :
                dataAxisy.filter((data,i)=>!String(data.id).includes("PSU")&&!String(data.id).includes("%")),
             axisY2 :{
                title:"% - PSU"
            },   
            
             }
         
             
         ///***********************************+++ */

        return (
            <Fragment>
                     {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica {this.state.NameWorkplace}
                                    </div>
                                </div>
                            </div>
                        </div> */}
                      <Breadcrumb tag="nav" listTag="div">
                      <BreadcrumbItem tag="span" href="#">Funciones por Centro</BreadcrumbItem>
                      <BreadcrumbItem tag="span" href="#">Usuario Centros</BreadcrumbItem>
                      {/* <BreadcrumbItem active tag="span">{this.state.NameWorkplace.name}</BreadcrumbItem> */}
                      </Breadcrumb>
                     <Row>  
                                         
                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">                                 
                                    <Row>
                                        <Col md={12}  lg={3}>
                                            <Input value={this.state.selectValue} type="select" id="centro" onChange={((e)=>{
                                                let index = e.target.selectedIndex;
                                                nombre_centros=e.target.options[index].text;
                                                //alert(nombre_centros);
                                                centro=e.target.value+"/"+e.target.options[index].text;
                                                this.setState({selectValue:e.target.value})
                                                // this.getSondas(e.target.value);
                                            })}>
                                              <option disabled selected value="0">SELECCIONE UN CENTRO</option>
                                              {centros.map((data,i)=>{
                                                return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.code}>{data.name}</option>
                                              })}
                                              
                                            </Input>
                                        </Col>
                                        <Col md={12}  lg={3}>
                                            <Input value={nombres_usuarios} placeholder="Nombre de Usuario..." onChange={e => {
                                                nombres_usuarios=e.target.value;
                                                this.inputChangeHandler(e);
                                                }} />
                                        </Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col md="12" lg="1"></Col>
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            this.limpiar();
                                                             }}
                                                >limpiar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>                            
                                        <Col   md={12} lg={1}> 
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        onClick={() => { 
                                                            // this.setState({
                                                            //     dataExport:[],
                                                            //     buttonExport:'block'
                                                            // });
                                                            this.setState({selectValue_usu:'0',selectValue_cent:'0'})
                                                             this.toggleModal1()
                                                             }}
                                                >Agreagar
                                                </Button>
                                            </ButtonGroup>
                                        </Col>


                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                            {
                            <Card className="p-10 m-10" >                                 
                                                      
                            <ReactTable
                                style={{height:600}}                                                       
                                //data={this.state.Seleccionadas}
                                data={this.state.Usuarios_Centros
                                    .filter((data)=>{
                                        if(this.state.selectValue!=0 && nombres_usuarios!=""){
                                            return data.nombres_usuarios.toLocaleUpperCase().includes(nombres_usuarios.toLocaleUpperCase())&&data.id_centros==this.state.selectValue
                                        }else if(nombres_usuarios!=""){
                                            return data.nombres_usuarios.toLocaleUpperCase().includes(nombres_usuarios.toLocaleUpperCase())
                                        }else if(this.state.selectValue!=0){
                                            return data.id_centros==this.state.selectValue
                                        }else{
                                             return data;
                                        }
                                        
                                    })
                                    //.filter((data)=>)
                                    .map((data)=>data)}
                      
                                // loading= {false}
                                showPagination= {true}
                                showPaginationTop= {false}
                                showPaginationBottom= {true}
                                showPageSizeOptions= {false}
                                pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                defaultPageSize={10}
                                columns={[
                                        {
                                        Header: "CENTROS",
                                        accessor: "nombre_centros",                                                              
                                        width: 500,
                                        Cell: ({ original }) => (
                                            <div style={{marginLeft:`${44}%`}}>
                                              {original.nombre_centros}
                                            </div>
                                          )
                                        },
                                        {
                                          Header: "USUARIOS",
                                          accessor: "nombres_usuarios",
                                          Cell: ({ original }) => (
                                              <div style={{marginLeft:`${46}%`}}>
                                                {(original.nombres_usuarios).toUpperCase()}
                                              </div>
                                            )
                                        },
                                        {
                                            width: 300,
                                            Header: "Acciones",
                                            Cell: ({ original }) => (
                                              <Button color="danger" outline style={{marginLeft:`${40}%`}} value={original.centros} onClick={((e)=>{
                                                centro_usuario=original.id;
                                                this.toggleModal2(original.id);
                                              })} >
                                                Quitar
                                              </Button>
                                            )
                                        }
                                    ]                                                             
                                }
                                
                                className="-striped -highlight"
                                />
                            </Card>
                            }
                        </Col>
                        
                        <Col>
                        
                        </Col>

                    </Row>
                    <Modal isOpen={this.state.modal1} toggle={() => this.toggleModal1("-1")} className={this.props.className} style={{marginTop:80}}>
                                                <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                    <ModalHeader toggle={() => this.toggleModal1("-1")}>Asigna Centro </ModalHeader>
                                                    <ModalBody>

                                                    <Row>     
                                                        <Col xs="3" md="3" lg="3"></Col>
                                                        <Col  xs="12" md="12" lg="12">
                                                            <Card className="main-card mb-3">
                                                                <CardBody>
                                                                    <CardTitle>Formulario de Registro</CardTitle>
                                                                    <Form>
                                                                        <FormGroup>
                                                                            <Label for="setpointOx" className="m-0">Centros</Label>
                                                                            <Input value={this.state.selectValue_cent}  type="select" id="centro" onChange={((e)=>{
                                                                                let index = e.target.selectedIndex;
                                                                                //centro=e.target.value+"/"+e.target.options[index].text;
                                                                                id_centro=e.target.value;
                                                                                //this.getSondas(e.target.value);
                                                                                this.setState({selectValue_cent:e.target.value})
                                                                            })}>
                                                                                <option disabled selected value="0">SELECCIONE UN CENTRO</option>
                                                                                {centros.map((data,i)=>{
                                                                                    return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.code}>{data.name}</option>
                                                                                })}
                                                                            </Input>
                                                                        </FormGroup>
                                                                        <div style={{marginTop:10}}></div>
                                                                        <FormGroup>
                                                                            <Label for="setpointOx" className="m-0">Usuarios</Label>
                                                                            <Input value={this.state.selectValue_usu} type="select" id="dispositivo" onChange={((e)=>{
                                                                                let index = e.target.selectedIndex;
                                                                                //sensor=e.target.value+"/"+e.target.options[index].text;
                                                                                id_usuario=e.target.value;
                                                                                this.setState({selectValue_usu:e.target.value})
                                                                            })}>
                                                                            <option disabled selected value="0">SELECCIONE UN USUARIO</option>
                                                                            {this.state.Usuarios.map((data,i)=>{
                                                                                let name=data.name;
                                                                                name=name.toUpperCase();
                                                                                return <option value={data.id}>{name}</option>
                                                                            })}
                                                                            </Input>
                                                                        </FormGroup>
                                                                    </Form>
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        
                                                    </Row>
                                                    
                                                    </ModalBody>
                                                    <ModalFooter>
                                                    
                                                            <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button> 
                                                            <Button color="primary"  onClick={() => this.agregar()}>Guardar</Button>{' '}
                                                    
                                                    </ModalFooter>
                                                    </BlockUi>
                     </Modal>
                     <Modal isOpen={this.state.modal2} toggle={() => this.toggleModal2("-1")} className={this.props.className} style={{marginTop:80}}>
                        <BlockUi tag="div" blocking={this.state.blocking2} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                    {/* <ModalHeader toggle={() => this.toggleModal2("-1")}></ModalHeader> */}
                                                    <ModalBody>

                                                    <Row>     
                                                        <Col xs="3" md="3" lg="3"></Col>
                                                        <Col  xs="12" md="12" lg="12">
                                                            <Card className="main-card mb-3" style={{marginTop:20}}>
                                                                <CardBody>
                                                                    <Form>
                                                                    <div style={{textAlign:"center"}}>
                                                                    <h2>¿Estás seguro?</h2>
                                                                    <br />
                                                                    <h3>¡No podrás revertir esto!</h3>
                                                                    </div>
                                                                    </Form>
                                                                </CardBody>
                                                            </Card>
                                                        </Col>
                                                        
                                                    </Row>
                                                    
                                                    </ModalBody>
                                                    <ModalFooter>
                                                    
                                                            <Button color="link" onClick={() => this.toggleModal2("-1")}>Cancel</Button> 
                                                            <Button color="danger"  onClick={() => this.eliminar(centro_usuario)}>Eliminar <i className="pe-7s-trash btn-icon-wrapper"></i></Button>{' '}
                                                    
                                                    </ModalFooter>
                        </BlockUi>
                     </Modal>
            </Fragment>
        );
    }

}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    centroUsuario: state.Session.centroUsuario
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UsuarioCentros);
