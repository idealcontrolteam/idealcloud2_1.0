import { Module } from '@nestjs/common';
import { RegistroService } from './registro.service';
import { RegistroController } from './registro.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Registros } from './entity/registros.entity';
import { UsuarioModule } from '../usuario/usuario.module';
import { UsuarioService } from '../usuario/usuario.service';
import { DispositivoModule } from '../dispositivo/dispositivo.module';
import { DispositivoService } from '../dispositivo/dispositivo.service'

@Module({
  imports: [TypeOrmModule.forFeature([Registros]), UsuarioModule,DispositivoModule],
  providers: [RegistroService,UsuarioService,DispositivoService],
  controllers: [RegistroController],
  exports: [TypeOrmModule]
})
export class RegistroModule {}
