import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Dispositivos } from './entity/dispositivos.entity';
import { GLOBAL_BD_PREFIX } from '../global';

@Injectable()
export class DispositivoService {
  constructor(
    @InjectRepository(Dispositivos)
    private dispositivoRepository: Repository<Dispositivos>,
  ) {}

  async getDispositivos(empresa:string): Promise<Dispositivos[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.dispositivoRepository
    .query(
      `SELECT 
      id_dispositivos as code, 
      nombre_dispositivos as name, 
      activo_dispositivos as active 
      FROM ${prefix}.dispositivos`,
    );
  }
  async getDispositivo(empresa:string, id: number): Promise<Dispositivos[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.dispositivoRepository
    .query(
      `SELECT 
      id_dispositivos as code, 
      grupos.nombre_grupos as grupo,
      nombre_dispostivos as name, 
      activo_dispositivos as active 
      FROM ${prefix}.dispositivos 
      INNER JOIN ${prefix}.grupos ON
      ${prefix}.dispositivos.id_grupos = ${prefix}.grupos.id_grupos
      WHERE 
      id_dispositivos = ?`,
      [id],
    );
  }
  async getDispositivosActivos(empresa:string): Promise<Dispositivos[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.dispositivoRepository
    .query(
      `SELECT 
      id_dispositivos as code, 
      nombre_dispostivos as name, 
      activo_dispositivos as active 
      from ${prefix}.dispositivos 
      WHERE 
      activo_dispositivos = 1`,
    );
  }
  async getDispositivosCentroActivos(empresa:string, centro: number): Promise<Dispositivos[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    const centros = await this.dispositivoRepository
    .query(
      `SELECT 
        id_dispositivos as code,
        grupos.nombre_grupos as grupo,
        nombre_dispostivos as name, 
        activo_dispositivos as active 
        from ${prefix}.dispositivos 
        INNER JOIN ${prefix}.grupos ON
        ${prefix}.dispositivos.id_grupos = ${prefix}.grupos.id_grupos
        WHERE
        ${prefix}.grupos.id_centros = ? and 
        activo_dispositivos = 1`,
        [centro]
    );
    return centros;
  }
  // async getGruposCentro(empresa:string, centro: number): Promise<Dispositivos[]> {
  //   const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
  //   return await this.dispositivoRepository
  //   .query(
  //     `SELECT 
  //       id_dispositivos as code, 
  //       nombre_dispostivos as name, 
  //       activo_dispositivos as active 
  //       from ${prefix}.dispositivos 
  //       INNER JOIN ${prefix}.grupos ON
  //       ${prefix}.dispositivos.id_grupos = ${prefix}.grupos.id_grupos
  //       WHERE
  //       ${prefix}.grupos.id_centros = ?`,
  //       [centro]
  //   );
  // }
}
