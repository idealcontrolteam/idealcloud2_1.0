import axios from 'axios';
import { API_ROOT} from '../api-config';
import moment from 'moment';

const APItag1 = `${API_ROOT}/tag/filteroregister/true`;
let empresa=sessionStorage.getItem("Empresa");
//console.log(APItag1);
export class TagServices {    

    getDataTag() { 
        const token = "tokenFalso"; 
        return axios.get(APItag1, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
   
     }

     getDataIdTags(id) { 
      const token = "tokenFalso"; 
      return axios.get(APItag1+'/'+id, {
         headers: {  
           'Authorization': 'Bearer ' + token
         }
       })
       .then(response =>  response.data.data)
 
   }

   getRegistros(id){
    var date = moment().subtract(1, 'hours');
    var minute = date.minutes();
    //console.log(minute);
    //alert(date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm'))
    let now = new Date(); 
    const f1 = date.subtract(minute, 'minutes').format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
    const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";    
  
    let f1_split=f1.split("T");
    let f2_split=f2.split("T");
    //const API1 = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
    //alert(API1);
    //const EndPointTag = `${API_ROOT}/${empresa}/registros/sonda/${id_disp}/${f1_split[0]}/${f2_split[0]}`;
    //const EndPointTag =`${API_ROOT}/${empresa}/registros/sonda/xy/${id}/2019-06-07T00:00:00/2019-06-07T23:59:59`;
    const EndPointTag =`${API_ROOT}/${empresa}/registros/sonda/xy/${id}/${f1_split[0]}T00:00:00/${f2_split[0]}T23:59:59`;

    return axios
    .post(EndPointTag,  {
        "login_usuarios": "purrutia",
        "clave_usuarios": "purrutia"
      })
    .then(response => response.data.data)
}

}