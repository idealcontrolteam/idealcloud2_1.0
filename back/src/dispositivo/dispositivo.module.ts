import { Module } from '@nestjs/common';
import { DispositivoController } from './dispositivo.controller';
import { DispositivoService } from './dispositivo.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dispositivos } from './entity/dispositivos.entity';
import { UsuarioModule } from '../usuario/usuario.module';
import { UsuarioService } from '../usuario/usuario.service';

@Module({
  imports: [TypeOrmModule.forFeature([Dispositivos]), UsuarioModule],
  controllers: [DispositivoController],
  providers: [DispositivoService,UsuarioService],
  exports: [TypeOrmModule]
})
export class DispositivoModule {}
