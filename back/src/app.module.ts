import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CentroModule } from './centro/centro.module';
import { DispositivoModule } from './dispositivo/dispositivo.module';
import { RegistroModule } from './registro/registro.module';
import { UsuarioModule } from './usuario/usuario.module';
import { Centros } from './centro/entity/centros.entity';
import { Dispositivos } from './dispositivo/entity/dispositivos.entity';
import { Registros } from './registro/entity/registros.entity';
import { Usuarios } from './usuario/entity/usuarios.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      //host: '23.246.244.170',
      host: 'localhost',
      port: 3306,
      username: 'root',
      //username: 'idealcontrol3',
      password: '',//vh7G*98Q
      database: 'idealcloud_bd_aqu',
      entities: [Centros, Dispositivos, Registros, Usuarios],
      synchronize: false, // Crea tablas y relaciones en la base de datos configurada
    }),
    CentroModule,
    DispositivoModule,
    RegistroModule,
    UsuarioModule,
  ],
})
export class AppModule {}
