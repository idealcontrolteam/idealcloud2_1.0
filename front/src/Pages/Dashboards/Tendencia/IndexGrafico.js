import React, {Component, Fragment} from 'react';
 import Grefico_Tendecia from './ComponentTendencia/Tendencia/Grafico_Tendencia';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {checkStatusLogin} from '../../../services/user';
import { Redirect, Route } from 'react-router-dom';



export default class IndexGrafico extends Component {
    render() {
        const isLoggedIn = checkStatusLogin();
        return (
            <Fragment>
                <Route 
                    render={() => 
                        isLoggedIn ? ( 
                            <ReactCSSTransitionGroup
                                component="div"
                                transitionName="TabsAnimation"
                                transitionAppear={true}
                                transitionAppearTimeout={0}
                                transitionEnter={false}
                                transitionLeave={false}>
                                <div className="app-inner-layout">                      
                                    <Grefico_Tendecia/>              
                                </div>
                            </ReactCSSTransitionGroup>
                                ) : (
                                    <Redirect to="/pages/login"></Redirect>
                                )
                        }      
                
                /> 
            </Fragment>
        )
    }
}