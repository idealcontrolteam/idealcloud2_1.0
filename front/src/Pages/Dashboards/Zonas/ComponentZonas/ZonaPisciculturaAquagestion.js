import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../api-config';
import Loader from 'react-loaders';
/*import {
    toast

} from 'react-toastify';*/

//import _ from 'lodash';

import moment from 'moment';
import {
    Row, Col,
    Button, 
    CardHeader,
    Card, CardBody,
    Collapse
 
    /*   Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText*/
} from 'reactstrap';
//CustomInput,

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
// import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';


const S1 = "5ced38e2197f5732a44eb601";
const S2 = "5ced38e2197f5732a44eb602";
const S3 = "5ced38e2197f5732a44eb603";
const S4 = "5ced38e2197f5732a44eb604";
const S5 = "5ced38e2197f5732a44eb605";
const S6 = "5ced38e2197f5732a44eb606";

// const Z1 = "5ced3565197f5732a44eb50e";

const favoritos =[
"5d10ecfcefe8e6289cef0a01",
"5d10ecfcefe8e6289cef0a02", 
"5d10ecfcefe8e6289cef0a03",
"5d10ecfcefe8e6289cef0a04",
"5d10ecfcefe8e6289cef0a10",
"5d10ecfcefe8e6289cef0a11", 
"5d10ecfcefe8e6289cef0a12",
"5d10ecfcefe8e6289cef0a13",
"5d10ecfcefe8e6289cef0a19",
"5d10ecfcefe8e6289cef0a20", 
"5d10ecfcefe8e6289cef0a21",
"5d10ecfcefe8e6289cef0a22",
"5d10ecfcefe8e6289cef0a28",
"5d10ecfcefe8e6289cef0a29", 
"5d10ecfcefe8e6289cef0a30",
"5d10ecfcefe8e6289cef0a31",
"5d10ecfcefe8e6289cef0a37",
"5d10ecfcefe8e6289cef0a38", 
"5d10ecfcefe8e6289cef0a39",
"5d10ecfcefe8e6289cef0a40",
"5d10ecfcefe8e6289cef0a46",
"5d10ecfcefe8e6289cef0a47", 
"5d10ecfcefe8e6289cef0a48",
"5d10ecfcefe8e6289cef0a49"]
const Nofavoritos =[
"5d10ecfcefe8e6289cef0a05",
"5d10ecfcefe8e6289cef0a06", 
"5d10ecfcefe8e6289cef0a07",
"5d10ecfcefe8e6289cef0a08",
"5d10ecfcefe8e6289cef0a09",
"5d10ecfcefe8e6289cef0a14",
"5d10ecfcefe8e6289cef0a15", 
"5d10ecfcefe8e6289cef0a16",
"5d10ecfcefe8e6289cef0a17",
"5d10ecfcefe8e6289cef0a18",
"5d10ecfcefe8e6289cef0a23",
"5d10ecfcefe8e6289cef0a24", 
"5d10ecfcefe8e6289cef0a25",
"5d10ecfcefe8e6289cef0a26",
"5d10ecfcefe8e6289cef0a27",
"5d10ecfcefe8e6289cef0a32",
"5d10ecfcefe8e6289cef0a33", 
"5d10ecfcefe8e6289cef0a34",
"5d10ecfcefe8e6289cef0a35",
"5d10ecfcefe8e6289cef0a36",
"5d10ecfcefe8e6289cef0a41",
"5d10ecfcefe8e6289cef0a42", 
"5d10ecfcefe8e6289cef0a43",
"5d10ecfcefe8e6289cef0a44",
"5d10ecfcefe8e6289cef0a45",
"5d10ecfcefe8e6289cef0a50"]



export default class IndexZonaPisciculturaAquagestion extends Component {   
    constructor() {
        super();
        this.state = {   
            myDataChart1: [],   
            myDataChart2: [], 
            myDataChart3: [], 
            myDataChart4: [],
            myDataChart5: [],
            myDataChart6: [],
            //misTag: [],      
            isLoading: false,
            error: null
            
        }
        this.loadDataChar = this.loadDataChar.bind(this);
        
        //this.loadDataTag = this.loadDataTag.bind(this);
        this.toggle1 = this.toggle1.bind(this);
        this.toggle2 = this.toggle2.bind(this);
        this.toggle3 = this.toggle3.bind(this);
        this.toggle4 = this.toggle4.bind(this);
        this.toggle5 = this.toggle5.bind(this);
        this.toggle6 = this.toggle6.bind(this);
        //this.almacenarConfig = this.almacenarConfig.bind(this);
        //this.confirmacionTermino = this.confirmacionTermino.bind(this);
    }



    componentDidMount = () => {  
     //   window.location.reload(); 
        const intervaloRefresco =240000;
        this.setState({ isLoading: true });
        this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        this.loadDataChar(); 
      
        
        //this.loadDataTag();
        //this.intervalIdtag = setInterval(() => this.loadDataTag(),2000);

        // this.slider1.slickGoTo(1);
            setTimeout(
                function() {
                   // console.log("holita");
           
                    this.slider1.slickGoTo(0);
                    this.slider2.slickGoTo(0);
                    this.slider3.slickGoTo(0);
                    this.slider4.slickGoTo(0);
                    this.slider5.slickGoTo(0);
                    this.slider6.slickGoTo(0);
               

                }
                .bind(this),
                7000
            );

      
    //     this.slider1.slickGoTo(0);
    //     //  this.slider2.slickGoTo(1)
    //     this.slider2.slickGoTo(0)
    //    this.slider3.slickGoTo(0)
    //    this.slider3.slickGoTo(0)
    //    this.slider3.slickGoTo(0)
    //    this.slider3.slickGoTo(2)

    }
    componentWillUnmount = () => {
       clearInterval(this.intervalIdChart);
      // clearInterval(this.intervalConfirmacion);
       //clearInterval(this.intervalIdtag);
    }  

    loadDataChar = () => {
        console.log("refresh");
        let now = new Date(); 
        const f1 = moment(now).subtract(12, "hours").format('YYYY-MM-DDT00:00:00') + ".000Z";
        const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";    
      
        const API1 = `${API_ROOT}/tag/location/${S1}/${f1}/${f2}`;
        const API2 = `${API_ROOT}/tag/location/${S2}/${f1}/${f2}`;
        const API3 = `${API_ROOT}/tag/location/${S3}/${f1}/${f2}`;
        const API4 = `${API_ROOT}/tag/location/${S4}/${f1}/${f2}`;
        const API5 = `${API_ROOT}/tag/location/${S5}/${f1}/${f2}`;
        const API6 = `${API_ROOT}/tag/location/${S6}/${f1}/${f2}`;
        
        //console.log(API1);
        //const token = localStorage.getItem("token");        
        const token = "tokenfalso";
        axios
        .get(API1, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart1 = response.data.data;  
           // console.log(myDataChart1)

           this.setState({ myDataChart1,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });


   axios
        .get(API2, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart2 = response.data.data;  

           this.setState({ myDataChart2,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });

        


        axios
        .get(API3, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart3 = response.data.data;  

           this.setState({ myDataChart3,isLoading: false });   
          //   this.slider3.slickGoTo(0);
        })
        .catch(error => {
          console.log(error);
        });


        axios
        .get(API4, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart4 = response.data.data;  

           this.setState({ myDataChart4,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });


        axios
        .get(API5, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart5 = response.data.data;  

           this.setState({ myDataChart5,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });

       axios
        .get(API6, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart6 = response.data.data;  

           this.setState({ myDataChart6,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });

        
    }
   
    toggle1() {
        this.setState(state => ({ collapse1: !state.collapse1 }));
        // this.slider1.slickGoTo(1);
        // this.slider1.slickGoTo(0);

      }
    toggle2() {
        this.setState(state => ({ collapse2: !state.collapse2 }));

        //  this.slider2.slickGoTo(1)
        // this.slider2.slickGoTo(0)
      }
    toggle3() {
        this.setState(state => ({ collapse3: !state.collapse3 }));
      }
      toggle4() {
        this.setState(state => ({ collapse4: !state.collapse4 }));
      }
    toggle5() {
        this.setState(state => ({ collapse5: !state.collapse5 }));
      }

   toggle6() {
        this.setState(state => ({ collapse6: !state.collapse6 }));
      }
    
    
    
    render() {  
        
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { myDataChart1,myDataChart2, myDataChart3,myDataChart4,myDataChart5,myDataChart6,isLoading, error} = this.state;
          const settings = {
            // autoplaySpeed:6000,
            // autoplay: false,        
            // centerMode: false,
             infinite: true,
            slidesToShow: 1,
            // slidesToScroll: 1,
            // speed: 600,
             arrows: false,
            // dots: false
        };
      const settings3 = {
            // autoplaySpeed:6000,
            // autoplay: false,        
            // centerMode: false,
             infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            // speed: 600,
             arrows: false,
            // dots: false
        };

    
       
        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if (error) {
            return <p>{error.message}</p>;
        }
    
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        
      
        return (
            <Fragment>     
                              
                    {/* <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                      Piscicultura Aquagestion
                                    </div>
                                </div>
                                <div className="page-title-actions"> 
                                </div>                    
                            </div>
                        </div>                      
                    */}
    
        
                       <Row>
                            <Col sm="6" lg="2" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 1

                                    </div>
                                                                                                   
                                    </CardHeader>
                                    <CardBody className="p-0">
                               
                         
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                {   
                                                                 myDataChart1
                                                                 .filter((data) =>   favoritos.includes(data._id))
                                                                 .map((data, i)=>
                                                                    <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider1.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                    
                                                                            {data.measurements[data.measurements.length-1].value}
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>
                                                                    </Col>
                                                                        )    
                                                                 
                                                         }  
                                           
                                                </Row>
                                                </div>
                                                <Button outline color="secondary" size="sm" onClick={this.toggle1}>...</Button>
                                                <Collapse isOpen={this.state.collapse1}>          
                                                <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                    {   
                                                                    myDataChart1
                                                                    .filter((data) =>   Nofavoritos.includes(data._id))
                                                                    .map((data, i)=>
                                                                        <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                            <Button 
                                                                            onClick={ e => this.slider1.slickGoTo(i+4)}
                                                                            key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i+4]}>  
                                                                                <div className="size-boton mt-0  " style={{color:miscolores[i+4]}} >                                                    
                                                                                {data.measurements[data.measurements.length-1].value} 
                                                                                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                                </div>
                                                                                <div className="widget-subheading">
                                                                                {data.shortName}.substr(0, 3)
                                                                                </div>
                                                                            </Button>  
                                                                        </div>
                                                                        </Col>
                                                                            )    
                                                                    
                                                            }  

                                                    </Row>
                                                    </div>
                                                
                                                </Collapse>




                                       
                                        </Card>
                                        
                                        
                                        
                                        
                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider1 => (this.slider1 = slider1)} {...settings}>

                                        {
                                                             
                                                             myDataChart1
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                       {data.shortName}
                                                                                    </div>
                                                                                    
                                                                                </div>                                                                                  
                                                                                
                                                                           
                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline size_prom">
                                                                                  Max
                                                                                  </div>
                                                                                  
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                                                                                
                                                                                       <span className="pl-1 size_prom">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2 size_prom">
                                                                                  Prom
                                                                                  </div>
                                                                                 
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2 size_prom">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={100} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:5,
                                                                                                left: -44,
                                                                                                bottom: -16
                                                                                            }}>
                                                                                           
                                                                                            <Tooltip                                                                                                        
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '8px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '8px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                                                                          
                                    </CardBody>
                                </Card>
                            </Col>
                            
                            <Col sm="6" lg="2" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 2

                                    </div>
                                                                                                   
                                    </CardHeader>
                                    <CardBody className="p-0">
                               
                 
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                {   
                                                                 myDataChart2
                                                                 .filter((data) =>   favoritos.includes(data._id))
                                                                 .map((data, i)=>
                                                                    <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider2.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                    
                                                                            {data.measurements[data.measurements.length-1].value} 
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>
                                                                    </Col>
                                                                        )    
                                                                 
                                                         }  
                                           
                                                </Row>
                                                </div>
                                                <Button outline color="secondary" size="sm" onClick={this.toggle2}>...</Button>
                                                <Collapse isOpen={this.state.collapse2}>          
                                                <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                    {   
                                                                    myDataChart2
                                                                    .filter((data) =>   Nofavoritos.includes(data._id))
                                                                    .map((data, i)=>
                                                                        <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                            <Button 
                                                                            onClick={ e => this.slider2.slickGoTo(i+4)}
                                                                            key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i+4]}>  
                                                                                <div className="size-boton mt-0  " style={{color:miscolores[i+4]}} >                                                    
                                                                                {data.measurements[data.measurements.length-1].value   } 
                                                                                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                                </div>
                                                                                <div className="widget-subheading">
                                                                                {data.shortName}
                                                                                </div>
                                                                            </Button>  
                                                                        </div>
                                                                        </Col>
                                                                            )    
                                                                    
                                                            }  

                                                    </Row>
                                                    </div>
                                                
                                                </Collapse>




                                       
                                        </Card>
                                        
                                        
                                        
                                        
                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider2 => (this.slider2 = slider2)} {...settings}>

                                        {
                                                             
                                                             myDataChart2
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                       {data.shortName}
                                                                                    </div>
                                                                                    
                                                                                </div>                                                                                  
                                                                                
                                                                           
                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline size_prom">
                                                                                  Max
                                                                                  </div>
                                                                                  
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                                                                                
                                                                                       <span className="pl-1 size_prom">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2 size_prom">
                                                                                  Prom
                                                                                  </div>
                                                                                 
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2 size_prom">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={100} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:5,
                                                                                                left: -44,
                                                                                                bottom: -16
                                                                                            }}>
                                                                                           
                                                                                            <Tooltip                                                                                                        
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '8px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '8px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                                                                          
                                    </CardBody>
                                </Card>
                            </Col>
                              
                            <Col sm="6" lg="2" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 3

                                    </div>
                                                                                                   
                                    </CardHeader>
                                    <CardBody className="p-0">
                               
                 
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                {   
                                                                 myDataChart3
                                                                 .filter((data) =>   favoritos.includes(data._id))
                                                                 .map((data, i)=>
                                                                    <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider3.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                    
                                                                            {data.measurements[data.measurements.length-1].value   } 
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>
                                                                    </Col>
                                                                        )    
                                                                 
                                                         }  
                                           
                                                </Row>
                                                </div>
                                                <Button outline color="secondary" size="sm" onClick={this.toggle3}>...</Button>
                                                <Collapse isOpen={this.state.collapse3}>          
                                                <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                    {   
                                                                    myDataChart3
                                                                    .filter((data) =>   Nofavoritos.includes(data._id))
                                                                    .map((data, i)=>
                                                                        <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                            <Button 
                                                                            onClick={ e => this.slider3.slickGoTo(i+4)}
                                                                            key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i+4]}>  
                                                                                <div className="size-boton mt-0  " style={{color:miscolores[i+4]}} >                                                    
                                                                                {data.measurements[data.measurements.length-1].value   } 
                                                                                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                                </div>
                                                                                <div className="widget-subheading">
                                                                                {data.shortName}
                                                                                </div>
                                                                            </Button>  
                                                                        </div>
                                                                        </Col>
                                                                            )    
                                                                    
                                                            }  

                                                    </Row>
                                                    </div>
                                                
                                                </Collapse>




                                       
                                        </Card>
                                        
                                        
                                        
                                        
                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider3 => (this.slider3 = slider3)} {...settings3}>

                                        {
                                                             
                                                             myDataChart3
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                       {data.shortName}
                                                                                    </div>
                                                                                    
                                                                                </div>                                                                                  
                                                                                
                                                                           
                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline size_prom">
                                                                                  Max
                                                                                  </div>
                                                                                  
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                                                                                
                                                                                       <span className="pl-1 size_prom">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2 size_prom">
                                                                                  Prom
                                                                                  </div>
                                                                                 
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2 size_prom">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={100} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:5,
                                                                                                left: -44,
                                                                                                bottom: -16
                                                                                            }}>
                                                                                           
                                                                                            <Tooltip                                                                                                        
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '8px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '8px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                                                                          
                                    </CardBody>
                                </Card>
                            </Col>
                   

                                                  
                             <Col sm="6" lg="2" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 4

                                    </div>
                                                                                                   
                                    </CardHeader>
                                    <CardBody className="p-0">
                             
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                {   
                                                                 myDataChart4
                                                                 .filter((data) =>   favoritos.includes(data._id))
                                                                 .map((data, i)=>
                                                                    <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider4.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                    
                                                                            {data.measurements[data.measurements.length-1].value   }
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>
                                                                    </Col>
                                                                        )    
                                                                 
                                                         }  
                                           
                                                </Row>
                                                </div>
                                                <Button outline color="secondary" size="sm" onClick={this.toggle4}>...</Button>
                                                <Collapse isOpen={this.state.collapse4}>          
                                                <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                    {   
                                                                    myDataChart4
                                                                    .filter((data) =>   Nofavoritos.includes(data._id))
                                                                    .map((data, i)=>
                                                                        <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                            <Button 
                                                                            onClick={ e => this.slider4.slickGoTo(i)}
                                                                            key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i+4]}>  
                                                                                <div className="size-boton mt-0  " style={{color:miscolores[i+4]}} >                                                    
                                                                                {data.measurements[data.measurements.length-1].value   }
                                                                                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                                </div>
                                                                                <div className="widget-subheading">
                                                                                {data.shortName}
                                                                                </div>
                                                                            </Button>  
                                                                        </div>
                                                                        </Col>
                                                                            )    
                                                                    
                                                            }  

                                                    </Row>
                                                    </div>
                                                
                                                </Collapse>




                                       
                                        </Card>
                                        
                                        
                                        
                                        
                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider4 => (this.slider4 = slider4)} {...settings}>

                                        {
                                                             
                                                             myDataChart4
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                       {data.shortName}
                                                                                    </div>
                                                                                    
                                                                                </div>                                                                                  
                                                                                
                                                                           
                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline size_prom">
                                                                                  Max
                                                                                  </div>
                                                                                  
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                                                                                
                                                                                       <span className="pl-1 size_prom">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2 size_prom">
                                                                                  Prom
                                                                                  </div>
                                                                                 
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2 size_prom">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={100} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:5,
                                                                                                left: -44,
                                                                                                bottom: -16
                                                                                            }}>
                                                                                           
                                                                                            <Tooltip                                                                                                        
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '8px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '8px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                                                                          
                                    </CardBody>
                                </Card>
                            </Col>
                            
                            <Col sm="6" lg="2" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 5

                                    </div>
                                                                                                   
                                    </CardHeader>
                                    <CardBody className="p-0">
                           
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                {   
                                                                 myDataChart5
                                                                 .filter((data) =>   favoritos.includes(data._id))
                                                                 .map((data, i)=>
                                                                    <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider5.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                    
                                                                            {data.measurements[data.measurements.length-1].value   }
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>
                                                                    </Col>
                                                                        )    
                                                                 
                                                         }  
                                           
                                                </Row>
                                                </div>
                                                <Button outline color="secondary" size="sm" onClick={this.toggle5}>...</Button>
                                                <Collapse isOpen={this.state.collapse5}>          
                                                <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                            {   
                                                                    myDataChart5
                                                                    .filter((data) =>   Nofavoritos.includes(data._id))
                                                                    .map((data, i)=>
                                                                        <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                            <Button 
                                                                            onClick={ e => this.slider5.slickGoTo(i+4)}
                                                                            key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i+1]}>  
                                                                                <div className="size-boton mt-0  " style={{color:miscolores[i+1]}} >                                                    
                                                                                {data.measurements[data.measurements.length-1].value   }
                                                                                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                                </div>
                                                                                <div className="widget-subheading">
                                                                                {data.shortName}
                                                                                </div>
                                                                            </Button>  
                                                                        </div>
                                                                        </Col>
                                                                            )    
                                                                    
                                                            }  

                                                    </Row>
                                                    </div>
                                                
                                                </Collapse>




                                       
                                        </Card>
                                        
                                        
                                        
                                        
                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider5 => (this.slider5 = slider5)} {...settings}>

                                        {
                                                             
                                                             myDataChart5
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                       {data.shortName}
                                                                                    </div>
                                                                                    
                                                                                </div>                                                                                  
                                                                                
                                                                           
                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline size_prom">
                                                                                  Max
                                                                                  </div>
                                                                                  
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                                                                                
                                                                                       <span className="pl-1 size_prom">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2 size_prom">
                                                                                  Prom
                                                                                  </div>
                                                                                 
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2 size_prom">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={100} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:5,
                                                                                                left: -44,
                                                                                                bottom: -16
                                                                                            }}>
                                                                                           
                                                                                            <Tooltip                                                                                                        
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '8px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '8px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                                                                          
                                    </CardBody>
                                </Card>
                            </Col>  
                      
                            <Col sm="6" lg="2" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala Aquario

                                    </div>
                                                                                                   
                                    </CardHeader>
                                    <CardBody className="p-0">
                               
                                 
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                {   
                                                                 myDataChart6
                                                                 .filter((data) =>   favoritos.includes(data._id))
                                                                 .map((data, i)=>
                                                                    <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button 
                                                                           onClick={ e => this.slider6.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>  
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >                                                    
                                                                            {data.measurements[data.measurements.length-1].value   }
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>  
                                                                    </div>
                                                                    </Col>
                                                                        )    
                                                                 
                                                         }  
                                           
                                                </Row>
                                                </div>
                                                <Button outline color="secondary" size="sm" onClick={this.toggle6}>...</Button>
                                                <Collapse isOpen={this.state.collapse6}>          
                                                <div className="grid-menu grid-menu-2col">
                                                <Row className="no-gutters">

                                                    {   
                                                                    myDataChart6
                                                                    .filter((data) =>   Nofavoritos.includes(data._id))
                                                                    .map((data, i)=>
                                                                        <Col sm="6"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}> 
                                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                            <Button 
                                                                            onClick={ e => this.slider6.slickGoTo(i+4)}
                                                                            key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i+4]}>  
                                                                                <div className="size-boton mt-0  " style={{color:miscolores[i+4]}} >                                                    
                                                                                {data.measurements[data.measurements.length-1].value   }
                                                                                    <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                                </div>
                                                                                <div className="widget-subheading">
                                                                                {data.shortName}
                                                                                </div>
                                                                            </Button>  
                                                                        </div>
                                                                        </Col>
                                                                            )    
                                                                    
                                                            }  

                                                    </Row>
                                                    </div>
                                                
                                                </Collapse>




                                       
                                        </Card>
                                        
                                        
                                        
                                        
                                        <div className="p-1 slick-slider-sm mx-auto">                                     
                                        <Slider ref={slider6 => (this.slider6 = slider6)} {...settings}>

                                        {
                                                             
                                                             myDataChart6
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                       {data.shortName}
                                                                                    </div>
                                                                                    
                                                                                </div>                                                                                  
                                                                                
                                                                           
                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline size_prom">
                                                                                  Max
                                                                                  </div>
                                                                                  
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>  
                                                                                                                                                                
                                                                                       <span className="pl-1 size_prom">
                                                                                            {                                             
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)                                             
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2 size_prom">
                                                                                  Prom
                                                                                  </div>
                                                                                 
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2 size_prom">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>                                                                                  
                                                                                       <span className="pl-1 size_prom">
                                                                                       {                                             
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)                                             
                                                                                        }
                                                                                       </span>
                                                                                   </div>
                                                                                   
                                                                                   
                                                                              </div> 
                                                                                 
                                                                           
                                                                            </div>


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={80} width='100%'>
                
                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:5,
                                                                                                left: -44,
                                                                                                bottom: -16
                                                                                            }}>
                                                                                           
                                                                                            <Tooltip                                                                                                        
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis                                                                                    
                                                                                            tick={{fontSize: '8px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} /> 
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}                                                                              
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '8px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                          
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>
                
                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                                 
                                                                 )
                                                                 
                                                         }  
                                       
                                        </Slider>
                                        </div>
                                                                                          
                                    </CardBody>
                                </Card>
                            </Col> 
                        </Row>
            </Fragment>
        )
    }
}
