import React, {Component, Fragment} from 'react';

 import cx from 'classnames';
import {Row, Col,Button,CardHeader,Card,CardBody,CardFooter,CardTitle} from 'reactstrap';
import Loader from 'react-loaders';
import axios from 'axios';
import moment from 'moment';

import { API_ROOT} from '../../../../../api-config';

//sala Broodstock
const Nave100 = "5d7800be4d43af2b90afd26d";
const Nave200 = "5d7a6234b123470168990aa1";
const Nave300 = "5d7a625bb123470168990aa2";
const Nave400 = "5d7a6267b123470168990aa3";
const Nave500 = "5d7a6279b123470168990aa4";
const Nave600 = "5d7a6282b123470168990aa5";
const Nave700 = "5d7a628fb123470168990aa6";
const Nave800 = "5d94aa611c064a2120e19058";

const tag101="5d7801514d43af2b90afd26e";

// //101-112
// const TKN100 = [
//     "5d7801514d43af2b90afd26e",
//     "5d7a73b0b123470168990aaa",
//     "5d7a73b9b123470168990aab",
//     "5d7a73c1b123470168990aac",
//     "5d7a73cdb123470168990aad",
//     "5d7a73d7b123470168990aae",
//     "5d7a73e0b123470168990aaf",
//     "5d7a73eab123470168990ab0",
//     "5d7a73f4b123470168990ab1",
//     "5d7a73fdb123470168990ab2",
//     "5d7a7403b123470168990ab3",
//     "5d7a7408b123470168990ab4"]

// const TKN200 = [
//     "5d7a77a9746ff9396c51f111",
//     "5d7a77b2746ff9396c51f112",
//     "5d7a77ba746ff9396c51f113",
//     "5d7a77c3746ff9396c51f114",
//     "5d7a77ca746ff9396c51f115",
//     "5d7a77d2746ff9396c51f116",
//     "5d7a77da746ff9396c51f117",
//     "5d7a77e2746ff9396c51f118",
//     "5d7a77e9746ff9396c51f119",
//     "5d7a77f3746ff9396c51f11a",
//     "5d7a77fa746ff9396c51f11b",
//     "5d7a77fe746ff9396c51f11c"]

// const TKN300=[
//     "5d7a7e43746ff9396c51f11d",
//     "5d7a7e4c746ff9396c51f11e",
//     "5d7a7e54746ff9396c51f11f",
//     "5d7a7e5b746ff9396c51f120",
//     "5d7a7e64746ff9396c51f121",
//     "5d7a7e64746ff9396c51f122",
//     "5d7a7e73746ff9396c51f123",
//     "5d7a7e7a746ff9396c51f124",
//     "5d7a7e81746ff9396c51f125",
//     "5d7a7e86746ff9396c51f126",
//     "5d7a7e8d746ff9396c51f127",
//     "5d7a7e90746ff9396c51f128"]
    
// const TKN400=[
//     "5d7a7ffb746ff9396c51f129",
//     "5d7a8002746ff9396c51f12a",
//     "5d7a8009746ff9396c51f12b",
//     "5d7a8013746ff9396c51f12c",
//     "5d7a801b746ff9396c51f12d",
//     "5d7a8021746ff9396c51f12e",
//     "5d7a8026746ff9396c51f12f",
//     "5d7a802d746ff9396c51f130",
//     "5d7a8033746ff9396c51f131",
//     "5d7a8039746ff9396c51f132",
//     "5d7a803f746ff9396c51f133",
//     "5d7a8043746ff9396c51f134"
//     ]

// const TKN500=[
//     "5d7a80cb746ff9396c51f135",
//     "5d7a80d3746ff9396c51f136",
//     "5d7a80db746ff9396c51f137",
//     "5d7a80e7746ff9396c51f138",
//     "5d7a80ef746ff9396c51f139",
//     "5d7a80f5746ff9396c51f13a",
//     "5d7a80fb746ff9396c51f13b",
//     "5d7a8102746ff9396c51f13c",
//     "5d7a810b746ff9396c51f13d",
//     "5d7a8113746ff9396c51f13e",
//     "5d7a8118746ff9396c51f13f",
//     "5d7a811c746ff9396c51f140"
//     ]

// const TKN600=[
//     "5d7a82a2746ff9396c51f141",
//     "5d7a82a9746ff9396c51f142",
//     "5d7a82b4746ff9396c51f143",
//     "5d7a82fb746ff9396c51f144",
//     "5d7a8302746ff9396c51f145",
//     "5d7a8308746ff9396c51f146",
//     "5d7a8310746ff9396c51f147",
//     "5d7a8317746ff9396c51f148",
//     "5d7a831d746ff9396c51f149",
//     "5d7a8325746ff9396c51f14a",
//     "5d7a832a746ff9396c51f14b",
//     "5d7a832e746ff9396c51f14c"
//     ]

// const TKN700=[
//     "5d7a839f746ff9396c51f14d",
//     "5d7a83a5746ff9396c51f14e",
//     "5d7a83ad746ff9396c51f14f",
//     "5d7a83b4746ff9396c51f150",
//     "5d7a83bc746ff9396c51f151",
//     "5d7a83c4746ff9396c51f152",
//     "5d7a83d0746ff9396c51f153",
//     "5d7a83d6746ff9396c51f154",
//     "5d7a83dc746ff9396c51f155",
//     "5d7a83e1746ff9396c51f156",
//     "5d7a83e7746ff9396c51f157",
//     "5d7a83eb746ff9396c51f158"
//     ]



class PanelGeneralIgnao extends Component {  
    constructor() {
        super();
        this.state = {         
            myDataTKN100: [],     
            myDataTKN200: [],
            myDataTKN300: [],
            myDataTKN400: [],
            myDataTKN500: [],
            myDataTKN600: [],
            myDataTKN700: [],
            myDataTKN800: [],
            error: null,
            n_naves:["01","02","03","04","05","06","07","08"],
            Ox:true,
            Temp:false,
            Sat:false,
            dateTimeRefresh:""
        }
        this.loadDataTag1 = this.loadDataTag1.bind(this);

        

    }
    
    
    componentDidMount = () => {
        this.setState({ isLoading: true });    
        this.loadDataTag1();
        const intervaloRefresco = 10000;
         this.intervalIdTag1 = setInterval(() => this.loadDataTag1(),intervaloRefresco);
      }

    getdateLastTime=(token,f1,f2)=>{
        //, dateTimeRefresh:myDataTKN100.lastData
        axios
        .get(`${API_ROOT}/measurement/xy/tag/5d87f973a5f89831dc75b24c/${f1}/${f2}`, {
             'Authorization': 'Bearer ' + token
        })
        .then(response => {
            const measurements = response.data.data;
            this.setState({ dateTimeRefresh:measurements[measurements.length-1].x});
        })
        .catch(error => {
          console.log(error);
        });
    }

    updateTagState=(id,actual_state)=>{
        const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };

       let EndPointTag = `${API_ROOT}/tag/${id}`; 
        const  content1 = {
            "state": actual_state
        }   
        axios.put(EndPointTag, content1, config)
        .then(response => {
          if(response.status==200){
            this.loadDataTag1();
          }
          console.log("actualizado " + response.status);
          
        });
    }
  
    // componentWillUnmount = () => { 
    //       clearInterval(this.intervalIdTag1);  
    // }

    // getAxios=(api,token)=>{
    //     return axios
    //     .get(api, {
    //       headers: {  
    //         'Authorization': 'Bearer ' + token
    //       }
    //     })
    //     .then(response => {
    //         return response.data.data
    //     })
    //     .catch(error => {
    //         console.log(error);
    //       });
    // }
    
    loadDataTag1 = () =>  {  
        
        let now = new Date(); 
        //this.setState({dateTimeRefresh:now})
        const f1 = moment(now).subtract(360, "minutes").format('YYYY-MM-DDTHH:mm:ss') + ".000Z";
        const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";    
      
        const API1 = `${API_ROOT}/tag/zone/${Nave100}/${f1}/${f2}`;
        const API2 = `${API_ROOT}/tag/zone/${Nave200}/${f1}/${f2}`;
        const API3 = `${API_ROOT}/tag/zone/${Nave300}/${f1}/${f2}`;
        const API4 = `${API_ROOT}/tag/zone/${Nave400}/${f1}/${f2}`;
        const API5 = `${API_ROOT}/tag/zone/${Nave500}/${f1}/${f2}`;
        const API6 = `${API_ROOT}/tag/zone/${Nave600}/${f1}/${f2}`;
        const API7 = `${API_ROOT}/tag/zone/${Nave700}/${f1}/${f2}`;
        const API8 = `${API_ROOT}/tag/zone/${Nave800}/${f1}/${f2}`;

       //const token = localStorage.getItem("token"); 
       const token = "tokenFalso";
       
       //Naves

       //console.log(this.getAxios(API1,token));

       axios.all([
        axios.get(API1, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API2, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API3, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API4, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API5, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API6, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API7, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
        axios.get(API8, {
            headers: {  
              'Authorization': 'Bearer ' + token
            }
          }),
      ])
      .then(axios.spread((data1,data2,data3,data4,data5,data6,data7,data8)=>{
        //console.log(data1.data.data.concat(data2.data.data.concat(data3.data.data)));
        //console.log(data1.data.data);
        // const myDataTKN100 = data1.data.data.concat(data2.data.data
        //                                     .concat(data3.data.data
        //                                     .concat(data4.data.data
        //                                     .concat(data5.data.data
        //                                     .concat(data6.data.data
        //                                     .concat(data7.data.data
        //                                     .concat(data8.data.data
        //                                     .concat(data9.data.data
        //                                     .concat(data10.data.data
        //                                     .concat(data11.data.data
        //                                     .concat(data12.data.data
        //                                     )))))))))));
        const myDataTKN100 = data1.data.data;
        const myDataTKN200 = data2.data.data;
        const myDataTKN300 = data3.data.data;
        const myDataTKN400 = data4.data.data;
        const myDataTKN500 = data5.data.data;
        const myDataTKN600 = data6.data.data;
        const myDataTKN700 = data7.data.data;
        const myDataTKN800 = data8.data.data;
        this.setState({ myDataTKN100,myDataTKN200,myDataTKN300,myDataTKN400,myDataTKN500,
            myDataTKN600,myDataTKN700,myDataTKN800,isLoading: false })
      }))
      .catch(error => console.log(error));


        this.getdateLastTime(token,f1,f2);

    //Sala Smolt
    //Sala Alevines 
        

    }

    getButtonOx=(data, num, n)=>{
        return <Button id={data.id} 
            className=" mr-0 mb-1 ml-1  pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast"
            style={{marginTop:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}} onClick={((e)=>{
                // console.log(data);
                // console.log(data._id)
                // console.log(data.nameAddress);
                // data.state==1?this.updateTagState(data._id,0):this.updateTagState(data._id,1)
                
            })}>     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                        <span className="badge badge-light m-0 ml-0 pl-1 w-100 ">
                        <font size="2">{data.state!=1?"-":data.lastValue}</font>
                        {/* <span className="opacity-6  pl-0 size_unidad2">  
                        225 </span>  */}
                        </span>

                    {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1){
                            if(data.address=="100" || data.address=="200" || data.address=="300" || data.address=="400" ||
                             data.address=="500" || data.address=="600" || data.address=="700" || data.address=="800" ||
                             data.address=="900" || data.address=="1000" || data.address=="1100" || data.address=="1200"){
                                 //console.log(data.name+" "+data.alertMin+" max: "+data.alertMax);
                                if(data.lastValue>=data.alertMin && data.lastValue<=data.alertMax){
                                    //return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                                    return <span  className={cx("badge badge-dot badge-dot-lg badge-success")}>> </span>
                                }else{
                                    return <span  className={cx("badge badge-dot badge-dot-lg badge-danger")}>> </span>
                                }
                            }
                            
                        }
                        
                    })}
                     
                </Button>
    }

    getButtonTag=(array,n,num)=>{
        if(this.state.Ox)
        return array.filter(()=>n==num).map((data)=>{
            //console.log(n);
            // let split=data.nameAddress.split('K');
            // let n_tks=split[1].split('N');
            // console.log(n_tks[0]);
            if(data.nameAddress=="D100TK"+num+"01N"+num || data.nameAddress=="D100TK"+num+"02N"+num || data.nameAddress=="D100TK"+num+"03N"+num ||
            data.nameAddress=="D100TK"+num+"04N"+num || data.nameAddress=="D100TK"+num+"05N"+num || data.nameAddress=="D100TK"+num+"06N"+num ||
            data.nameAddress=="D100TK"+num+"07N"+num || data.nameAddress=="D100TK"+num+"08N"+num || data.nameAddress=="D100TK"+num+"09N"+num ||
            data.nameAddress=="D100TK"+num+"10N"+num || data.nameAddress=="D100TK"+num+"11N"+num || data.nameAddress=="D100TK"+num+"12N"+num ||
            
            data.nameAddress=="D200TK"+num+"01N"+num || data.nameAddress=="D200TK"+num+"02N"+num || data.nameAddress=="D200TK"+num+"03N"+num ||
            data.nameAddress=="D200TK"+num+"04N"+num || data.nameAddress=="D200TK"+num+"05N"+num || data.nameAddress=="D200TK"+num+"06N"+num ||
            data.nameAddress=="D200TK"+num+"07N"+num || data.nameAddress=="D200TK"+num+"08N"+num || data.nameAddress=="D200TK"+num+"09N"+num ||
            data.nameAddress=="D200TK"+num+"10N"+num || data.nameAddress=="D200TK"+num+"11N"+num || data.nameAddress=="D200TK"+num+"12N"+num ||
            
            data.nameAddress=="D300TK"+num+"01N"+num || data.nameAddress=="D300TK"+num+"02N"+num || data.nameAddress=="D300TK"+num+"03N"+num ||
            data.nameAddress=="D300TK"+num+"04N"+num || data.nameAddress=="D300TK"+num+"05N"+num || data.nameAddress=="D300TK"+num+"06N"+num ||
            data.nameAddress=="D300TK"+num+"07N"+num || data.nameAddress=="D300TK"+num+"08N"+num || data.nameAddress=="D300TK"+num+"09N"+num ||
            data.nameAddress=="D300TK"+num+"10N"+num || data.nameAddress=="D300TK"+num+"11N"+num || data.nameAddress=="D300TK"+num+"12N"+num ||
            
            data.nameAddress=="D400TK"+num+"01N"+num || data.nameAddress=="D400TK"+num+"02N"+num || data.nameAddress=="D400TK"+num+"03N"+num ||
            data.nameAddress=="D400TK"+num+"04N"+num || data.nameAddress=="D400TK"+num+"05N"+num || data.nameAddress=="D400TK"+num+"06N"+num ||
            data.nameAddress=="D400TK"+num+"07N"+num || data.nameAddress=="D400TK"+num+"08N"+num || data.nameAddress=="D400TK"+num+"09N"+num ||
            data.nameAddress=="D400TK"+num+"10N"+num || data.nameAddress=="D400TK"+num+"11N"+num || data.nameAddress=="D400TK"+num+"12N"+num ||
            
            data.nameAddress=="D500TK"+num+"01N"+num || data.nameAddress=="D500TK"+num+"02N"+num || data.nameAddress=="D500TK"+num+"03N"+num ||
            data.nameAddress=="D500TK"+num+"04N"+num || data.nameAddress=="D500TK"+num+"05N"+num || data.nameAddress=="D500TK"+num+"06N"+num ||
            data.nameAddress=="D500TK"+num+"07N"+num || data.nameAddress=="D500TK"+num+"08N"+num || data.nameAddress=="D500TK"+num+"09N"+num ||
            data.nameAddress=="D500TK"+num+"10N"+num || data.nameAddress=="D500TK"+num+"11N"+num || data.nameAddress=="D500TK"+num+"12N"+num ||
            
            data.nameAddress=="D600TK"+num+"01N"+num || data.nameAddress=="D600TK"+num+"02N"+num || data.nameAddress=="D600TK"+num+"03N"+num ||
            data.nameAddress=="D600TK"+num+"04N"+num || data.nameAddress=="D600TK"+num+"05N"+num || data.nameAddress=="D600TK"+num+"06N"+num ||
            data.nameAddress=="D600TK"+num+"07N"+num || data.nameAddress=="D600TK"+num+"08N"+num || data.nameAddress=="D600TK"+num+"09N"+num ||
            data.nameAddress=="D600TK"+num+"10N"+num || data.nameAddress=="D600TK"+num+"11N"+num || data.nameAddress=="D600TK"+num+"12N"+num ||
            
            data.nameAddress=="D700TK"+num+"01N"+num || data.nameAddress=="D700TK"+num+"02N"+num || data.nameAddress=="D700TK"+num+"03N"+num ||
            data.nameAddress=="D700TK"+num+"04N"+num || data.nameAddress=="D700TK"+num+"05N"+num || data.nameAddress=="D700TK"+num+"06N"+num ||
            data.nameAddress=="D700TK"+num+"07N"+num || data.nameAddress=="D700TK"+num+"08N"+num || data.nameAddress=="D700TK"+num+"09N"+num ||
            data.nameAddress=="D700TK"+num+"10N"+num || data.nameAddress=="D700TK"+num+"11N"+num || data.nameAddress=="D700TK"+num+"12N"+num ||
            
            data.nameAddress=="D800TK"+num+"01N"+num || data.nameAddress=="D800TK"+num+"02N"+num || data.nameAddress=="D800TK"+num+"03N"+num ||
            data.nameAddress=="D800TK"+num+"04N"+num || data.nameAddress=="D800TK"+num+"05N"+num || data.nameAddress=="D800TK"+num+"06N"+num ||
            data.nameAddress=="D800TK"+num+"07N"+num || data.nameAddress=="D800TK"+num+"08N"+num || data.nameAddress=="D800TK"+num+"09N"+num ||
            data.nameAddress=="D800TK"+num+"10N"+num || data.nameAddress=="D800TK"+num+"11N"+num || data.nameAddress=="D800TK"+num+"12N"+num ||
            
            data.nameAddress=="D900TK"+num+"01N"+num || data.nameAddress=="D900TK"+num+"02N"+num || data.nameAddress=="D900TK"+num+"03N"+num ||
            data.nameAddress=="D900TK"+num+"04N"+num || data.nameAddress=="D900TK"+num+"05N"+num || data.nameAddress=="D900TK"+num+"06N"+num ||
            data.nameAddress=="D900TK"+num+"07N"+num || data.nameAddress=="D900TK"+num+"08N"+num || data.nameAddress=="D900TK"+num+"09N"+num ||
            data.nameAddress=="D900TK"+num+"10N"+num || data.nameAddress=="D900TK"+num+"11N"+num || data.nameAddress=="D900TK"+num+"12N"+num ||
            
            data.nameAddress=="D1000TK"+num+"01N"+num || data.nameAddress=="D1000TK"+num+"02N"+num || data.nameAddress=="D1000TK"+num+"03N"+num ||
            data.nameAddress=="D1000TK"+num+"04N"+num || data.nameAddress=="D1000TK"+num+"05N"+num || data.nameAddress=="D1000TK"+num+"06N"+num ||
            data.nameAddress=="D1000TK"+num+"07N"+num || data.nameAddress=="D1000TK"+num+"08N"+num || data.nameAddress=="D1000TK"+num+"09N"+num ||
            data.nameAddress=="D1000TK"+num+"10N"+num || data.nameAddress=="D1000TK"+num+"11N"+num || data.nameAddress=="D1000TK"+num+"12N"+num ||
            
            data.nameAddress=="D1100TK"+num+"01N"+num || data.nameAddress=="D1100TK"+num+"02N"+num || data.nameAddress=="D1100TK"+num+"03N"+num ||
            data.nameAddress=="D1100TK"+num+"04N"+num || data.nameAddress=="D1100TK"+num+"05N"+num || data.nameAddress=="D1100TK"+num+"06N"+num ||
            data.nameAddress=="D1100TK"+num+"07N"+num || data.nameAddress=="D1100TK"+num+"08N"+num || data.nameAddress=="D1100TK"+num+"09N"+num ||
            data.nameAddress=="D1100TK"+num+"10N"+num || data.nameAddress=="D1100TK"+num+"11N"+num || data.nameAddress=="D1100TK"+num+"12N"+num ||
            
            data.nameAddress=="D1200TK"+num+"01N"+num || data.nameAddress=="D1200TK"+num+"02N"+num || data.nameAddress=="D1200TK"+num+"03N"+num ||
            data.nameAddress=="D1200TK"+num+"04N"+num || data.nameAddress=="D1200TK"+num+"05N"+num || data.nameAddress=="D1200TK"+num+"06N"+num ||
            data.nameAddress=="D1200TK"+num+"07N"+num || data.nameAddress=="D1200TK"+num+"08N"+num || data.nameAddress=="D1200TK"+num+"09N"+num ||
            data.nameAddress=="D1200TK"+num+"10N"+num || data.nameAddress=="D1200TK"+num+"11N"+num || data.nameAddress=="D1200TK"+num+"12N"+num)

            // if(!n_tks[0]%2){
            //     return this.getButtonOx(data,num,n);
            // }
            // else if(n_tks[0]%2){
            //     return this.getButtonOx(data,num,n);
            // }
            return this.getButtonOx(data,num,n);
        })
        if(this.state.Temp)
        return array.filter(()=>n==num).map((data)=>{
            if(data.nameAddress=="D101TK"+num+"01N"+num || data.nameAddress=="D101TK"+num+"02N"+num || data.nameAddress=="D101TK"+num+"03N"+num ||
            data.nameAddress=="D101TK"+num+"04N"+num || data.nameAddress=="D101TK"+num+"05N"+num || data.nameAddress=="D101TK"+num+"06N"+num ||
            data.nameAddress=="D101TK"+num+"07N"+num || data.nameAddress=="D101TK"+num+"08N"+num || data.nameAddress=="D101TK"+num+"09N"+num ||
            data.nameAddress=="D101TK"+num+"10N"+num || data.nameAddress=="D101TK"+num+"11N"+num || data.nameAddress=="D101TK"+num+"12N"+num ||
            
            data.nameAddress=="D201TK"+num+"01N"+num || data.nameAddress=="D201TK"+num+"02N"+num || data.nameAddress=="D201TK"+num+"03N"+num ||
            data.nameAddress=="D201TK"+num+"04N"+num || data.nameAddress=="D201TK"+num+"05N"+num || data.nameAddress=="D201TK"+num+"06N"+num ||
            data.nameAddress=="D201TK"+num+"07N"+num || data.nameAddress=="D201TK"+num+"08N"+num || data.nameAddress=="D201TK"+num+"09N"+num ||
            data.nameAddress=="D201TK"+num+"10N"+num || data.nameAddress=="D201TK"+num+"11N"+num || data.nameAddress=="D201TK"+num+"12N"+num ||
            
            data.nameAddress=="D301TK"+num+"01N"+num || data.nameAddress=="D301TK"+num+"02N"+num || data.nameAddress=="D301TK"+num+"03N"+num ||
            data.nameAddress=="D301TK"+num+"04N"+num || data.nameAddress=="D301TK"+num+"05N"+num || data.nameAddress=="D301TK"+num+"06N"+num ||
            data.nameAddress=="D301TK"+num+"07N"+num || data.nameAddress=="D301TK"+num+"08N"+num || data.nameAddress=="D301TK"+num+"09N"+num ||
            data.nameAddress=="D301TK"+num+"10N"+num || data.nameAddress=="D301TK"+num+"11N"+num || data.nameAddress=="D301TK"+num+"12N"+num ||
            
            data.nameAddress=="D401TK"+num+"01N"+num || data.nameAddress=="D401TK"+num+"02N"+num || data.nameAddress=="D401TK"+num+"03N"+num ||
            data.nameAddress=="D401TK"+num+"04N"+num || data.nameAddress=="D401TK"+num+"05N"+num || data.nameAddress=="D401TK"+num+"06N"+num ||
            data.nameAddress=="D401TK"+num+"07N"+num || data.nameAddress=="D401TK"+num+"08N"+num || data.nameAddress=="D401TK"+num+"09N"+num ||
            data.nameAddress=="D401TK"+num+"10N"+num || data.nameAddress=="D401TK"+num+"11N"+num || data.nameAddress=="D401TK"+num+"12N"+num ||
            
            data.nameAddress=="D501TK"+num+"01N"+num || data.nameAddress=="D501TK"+num+"02N"+num || data.nameAddress=="D501TK"+num+"03N"+num ||
            data.nameAddress=="D501TK"+num+"04N"+num || data.nameAddress=="D501TK"+num+"05N"+num || data.nameAddress=="D501TK"+num+"06N"+num ||
            data.nameAddress=="D501TK"+num+"07N"+num || data.nameAddress=="D501TK"+num+"08N"+num || data.nameAddress=="D501TK"+num+"09N"+num ||
            data.nameAddress=="D501TK"+num+"10N"+num || data.nameAddress=="D501TK"+num+"11N"+num || data.nameAddress=="D501TK"+num+"12N"+num ||
            
            data.nameAddress=="D601TK"+num+"01N"+num || data.nameAddress=="D601TK"+num+"02N"+num || data.nameAddress=="D601TK"+num+"03N"+num ||
            data.nameAddress=="D601TK"+num+"04N"+num || data.nameAddress=="D601TK"+num+"05N"+num || data.nameAddress=="D601TK"+num+"06N"+num ||
            data.nameAddress=="D601TK"+num+"07N"+num || data.nameAddress=="D601TK"+num+"08N"+num || data.nameAddress=="D601TK"+num+"09N"+num ||
            data.nameAddress=="D601TK"+num+"10N"+num || data.nameAddress=="D601TK"+num+"11N"+num || data.nameAddress=="D601TK"+num+"12N"+num ||
            
            data.nameAddress=="D701TK"+num+"01N"+num || data.nameAddress=="D701TK"+num+"02N"+num || data.nameAddress=="D701TK"+num+"03N"+num ||
            data.nameAddress=="D701TK"+num+"04N"+num || data.nameAddress=="D701TK"+num+"05N"+num || data.nameAddress=="D701TK"+num+"06N"+num ||
            data.nameAddress=="D701TK"+num+"07N"+num || data.nameAddress=="D701TK"+num+"08N"+num || data.nameAddress=="D701TK"+num+"09N"+num ||
            data.nameAddress=="D701TK"+num+"10N"+num || data.nameAddress=="D701TK"+num+"11N"+num || data.nameAddress=="D701TK"+num+"12N"+num ||
            
            data.nameAddress=="D801TK"+num+"01N"+num || data.nameAddress=="D801TK"+num+"02N"+num || data.nameAddress=="D801TK"+num+"03N"+num ||
            data.nameAddress=="D801TK"+num+"04N"+num || data.nameAddress=="D801TK"+num+"05N"+num || data.nameAddress=="D801TK"+num+"06N"+num ||
            data.nameAddress=="D801TK"+num+"07N"+num || data.nameAddress=="D801TK"+num+"08N"+num || data.nameAddress=="D801TK"+num+"09N"+num ||
            data.nameAddress=="D801TK"+num+"10N"+num || data.nameAddress=="D801TK"+num+"11N"+num || data.nameAddress=="D801TK"+num+"12N"+num ||
            
            data.nameAddress=="D901TK"+num+"01N"+num || data.nameAddress=="D901TK"+num+"02N"+num || data.nameAddress=="D901TK"+num+"03N"+num ||
            data.nameAddress=="D901TK"+num+"04N"+num || data.nameAddress=="D901TK"+num+"05N"+num || data.nameAddress=="D901TK"+num+"06N"+num ||
            data.nameAddress=="D901TK"+num+"07N"+num || data.nameAddress=="D901TK"+num+"08N"+num || data.nameAddress=="D901TK"+num+"09N"+num ||
            data.nameAddress=="D901TK"+num+"10N"+num || data.nameAddress=="D901TK"+num+"11N"+num || data.nameAddress=="D901TK"+num+"12N"+num ||
            
            data.nameAddress=="D1001TK"+num+"01N"+num || data.nameAddress=="D1001TK"+num+"02N"+num || data.nameAddress=="D1001TK"+num+"03N"+num ||
            data.nameAddress=="D1001TK"+num+"04N"+num || data.nameAddress=="D1001TK"+num+"05N"+num || data.nameAddress=="D1001TK"+num+"06N"+num ||
            data.nameAddress=="D1001TK"+num+"07N"+num || data.nameAddress=="D1001TK"+num+"08N"+num || data.nameAddress=="D1001TK"+num+"09N"+num ||
            data.nameAddress=="D1001TK"+num+"10N"+num || data.nameAddress=="D1001TK"+num+"11N"+num || data.nameAddress=="D1001TK"+num+"12N"+num ||
            
            data.nameAddress=="D1101TK"+num+"01N"+num || data.nameAddress=="D1101TK"+num+"02N"+num || data.nameAddress=="D1101TK"+num+"03N"+num ||
            data.nameAddress=="D1101TK"+num+"04N"+num || data.nameAddress=="D1101TK"+num+"05N"+num || data.nameAddress=="D1101TK"+num+"06N"+num ||
            data.nameAddress=="D1101TK"+num+"07N"+num || data.nameAddress=="D1101TK"+num+"08N"+num || data.nameAddress=="D1101TK"+num+"09N"+num ||
            data.nameAddress=="D1101TK"+num+"10N"+num || data.nameAddress=="D1101TK"+num+"11N"+num || data.nameAddress=="D1101TK"+num+"12N"+num ||
            
            data.nameAddress=="D1201TK"+num+"01N"+num || data.nameAddress=="D1201TK"+num+"02N"+num || data.nameAddress=="D1201TK"+num+"03N"+num ||
            data.nameAddress=="D1201TK"+num+"04N"+num || data.nameAddress=="D1201TK"+num+"05N"+num || data.nameAddress=="D1201TK"+num+"06N"+num ||
            data.nameAddress=="D1201TK"+num+"07N"+num || data.nameAddress=="D1201TK"+num+"08N"+num || data.nameAddress=="D1201TK"+num+"09N"+num ||
            data.nameAddress=="D1201TK"+num+"10N"+num || data.nameAddress=="D1201TK"+num+"11N"+num || data.nameAddress=="D1201TK"+num+"12N"+num)
            return <Button  style={{marginTop:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}}  
            className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast" color="primary" onClick={(()=>{
                //data.state=1?this.updateTagState(data._id,1):this.updateTagState(data._id,0)
            })}>     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100">  
                    <font size="2">{data.state!=1?"-":data.lastValue}</font>
                    {/* <span className="opacity-6  pl-0 size_unidad2">  
                    225 </span>  */}
                    </span>
        
                    {/* {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1)
                        return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                    })} */}
                </Button>
        })
        if(this.state.Sat)
        return array.filter(()=>n==num).map((data)=>{
            if(data.nameAddress=="D111TK"+num+"01N"+num || data.nameAddress=="D100TK"+num+"02N"+num || data.nameAddress=="D100TK"+num+"03N"+num ||
            data.nameAddress=="D111TK"+num+"04N"+num || data.nameAddress=="D111TK"+num+"05N"+num || data.nameAddress=="D111TK"+num+"06N"+num ||
            data.nameAddress=="D111TK"+num+"07N"+num || data.nameAddress=="D111TK"+num+"08N"+num || data.nameAddress=="D111TK"+num+"09N"+num ||
            data.nameAddress=="D111TK"+num+"10N"+num || data.nameAddress=="D111TK"+num+"11N"+num || data.nameAddress=="D111TK"+num+"12N"+num ||
            
            data.nameAddress=="D211TK"+num+"01N"+num || data.nameAddress=="D211TK"+num+"02N"+num || data.nameAddress=="D211TK"+num+"03N"+num ||
            data.nameAddress=="D211TK"+num+"04N"+num || data.nameAddress=="D211TK"+num+"05N"+num || data.nameAddress=="D211TK"+num+"06N"+num ||
            data.nameAddress=="D211TK"+num+"07N"+num || data.nameAddress=="D211TK"+num+"08N"+num || data.nameAddress=="D211TK"+num+"09N"+num ||
            data.nameAddress=="D211TK"+num+"10N"+num || data.nameAddress=="D211TK"+num+"11N"+num || data.nameAddress=="D211TK"+num+"12N"+num ||
            
            data.nameAddress=="D311TK"+num+"01N"+num || data.nameAddress=="D311TK"+num+"02N"+num || data.nameAddress=="D311TK"+num+"03N"+num ||
            data.nameAddress=="D311TK"+num+"04N"+num || data.nameAddress=="D311TK"+num+"05N"+num || data.nameAddress=="D311TK"+num+"06N"+num ||
            data.nameAddress=="D311TK"+num+"07N"+num || data.nameAddress=="D311TK"+num+"08N"+num || data.nameAddress=="D311TK"+num+"09N"+num ||
            data.nameAddress=="D311TK"+num+"10N"+num || data.nameAddress=="D311TK"+num+"11N"+num || data.nameAddress=="D311TK"+num+"12N"+num ||
            
            data.nameAddress=="D411TK"+num+"01N"+num || data.nameAddress=="D411TK"+num+"02N"+num || data.nameAddress=="D411TK"+num+"03N"+num ||
            data.nameAddress=="D411TK"+num+"04N"+num || data.nameAddress=="D411TK"+num+"05N"+num || data.nameAddress=="D411TK"+num+"06N"+num ||
            data.nameAddress=="D411TK"+num+"07N"+num || data.nameAddress=="D411TK"+num+"08N"+num || data.nameAddress=="D411TK"+num+"09N"+num ||
            data.nameAddress=="D411TK"+num+"10N"+num || data.nameAddress=="D411TK"+num+"11N"+num || data.nameAddress=="D411TK"+num+"12N"+num ||
            
            data.nameAddress=="D511TK"+num+"01N"+num || data.nameAddress=="D511TK"+num+"02N"+num || data.nameAddress=="D511TK"+num+"03N"+num ||
            data.nameAddress=="D511TK"+num+"04N"+num || data.nameAddress=="D511TK"+num+"05N"+num || data.nameAddress=="D511TK"+num+"06N"+num ||
            data.nameAddress=="D511TK"+num+"07N"+num || data.nameAddress=="D511TK"+num+"08N"+num || data.nameAddress=="D511TK"+num+"09N"+num ||
            data.nameAddress=="D511TK"+num+"10N"+num || data.nameAddress=="D511TK"+num+"11N"+num || data.nameAddress=="D511TK"+num+"12N"+num ||
            
            data.nameAddress=="D611TK"+num+"01N"+num || data.nameAddress=="D611TK"+num+"02N"+num || data.nameAddress=="D611TK"+num+"03N"+num ||
            data.nameAddress=="D611TK"+num+"04N"+num || data.nameAddress=="D611TK"+num+"05N"+num || data.nameAddress=="D611TK"+num+"06N"+num ||
            data.nameAddress=="D611TK"+num+"07N"+num || data.nameAddress=="D611TK"+num+"08N"+num || data.nameAddress=="D611TK"+num+"09N"+num ||
            data.nameAddress=="D611TK"+num+"10N"+num || data.nameAddress=="D611TK"+num+"11N"+num || data.nameAddress=="D611TK"+num+"12N"+num ||
            
            data.nameAddress=="D711TK"+num+"01N"+num || data.nameAddress=="D711TK"+num+"02N"+num || data.nameAddress=="D711TK"+num+"03N"+num ||
            data.nameAddress=="D711TK"+num+"04N"+num || data.nameAddress=="D711TK"+num+"05N"+num || data.nameAddress=="D711TK"+num+"06N"+num ||
            data.nameAddress=="D711TK"+num+"07N"+num || data.nameAddress=="D711TK"+num+"08N"+num || data.nameAddress=="D711TK"+num+"09N"+num ||
            data.nameAddress=="D711TK"+num+"10N"+num || data.nameAddress=="D711TK"+num+"11N"+num || data.nameAddress=="D711TK"+num+"12N"+num ||
            
            data.nameAddress=="D811TK"+num+"01N"+num || data.nameAddress=="D811TK"+num+"02N"+num || data.nameAddress=="D811TK"+num+"03N"+num ||
            data.nameAddress=="D811TK"+num+"04N"+num || data.nameAddress=="D811TK"+num+"05N"+num || data.nameAddress=="D811TK"+num+"06N"+num ||
            data.nameAddress=="D811TK"+num+"07N"+num || data.nameAddress=="D811TK"+num+"08N"+num || data.nameAddress=="D811TK"+num+"09N"+num ||
            data.nameAddress=="D811TK"+num+"10N"+num || data.nameAddress=="D811TK"+num+"11N"+num || data.nameAddress=="D811TK"+num+"12N"+num ||
            
            data.nameAddress=="D911TK"+num+"01N"+num || data.nameAddress=="D911TK"+num+"02N"+num || data.nameAddress=="D911TK"+num+"03N"+num ||
            data.nameAddress=="D911TK"+num+"04N"+num || data.nameAddress=="D911TK"+num+"05N"+num || data.nameAddress=="D911TK"+num+"06N"+num ||
            data.nameAddress=="D911TK"+num+"07N"+num || data.nameAddress=="D911TK"+num+"08N"+num || data.nameAddress=="D911TK"+num+"09N"+num ||
            data.nameAddress=="D911TK"+num+"10N"+num || data.nameAddress=="D911TK"+num+"11N"+num || data.nameAddress=="D911TK"+num+"12N"+num ||
            
            data.nameAddress=="D1011TK"+num+"01N"+num || data.nameAddress=="D1011TK"+num+"02N"+num || data.nameAddress=="D1011TK"+num+"03N"+num ||
            data.nameAddress=="D1011TK"+num+"04N"+num || data.nameAddress=="D1011TK"+num+"05N"+num || data.nameAddress=="D1011TK"+num+"06N"+num ||
            data.nameAddress=="D1011TK"+num+"07N"+num || data.nameAddress=="D1011TK"+num+"08N"+num || data.nameAddress=="D1011TK"+num+"09N"+num ||
            data.nameAddress=="D1011TK"+num+"10N"+num || data.nameAddress=="D1011TK"+num+"11N"+num || data.nameAddress=="D1011TK"+num+"12N"+num ||
            
            data.nameAddress=="D1111TK"+num+"01N"+num || data.nameAddress=="D1111TK"+num+"02N"+num || data.nameAddress=="D1111TK"+num+"03N"+num ||
            data.nameAddress=="D1111TK"+num+"04N"+num || data.nameAddress=="D1111TK"+num+"05N"+num || data.nameAddress=="D1111TK"+num+"06N"+num ||
            data.nameAddress=="D1111TK"+num+"07N"+num || data.nameAddress=="D1111TK"+num+"08N"+num || data.nameAddress=="D1111TK"+num+"09N"+num ||
            data.nameAddress=="D1111TK"+num+"10N"+num || data.nameAddress=="D1111TK"+num+"11N"+num || data.nameAddress=="D1111TK"+num+"12N"+num ||
            
            data.nameAddress=="D1211TK"+num+"01N"+num || data.nameAddress=="D1211TK"+num+"02N"+num || data.nameAddress=="D1211TK"+num+"03N"+num ||
            data.nameAddress=="D1211TK"+num+"04N"+num || data.nameAddress=="D1211TK"+num+"05N"+num || data.nameAddress=="D1211TK"+num+"06N"+num ||
            data.nameAddress=="D1211TK"+num+"07N"+num || data.nameAddress=="D1211TK"+num+"08N"+num || data.nameAddress=="D1211TK"+num+"09N"+num ||
            data.nameAddress=="D1211TK"+num+"10N"+num || data.nameAddress=="D1211TK"+num+"11N"+num || data.nameAddress=="D1211TK"+num+"12N"+num)
            return <Button style={{marginTop:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}} 
              className=" mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast" color="primary" onClick={(()=>{
                //data.state=1?this.updateTagState(data._id,1):this.updateTagState(data._id,0)
            })}>     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100">
                    <font size="2">{data.state!=1?"-":data.lastValue}</font>
                    {/* <span className="opacity-6  pl-0 size_unidad2">  
                    225 </span> */}
                     </span>
        
                     {/* {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1)
                        return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                    })} */}
                </Button>
        })
    }
    getButtonTagCabecera=(array,n,num)=>{
        return array.filter(()=>n==num).map((data)=>{
            
            if(data.nameAddress=="D100TK"+num+"01N"+num || data.nameAddress=="D101TK"+num+"01N"+num || data.nameAddress=="D111TK"+num+"01N"+num ||
             data.nameAddress=="D112TK"+num+"01N"+num)
            return <Button id={data.id} style={{marginTop:30,marginLeft:20,backgroundColor:`${data.state==-1?"rgba(84, 92, 216,.5)":"rgb(84, 92, 216)"}`}} 
            className=" mr-0 mb-1    pt-0 pr-0 pb-0 pl-0 ancho_btn animated fadeIn fast opacity-6" color="primary" onClick={((e)=>{
                // console.log(data._id)
                // console.log(data.state);
                //data.state=1?this.updateTagState(data._id,1):this.updateTagState(data._id,0)
                // if(data.state==1){
                //     this.updateTagState(data._id,0)
                // }else{
                //     this.updateTagState(data._id,1)
                // }
                
            })}>     
                        <div className="  pl-1 size_label">  
                        {data.name}</div> 
                    <span className="badge badge-light m-0 ml-0 pl-1 w-100 ">
                    <font size="2">{data.state!=1?"-":data.lastValue}</font>
                    {/* <span className="opacity-6  pl-0 size_unidad2">  
                    225 </span>  */}
                    </span>
        
                   
                    {this.state.n_naves.filter(()=>n==num).map(()=>{
                        if(data.state!=-1 && data.nameAddress=="D100TK"+num+"01N"+num){
                            if(data.lastValue>=data.alertMin && data.lastValue<=data.alertMax){
                                //return <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span>
                                return <span  className={cx("badge badge-dot badge-dot-lg badge-success")}>> </span>
                            }else{
                                return <span  className={cx("badge badge-dot badge-dot-lg badge-danger")}>> </span>
                            }
                        }
                        
                    })}
                </Button>
        })
    }
  
    render() { 
    
        const { myDataTKN100,myDataTKN200,myDataTKN300,myDataTKN400,myDataTKN500,myDataTKN600,myDataTKN700,myDataTKN800,isLoading, error} = this.state;  
        if (error) {
            return <p>{error.message}</p>;
        }  
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }   

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

    return (
        <Fragment>
                  
             

                            {/* <div className="app-page-title">
                                <div className="page-title-wrapper">
                                    <div className="page-title-heading">
                                        <div>
                                        Panel General
                                        </div>
                                    </div> */}
                                   {/* <div className="page-title-actions">
                                        Alarmado
                                        <span  className="badge badge-dot badge-dot-lg badge-danger  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>  
                                         Normal
                                        <span  className="badge badge-dot badge-dot-lg badge-success  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span> 
                                     </div> */}                   
                                {/* </div>
                             </div> */}
                            
                                <Row>

                                <Col md="12" lg="12">

                                <Card className="main-card mb-1">
                                            
                                            <CardHeader className="card-header-tab" style={{height:50}}>
                                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal" style={{marginLeft:-20}}>
                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                <Button 
                                                                className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Ox?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.setState({
                                                                        Ox:true,
                                                                        Temp:false,
                                                                        Sat:false
                                                                    })
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} opacity-1 */}<font color={!this.state.Ox?"":"#fff"}>Ox (mg/L)</font>
                                                                        </div>
                                                                </Button>  
                                                        </div>
                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                <Button 
                                                                className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Temp?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.setState({
                                                                        Ox:false,
                                                                        Temp:true,
                                                                        Sat:false
                                                                    })
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} */}<font color={!this.state.Temp?"":"#fff"}>Temp (°C)</font>
                                                                        </div>
                                                                </Button>  
                                                        </div>
                                                        <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                <Button 
                                                                className="btn-icon-vertical btn-square btn-transition p-1" style={{backgroundColor:`${!this.state.Sat?"":"#545cd8"}`}} outline color={miscolores[0]}
                                                                onClick={()=>{
                                                                    this.setState({
                                                                        Ox:false,
                                                                        Temp:false,
                                                                        Sat:true
                                                                    })
                                                                }}>  
                                                                    {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                    <div className="size-boton mt-0  " style={{color:miscolores[1]}} >                                                                                                                            
                                                                    {/* {data.measurements[data.measurements.length-1].value   }  */}
                                                                    <span className="opacity-6  pl-0 size_unidad">  </span>
                                                                    </div>
                                                                    <div className="widget-subheading">
                                                                        {/* {data.shortName} */}<font color={!this.state.Sat?"":"#fff"}>Sat (%)</font>
                                                                        </div>
                                                                </Button>  
                                                        </div>                            
                                                    </div>
                                                </CardHeader>
                                            <CardBody className="p-0 opacity-0"> 
                                                <Row className="no-gutters" style={{marginLeft:20}}>
                                                    {
                                                        this.state.n_naves.map((n)=>{
                                                            if(n==1)
                                                            return<Col > 
                                                                        {/* <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn text-center">   </div>  */}
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN100.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,1)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN100.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,1)
                                                                                }           
                                                                            </div>
                                                                            </Col>                                 
                                                                        </Row>                    
                                                                   </Col>
                                                            if(n==2)
                                                            return<Col > 
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN200.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,2)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN200.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,2)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                      
                                                                   </Col>
                                                            if(n==3)
                                                            return<Col > 
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN300.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,3)
                                                                                }           
                                                                            </div> 
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN300.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,3)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                     
                                                                    </Col>
                                                            if(n==4)
                                                            return<Col > 
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN400.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,4)
                                                                                }           
                                                                            </div> 
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN400.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,4)
                                                                                }           
                                                                            </div> 
                                                                            </Col>
                                                                        </Row>                       
                                                                    </Col>
                                                            if(n==5)
                                                            return<Col > 
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN500.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,5)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN500.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,5)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                      
                                                                    </Col>
                                                            if(n==6)
                                                            return<Col > 
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN600.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,6)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN600.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,6)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                     
                                                                    </Col>
                                                            if(n==7)
                                                            return<Col > 
                                                                        <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Nave{` ${n[1]}00`}</b></Col></Row>
                                                                        <Row>
                                                                            <Col>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN700.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2==0;
                                                                                    }),n,7)
                                                                                }           
                                                                            </div> 
                                                                            </Col>
                                                                            <Col style={{marginLeft:`${-20}%`}}>
                                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${70}px`}}>
                                                                                {
                                                                                    this.getButtonTag(myDataTKN700.filter((data)=>{
                                                                                        let split=data.nameAddress.split('K');
                                                                                        let n_tks=split[1].split('N');
                                                                                        return n_tks[0]%2!=0;
                                                                                    }),n,7)
                                                                                }           
                                                                            </div>
                                                                            </Col>
                                                                        </Row>                      
                                                                    </Col>
                                                            // if(n==8)
                                                            // return<Col > 
                                                            //             <Row><Col size={12} style={{marginLeft:50,marginTop:10}}><b>Cabecera{n}</b></Col></Row>
                                                            //             <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary" style={{width:`${150}px`}}>
                                                            //                 {
                                                            //                     this.getButtonTag(myDataTKN800,n,8)
                                                            //                 }           
                                                            //             </div>                        
                                                            //         </Col>
                                                            
                                                        })
                                                    }
                                                    
                                                </Row>
                                            </CardBody>
                                            <CardFooter className="text-muted center">Última Actualización: {this.state.dateTimeRefresh!=""?moment(this.state.dateTimeRefresh).format('DD-MM-YYYY HH:mm:ss'):""}</CardFooter>
                                            </Card>
                                            <Card>
                                                <CardHeader>Cabecera
                                                {
                                                        this.state.n_naves.map((n)=>{
                                                            if(n==8)
                                                            return<Col > 
                                                                        <div className="widget-description opacity-10 text-focus  border-primary">
                                                                            {
                                                                                this.getButtonTagCabecera(myDataTKN800,n,8)
                                                                            }           
                                                                        </div>
                                                                        <div style={{marginTop:25}}></div>                       
                                                                    </Col>
                                                        })
                                                    }
                                                </CardHeader>
                                            </Card>
                                    </Col>


                   
                                 



                                </Row>
              
          
      </Fragment>
    )
  }

}

export default PanelGeneralIgnao;




