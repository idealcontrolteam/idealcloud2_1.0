import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Centros } from './entity/centros.entity';
import { GLOBAL_BD_PREFIX } from '../global';

@Injectable()
export class CentroService {
  constructor(
    @InjectRepository(Centros) private centroRepository: Repository<Centros>,
  ) {}

  async getCentros(empresa:string): Promise<Centros[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.centroRepository.query(
      `SELECT 
            id_centros as code, 
            nombre_centros as name, 
            activo_centros as active 
            FROM ${prefix}.centros ORDER BY activo_centros DESC`,
    );
  }
  async getCentros_Usuario(empresa:string,usuario: any): Promise<Centros[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.centroRepository.query(
      `SELECT 
            centros.id_centros as code, 
            centros.nombre_centros as name, 
            centros.activo_centros as active
            FROM ${prefix}.centros
            INNER JOIN ${prefix}.cent_usua on cent_usua.id_centros=centros.id_centros
            INNER JOIN ${prefix}.usuarios on usuarios.id_usuarios=cent_usua.id_usuarios
            WHERE 
            usuarios.login_usuarios = ? AND 
            usuarios.clave_usuarios = ? ORDER BY activo_centros DESC`,
          [usuario.login_usuarios, usuario.clave_usuarios],
    );
  }
  async getCentro(empresa:string, _id: number): Promise<Centros[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.centroRepository.query(
      `SELECT 
            id_centros as code, 
            nombre_centros as name, 
            activo_centros as active 
            FROM ${prefix}.centros 
            WHERE 
            id_centros = ?`,
      [_id],
    );
  }
  async getCentrosActivos(empresa:string,usuario:any): Promise<Centros[]> {
    // console.log(usuario.id_usuarios)
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.centroRepository.query(
      `SELECT centros.id_centros as code, 
      nombre_centros as name, 
      activo_centros as active
      FROM ${prefix}.centros
      INNER JOIN cent_usua on cent_usua.id_centros=centros.id_centros 
      INNER JOIN usuarios on cent_usua.id_usuarios=usuarios.id_usuarios 
      WHERE activo_centros = 1 and usuarios.id_usuarios=${usuario.id_usuarios}
      `,
    );
  }
  async getCentrosInactivos(empresa:string): Promise<Centros[]> {
    const prefix=empresa=='sasa'?`idealcloud_ox_bd`:`${GLOBAL_BD_PREFIX}${empresa}`;
    return await this.centroRepository.query(
      `SELECT 
            id_centros as code, 
            nombre_centros as name, 
            activo_centros as active 
            FROM ${prefix}.centros 
            WHERE 
            activo_centros = 0 `,
    );
  }
}
