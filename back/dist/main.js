"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const rate = 1;
    const points = 20;
    app.enableCors();
    await app.listen(3013);
}
bootstrap();
//# sourceMappingURL=main.js.map