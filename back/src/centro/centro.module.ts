import { Module } from '@nestjs/common';
import { CentroService } from './centro.service';
import { CentroController } from './centro.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Centros } from './entity/centros.entity';
import { UsuarioModule } from '../usuario/usuario.module';
import { UsuarioService } from '../usuario/usuario.service';

@Module({
  imports: [TypeOrmModule.forFeature([Centros]),UsuarioModule],
  providers: [CentroService,UsuarioService],
  controllers: [CentroController],
  exports: [TypeOrmModule]
})
export class CentroModule {}
