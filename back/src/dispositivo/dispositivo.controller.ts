import {
  Controller,
  Post,
  Param,
  Req,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { DispositivoService } from './dispositivo.service';
import { GLOBAL_URL } from '../global';
import { UsuarioService } from '../usuario/usuario.service';

@Controller(`${GLOBAL_URL}`)
export class DispositivoController {
  constructor(
    private service: DispositivoService,
    private usuarioService: UsuarioService,
  ) {}

  @Post('sondas_activas')
  async dispositivos_activos(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getDispositivosActivos(params.empresa);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'Sondas fetched!',
        data: result,
        count: result.length,
      });
    }
  }
  @Post('sondas/:id')
  async get(@Param() params, @Req() req, @Res() res) {
    let userResult = await this.usuarioService.validateUser(
      params.empresa,
      req.body,
    );
    if (!userResult) {
      return res.status(HttpStatus.UNAUTHORIZED).json({
        statusCode: HttpStatus.UNAUTHORIZED,
        message: 'Not authorized',
      });
    } else {
      let result = await this.service.getDispositivo(params.empresa, params.id);
      return res.status(HttpStatus.OK).json({
        statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        message: result.length > 0 ? 'Sonda fetched!' : 'Not found',
        data: result[0] || null,
      });
    }
  }
  @Post('sondas/centro/:centro')
  async dispositivos_centro(@Param() params, @Req() req, @Res() res) {
      let result = await this.service.getDispositivosCentroActivos(
        params.empresa,
        params.centro,
      );
      return res.status(HttpStatus.OK).json({
        statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
        message: result.length > 0 ? 'Sondas fetched!' : 'Not found',
        data: result,
        count: result.length,
      });
  }

  // @Post('grupos/centro/:centro')
  // async grupos_centro(@Param() params, @Req() req, @Res() res) {
  //     console.log(params)

  //     let result = await this.service.getGruposCentro(
  //       params.empresa,
  //       params.centro,
  //     );
  //     return res.status(HttpStatus.OK).json({
  //       statusCode: result.length > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND,
  //       message: result.length > 0 ? 'Sondas fetched!' : 'Not found',
  //       data: result,
  //       count: result.length,
  //     });
  // }
}
