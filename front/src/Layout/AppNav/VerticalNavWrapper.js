import React, {Component, Fragment} from 'react';
import {withRouter,Route, Link, Redirect} from 'react-router-dom';
import MetisMenu from 'react-metismenu';
import { Menu } from './NavItems';
import { Button, Form, FormGroup, Label, Input, FormText,ListGroup, ListGroupItem, ButtonGroup } from 'reactstrap';
import {connect} from 'react-redux';
import {
    setNameUsuario,setEmailUsuario,setRolUsuario, setCentroUsuario
} from '../../reducers/Session';
import {getUserCompanys, getWorkPlace} from '../../services/user';
import Tendencia from '../../Pages/Dashboards/Tendencia/IndexTendencia'
import {reset} from 'redux-form';
import Tendencia_Online from '../../Pages/Dashboards/Tendencia/IndexTendencia_Online'

import "../../Pages/Dashboards/Zonas/style.css"

var ArrayMenu=[];
let select=1;
let select2=1;
let select3=1;
let buscar="";
let empresas=JSON.parse(sessionStorage.getItem("Centros"));
let sw=false;
let sw2=true;

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {         
            CompanyId:[],
            Companys:[],
            WorkPlace:[],
            ArrayMenu:[],
            disable:true,
            disable2:true,
            actividad:1,
        }

    }

    componentDidMount = () => {

        ArrayMenu=Menu; //.filter((data)=>data.content[1].label==Menu[1].content[1].label).map((data)=>data);
        let { emailUsuario,rolUsuario,centroUsuario } = this.props;
        //console.log("contador : "+Menu[1].content.length);
        if(rolUsuario=="SuperUsuario" || rolUsuario=="Administrador"){//rolUsuario=="SuperUsuario" ||
            if(Menu[1].content.length<=1){
                Menu[1].content.push({
                    icon: 'pe-7s-display1',
                    label: 'Usuario Centros',
                    to: '#/dashboards/usuario_centros'
                });
            }
        }
        //console.log("Menu : "+JSON.stringify(Menu[1]))
        

        this.getCompanys();
       
        console.log(emailUsuario);

        console.log("rol : "+rolUsuario);

        console.log(centroUsuario);

    }

    ingresarCentroUsuario = (centro) =>{
        let {setCentroUsuario} = this.props;
        setCentroUsuario(centro);
    }

    getNameCompany=(id)=>{
        return this.state.Companys.filter(data=>data._id==id).map((data)=>{
            return data.name
        })
    }

    getCompanys=()=>{
        let new_empresas=[];
        if(empresas!=null){
            empresas.map((data,i)=>{
                if(i==0){
                    buscar=data.name;
                    this.setId_Url(data.code,data.name);
                }
                new_empresas.push({
                    id:data.code,
                    name:data.name,
                    active:data.active
                });
                console.log(data);
            })
        }
        
        this.setState({WorkPlace:new_empresas})
    }


    setId_Url=(id,name)=>{
        let active=empresas.filter(data=>id==data.code).map((data)=>{
                return data.active
        });
        //alert("active : "+active);
        sessionStorage.setItem('workplace', JSON.stringify({"id":id,"name":name,"active":active}));
        this.ingresarCentroUsuario(`${id}/${name}`);
        //this.setState({WorkPlace:[]})
        buscar+="*"
    }

    inputChangeHandler = (event) => { 
        // console.log(event.target.value);  
        this.setState( { 
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }
    
    render() {
        //this.selectCompany(this.state.CompanyId[0])
        return (
            <Fragment> 
                <h5 className="app-sidebar__heading">Centros</h5>
                <div className="app-sidebar__heading ">
                        <div className=" ml-3 ">
                                {/* <div>
                                    Centros
                                </div> */}
                                
                                <ButtonGroup>
                                   <Button className={sw?"opacity-6":""} color="primary" style={{backgroundColor:`${sw?"white":""}`,borderColor:`${sw?"white":""}`}} onClick={(()=>{
                                       this.setState({actividad:1})
                                       sw=false;
                                       sw2=true;
                                   })}><font color={sw?"4C6971":""}>activos</font></Button> 
                                   <Button className={sw2?"opacity-6":""} color="primary" style={{backgroundColor:`${sw2?"white":""}`,borderColor:`${sw2?"white":""}`}} onClick={(()=>{
                                       this.setState({actividad:0})
                                       sw=true;
                                       sw2=false;
                                    })}><font color={sw2?"4C6971":""}>inactivos</font></Button> 
                                </ButtonGroup>
                                <div className="opacity-6" style={{borderRadius:`${2}%`}}>
                                {console.log(this.state.Companys)}
                                <ButtonGroup>
                                <Input placeholder="Buscar..." value={buscar} onChange={e => {
                                    buscar=e.target.value;
                                    this.inputChangeHandler(e);
                                }} />
                                <Button color="secondary" onClick={((e)=>{
                                    buscar="";
                                    this.inputChangeHandler(e);
                                })}><span className="pe-7s-refresh"></span></Button>
                                </ButtonGroup>
                                <div class="scrollbar" >
                                    <div className="force-overflow" style={{overflow:"auto",maxHeight:220}}>
                                    <ListGroup>
                                        {this.state.WorkPlace.filter((data)=>{
                                            if(buscar!=""){
                                                return data.name.toLocaleUpperCase().includes(buscar.toLocaleUpperCase())
                                            }
                                            if(buscar.includes("*")){
                                                return []
                                            }else{
                                                return data&&data.active==this.state.actividad
                                            }
                                        }).map((data,i)=>{
                                            if(i==0){
                                                return <ListGroupItem tag="button" action onClick={(()=>{
                                                    //alert(data.id)
                                                    buscar=data.name;
                                                    this.setId_Url(data.id,data.name);
                                                })}><span className="pe-7s-culture" style={{fontSize:20,opacity:6,color:`${data.active==1?"rgb(19, 185, 85)":""}`}}></span> {data.name}</ListGroupItem>
                                            }
                                            return <ListGroupItem tag="button" action onClick={(()=>{
                                                //alert(data.id)
                                                buscar=data.name;
                                                this.setId_Url(data.id,data.name);
                                            })}><span className="pe-7s-culture" style={{fontSize:20,color:`${data.active==1?"rgb(19, 185, 85)":""}`}}></span> {data.name}</ListGroupItem>
                                        })}
                                    </ListGroup>
                                    </div>
                                </div>
                                <FormGroup style={{display:"none"}}>
                                    <Input style={{marginTop:10}}  type="select" name="select" onChange={((e)=>{
                                            let index = e.target.selectedIndex;
                                            this.setId_Url(e.target.value,e.target.options[index].text);
                                        })} id="exampleSelect">
                                    <option  selected disabled value='0'>SELECCIONE UN CENTRO</option>
                                    {this.state.WorkPlace.map((data,i)=>{
                                        //console.log("active "+data.active)
                                        //className={`${data.active==1?"badge-success":"badge-secondary"}`}
                                        // if(data.active==1){
                                        //     if(i==0){
                                        //     return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}}  selected value={data.id} id={data.name}>&#x1F7E2; {data.name}</option>}
                                        //     return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.id} id={data.name}>&#x1F7E2; {data.name}</option>
                                        // }else{
                                        //     if(i==0){
                                        //         return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}}  selected value={data.id} id={data.name}>&#x26AA; {data.name}</option>}
                                        //         return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.id} id={data.name}>&#x26AA; {data.name}</option>
                                        // }
                                        if(i==0){
                                            return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}}  selected value={data.id} id={data.name}>{data.name}</option>
                                        }
                                        return <option style={{color:`${data.active==1?"rgb(19, 185, 85)":""}`}} value={data.id} id={data.name}> {data.name}</option>
                                    })}
                                    </Input>
                                </FormGroup>
                                
                                </div>
                         </div>
                 
                </div>
                <h5 className="app-sidebar__heading">Menu</h5>
                <MetisMenu  content={ArrayMenu} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix=""  classNameStateIcon="pe-7s-angle-down"/>
                {/* <h5 className="app-sidebar__heading">Salas</h5>
                <MetisMenu  content={Salas} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix="" classNameStateIcon="pe-7s-angle-down"/> */}
            </Fragment>
        );
    }

    isPathActive(path) {
        return this.props.location.pathname.startsWith(path);
    }
}
const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
    centroUsuario: state.Session.centroUsuario
  });

const mapDispatchToProps = dispatch => ({
    setCentroUsuario: enable => dispatch(setCentroUsuario(enable)),
});
  

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Nav));