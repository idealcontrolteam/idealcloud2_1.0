import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as rateLimit from 'express-rate-limit'; // CONFIGURACION DE LIMITE DE SOLICITUDES

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const rate=1; // MINUTOS DE ESPERA DE SOLICITUD
  const points=20; // SOLICITUDES PERMITIDAS EN EL PERIODO "rate"
  // CONFIGURACION DE LIMITE DE SOLICITUDES
  // app.use(
  //   rateLimit({
  //     windowMs: rate * 60 * 1000,
  //     max: points,
  //     message: {
  //       statusCode: 429,
  //       message: "Too many accounts created from this IP, please try again after an minute"
  //     }
  //   }),
  // );
  app.enableCors();
  await app.listen(3013);
}
bootstrap();
