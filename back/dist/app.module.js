"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const centro_module_1 = require("./centro/centro.module");
const dispositivo_module_1 = require("./dispositivo/dispositivo.module");
const registro_module_1 = require("./registro/registro.module");
const usuario_module_1 = require("./usuario/usuario.module");
const centros_entity_1 = require("./centro/entity/centros.entity");
const dispositivos_entity_1 = require("./dispositivo/entity/dispositivos.entity");
const registros_entity_1 = require("./registro/entity/registros.entity");
const usuarios_entity_1 = require("./usuario/entity/usuarios.entity");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: 'localhost',
                port: 3306,
                username: 'root',
                password: '',
                database: 'idealcloud_bd_aqu',
                entities: [centros_entity_1.Centros, dispositivos_entity_1.Dispositivos, registros_entity_1.Registros, usuarios_entity_1.Usuarios],
                synchronize: false,
            }),
            centro_module_1.CentroModule,
            dispositivo_module_1.DispositivoModule,
            registro_module_1.RegistroModule,
            usuario_module_1.UsuarioModule,
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map